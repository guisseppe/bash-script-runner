# **Problem Statement: Dependency Analysis and Tracking Across GitLab Projects**  

Our GitLab ecosystem consists of multiple applications across various subgroups, primarily Java applications, React applications, and Node.js projects. While configuration repositories can be disregarded, we require a structured approach to extract and track dependencies across our Java and Node.js projects.  

At present, we lack a centralised and efficient method for collecting and analysing dependencies from these projects. This limits our ability to distinguish between internal and external dependencies, assess potential risks, and make informed decisions regarding library usage across our applications.  

To address this, we need two scripts:  

1. **Java Dependency Parser**  
   - Parses the `pom.xml` file in each Java project.  
   - Extracts the parent POM and all declared dependencies.  
   - Captures any other relevant metadata about the project.  
   - Writes the extracted data into MongoDB.  

2. **Node.js Dependency Parser**  
   - Parses the `package.json` file in each Node.js project.  
   - Extracts all dependencies and relevant project metadata.  
   - Writes the extracted data into MongoDB.  

Additionally, both scripts should:  
- Include a **Boolean field** in MongoDB that identifies whether a dependency is an **internal** library, allowing us to differentiate internal and external dependencies for separate queries.  
- Add an **enum property** in MongoDB to categorise dependencies as either **Maven** or **NPM**, ensuring clear visibility of dependency types across projects.  

As part of this implementation, developers should also **draft a MongoDB schema** that is compatible with both the Node.js and Maven parsers while ensuring flexibility for future adoption of other languages, such as Python. This schema should be designed to accommodate evolving requirements and should be **presented to me** for review and approval.  

By implementing this solution, we will improve our ability to track dependencies, gain insights into internal library usage, and enhance dependency management across our applications.  