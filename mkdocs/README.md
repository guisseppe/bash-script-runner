# **MkDocs Pipeline Template**

This guide explains how to use the MkDocs pipeline template in your project.

> **Note:** The pipeline monitors the `static-site` directory for the mkdocs configuration file and site contents.

## **Overview**

The navigation bar generation script automates the creation of the `nav` section in `mkdocs.yml` by:

> **Note:** The nav bar generation script will not run if it detects a pre-defined nav section in your `mkdocs.yml` file.

1. Scanning the `static-site/docs/` directory for Markdown files (`.md`) and directories.

2. Automatically assigning the `index.md` file at the root of the `static-site/docs/` directory to the **Home** section.

3. Grouping root-level Markdown files under **Home**.

4. Preserving the order of directories and files based on numeric prefixes in their names.

5. Ignoring specified directories using the `IGNORE_DIRS` environment variable.

### **Ordering**

Files and directories are sorted based on numeric prefixes in their names.

1. **Numeric Prefixes**:
   
   - Files and directories are sorted numerically based on their prefixes.
   
   - Example:
     ```
     01-introduction.md
     02-getting-started.md
     ```
   
   - Appears in the navigation as:
       1. **Introduction**
       2. **Getting Started**

2. **Non-Numeric Names**:
   - Files and directories without numeric prefixes are sorted alphabetically.

**Example:** These files appear in the same order in the navigation.

```plaintext
01-introduction.md
02-getting-started.md
```

### **Ignored Directories**

Directories specified in the `IGNORE_DIRS` environment variable are skipped during navigation generation.

**Example:** To ignore the `archive` directory:
    
```yaml
variables:
    IGNORE_DIRS: archive
```

## **Usage Example**

Given a project with the following directory structure containing MkDocs configueration and your documentation:

```
static-site/
├── docs/
│   ├── index.md
│   ├── 01-introduction.md
│   ├── 02-getting-started.md
│   ├── cd-components/
│   │   ├── deployment-manifest.md
│   │   ├── maintenance-flags.md
│   ├── config-management/
│   │   ├── creating-a-profile.md
│   │   ├── getting-started.md
│   ├── archive/  # Example of a folder to ignore
├── mkdocs.yml
```

And you have the following variable defined in your `.gitlab-ci.yml` file.

```yaml
variables:
    IGNORE_DIRS: archive
```

When the pipeline builds your MKDocs it will auto-generate the following navbar if one doesn't exist in your `mkdcos.yml` file.

```yaml
nav:
  - Home:
      - Home: index.md
      - Introduction: 01-introduction.md
      - Getting started: 02-getting-started.md
  - CD Components:
      - Deployment manifest: cd-components/deployment-manifest.md
      - Maintenance flags: cd-components/maintenance-flags.md
  - Config Management:
      - Creating a profile: config-management/creating-a-profile.md
      - Getting started: config-management/getting-started.md
```