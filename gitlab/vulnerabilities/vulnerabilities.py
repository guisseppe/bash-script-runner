import csv
import gitlab

# Replace with your GitLab instance URL and access token
gl = gitlab.Gitlab('https://your-gitlab-instance.com', private_token='your_access_token')

# Replace with your subgroup ID
subgroup_id = 12345

def get_vulnerability_summary(group):
    """Gets vulnerability summary for a single group."""
    print(f"Fetching vulnerability summary for group: {group.name}")
    summary = {
        "group_name": group.name,
        "critical": 0,
        "high": 0,
        "medium": 0,
        "low": 0,
        "unknown": 0
    }
    for vulnerability in group.vulnerability_findings.list():
        summary[vulnerability.severity.lower()] += 1
    return summary

def write_to_csv(group, summary, filename):
    """Writes the vulnerability summary for a single group to a CSV file."""
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["Group Name", "Critical", "High", "Medium", "Low", "Unknown"])
        writer.writerow([
            summary["group_name"],
            summary["critical"],
            summary["high"],
            summary["medium"],
            summary["low"],
            summary["unknown"],
        ])

# Get the subgroup object
subgroup = gl.groups.get(subgroup_id)

# Generate and write the report for the main subgroup
main_summary = get_vulnerability_summary(subgroup)
write_to_csv(subgroup, main_summary, "main_subgroup_vulnerability_summary.csv")
print(f"Vulnerability summary for main subgroup written to main_subgroup_vulnerability_summary.csv")

# Generate and write reports for each nested subgroup
for subgroup in subgroup.subgroups.list(all=True):
    subgroup_summary = get_vulnerability_summary(subgroup)
    filename = f"{subgroup.name}_vulnerability_summary.csv"
    write_to_csv(subgroup, subgroup_summary, filename)
    print(f"Vulnerability summary for {subgroup.name} written to {filename}")