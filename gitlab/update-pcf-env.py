import sys
import logging
import subprocess
from enum import Enum

class Environment(Enum):
    DEV = "dev"
    SIT = "sit"
    UAT = "uat"
    NFT = "nft"
    ALL = "all"
    UNKNOWN = "unknown"


# Configure logging
logging.basicConfig(level=logging.DEBUG, format="%(asctime)s - %(levelname)s - %(message)s")


def get_modified_files():
    logging.debug("Fetching modified files from the latest commit...")
    try:
        # Get the list of modified files in the current commit
        result = subprocess.run(
            ["git", "diff", "--name-only", "HEAD~1..HEAD"],
            check=True,
            stdout=subprocess.PIPE,
            text=True,
        )
        files = result.stdout.strip().split("\n")
        logging.debug(f"Modified files: {files}")
        return files
    except subprocess.CalledProcessError as e:
        logging.error(f"Error while getting modified files: {e}")
        sys.exit(1)


def determine_environment(files):
    logging.debug("Determining environment based on modified files...")
    env_files = {
        "env-dev.ini": Environment.DEV,
        "env-sit.ini": Environment.SIT,
        "env-uat.ini": Environment.UAT,
        "env-nft.ini": Environment.NFT,
        "env-common.ini": Environment.ALL,
        "spring-application-dev.json": Environment.DEV,
        "spring-application-sit.json": Environment.SIT,
        "spring-application-uat.json": Environment.UAT,
        "spring-application-nft.json": Environment.NFT,
        "pcf-dev.config": Environment.DEV,
        "pcf-sit.config": Environment.SIT,
        "pcf-uat.config": Environment.UAT,
        "pcf-nft.config": Environment.NFT,
        "pcf-common.config": Environment.ALL,
    }

    matched_envs = set()

    for file in files:
        logging.debug(f"Checking file: {file}")
        if file in env_files:
            matched_envs.add(env_files[file])
            logging.debug(f"Matched environment: {env_files[file]}")
        elif "env-common" in file:
            matched_envs.add(Environment.ALL)
            logging.debug("Matched environment: ALL (env-common)")

    if not matched_envs:
        logging.debug("No environments matched. Returning UNKNOWN.")
        return [Environment.UNKNOWN]

    if Environment.ALL in matched_envs:
        logging.debug("Environment ALL found. Returning ALL.")
        return [Environment.ALL]

    env_list = sorted(env.name for env in matched_envs)
    logging.debug(f"Matched environments: {env_list}")
    return env_list


def main():
    logging.debug("Starting script...")
    modified_files = get_modified_files()
    if not modified_files:
        logging.debug("No modified files found. Exiting.")
        print("No modified files found.")
        sys.exit(0)

    envs = determine_environment(modified_files)
    if Environment.UNKNOWN in envs:
        print("Environment: unknown")
    else:
        print(f"Environment: {','.join(envs)}")


if __name__ == "__main__":
    main()