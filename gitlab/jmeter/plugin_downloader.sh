#!/bin/bash

# Array of URLs to download (replace with your actual URLs)
URLS=(
  "https://search.maven.org/remotecontent?filepath=kg/apc/jmeter-plugins-graphs-basic/2.0/jmeter-plugins-graphs-basic-2.0.jar"
  # Add more URLs here...
)

# Get the JMETER_PLUGIN_PATH from the environment
JMETER_PLUGIN_PATH=${JMETER_PLUGIN_PATH}

# Check if JMETER_PLUGIN_PATH is set
if [ -z "$JMETER_PLUGIN_PATH" ]; then
  echo "JMETER_PLUGIN_PATH environment variable is not set."
  exit 1
fi

# Check if JMeter plugin path exists
if [ ! -d "$JMETER_PLUGIN_PATH" ]; then
  echo "JMeter plugin path not found: $JMETER_PLUGIN_PATH"
  exit 1
fi

# Function to show download progress (using curl)
download_with_progress() {
  url="$1"
  filename=$(basename "$url")

  curl -L -o "$JMETER_PLUGIN_PATH/$filename" "$url" --progress-bar 2>&1
  echo "Download of $filename complete."
}

# Download each URL in the array
for url in "${URLS[@]}"; do
  # Check if it's a valid URL
  if [[ ! "$url" =~ ^https?:// ]]; then
    echo "Invalid URL format in array: $url"
    continue  # Skip to the next URL
  fi

  # Check if the file is a jar file
  if [[ ! "$url" =~ \.jar$ ]]; then
    echo "The URL in array does not point to a jar file: $url"
    continue  # Skip to the next URL
  fi

  # Download to the JMeter plugin path
  download_with_progress "$url" "$JMETER_PLUGIN_PATH"

  # List the contents of the plugin directory after each download
  echo "Contents of $JMETER_PLUGIN_PATH:"
  ls -l "$JMETER_PLUGIN_PATH"
done