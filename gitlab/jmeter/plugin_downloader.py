import os
import requests
from tqdm import tqdm

URLS = [
    "https://repo1.maven.org/maven2/kg/apc/jmeter-plugins-cmn-jmeter/0.7/jmeter-plugins-cmn-jmeter-0.7.jar",
    "https://repo1.maven.org/maven2/kg/apc/jmeter-plugins-manager/1.10/jmeter-plugins-manager-1.10.jar",
    # Add more URLs here
]

JMETER_PLUGIN_PATH = os.environ.get("JMETER_PLUGIN_PATH")

if not JMETER_PLUGIN_PATH:
    print("Error: JMETER_PLUGIN_PATH environment variable is not set.")
    exit(1)

if not os.path.isdir(JMETER_PLUGIN_PATH):
    print(f"Error: JMeter plugin path not found: {JMETER_PLUGIN_PATH}")
    exit(1)

def download_with_progress(url, filename):
    response = requests.get(url, stream=True)
    response.raise_for_status()  # Raise an exception for bad responses

    total_size = int(response.headers.get('content-length', 0))
    block_size = 1024
    progress_bar = tqdm(total=total_size, unit='iB', unit_scale=True)

    with open(filename, 'wb') as file:
        for data in response.iter_content(block_size):
            progress_bar.update(len(data))
            file.write(data)

    progress_bar.close()

    if total_size != 0 and progress_bar.n != total_size:
        print("Error: Download size mismatch.")
        exit(1)

    print(f"Download of {filename} complete.")

for url in URLS:
    filename = os.path.join(JMETER_PLUGIN_PATH, os.path.basename(url))
    download_with_progress(url, filename)

    # List contents after download (optional)
    print(f"Contents of {JMETER_PLUGIN_PATH}:")
    for item in os.listdir(JMETER_PLUGIN_PATH):
        print(item)
