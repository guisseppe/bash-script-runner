import gitlab
import csv

# GitLab setup
GITLAB_URL = "https://gitlab.com"  # Update if using a self-hosted instance
PRIVATE_TOKEN = "your_private_token"
SUBGROUP_ID = "your_subgroup_id"  # Replace with your actual subgroup ID
MR_SEARCH_CRITERIA = "your_mr_title_or_branch"  # Update with MR title/branch filter

gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)

def get_projects(subgroup_id):
    """Fetch non-archived projects from a subgroup and its nested subgroups before making API calls."""
    subgroup = gl.groups.get(subgroup_id)
    projects = subgroup.projects.list(all=True, archived=False)  # Directly filter non-archived projects

    # Get nested subgroups and fetch their projects
    subgroups = subgroup.subgroups.list(all=True)
    for sg in subgroups:
        projects.extend(get_projects(sg.id))

    return projects

def find_open_merge_requests(project):
    """Check if the project has an open MR matching the search criteria."""
    mrs = project.mergerequests.list(state='opened', all=True)
    for mr in mrs:
        if MR_SEARCH_CRITERIA in mr.title or MR_SEARCH_CRITERIA in mr.source_branch:
            return {
                "project_name": project.name,
                "subgroup": project.namespace["full_path"].split("/")[-2],  # Immediate subgroup name
                "project_id": project.id,
                "mr_link": mr.web_url
            }
    return None

def main():
    projects = get_projects(SUBGROUP_ID)
    report_data = []

    for project in projects:
        try:
            mr_data = find_open_merge_requests(gl.projects.get(project.id))
            if mr_data:
                report_data.append(mr_data)
        except gitlab.exceptions.GitlabGetError:
            print(f"Skipping project {project.name} due to access issues.")

    # Save as CSV
    csv_filename = "open_mr_report.csv"
    with open(csv_filename, "w", newline="") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=["project_name", "subgroup", "project_id", "mr_link"])
        writer.writeheader()
        writer.writerows(report_data)

    print(f"Report saved: {csv_filename}")

if __name__ == "__main__":
    main()