# Pact Broker Overview

To verify the Pact between a **Consumer** and a **Provider** using the Pact Broker and Maven, you need to run specific commands at different stages of development. This guide explains the commands you can use to **verify**, **deploy**, and **manage** the Pact workflow in a Spring project.

## **Verifying the Pact**

You can verify the Pact (that the provider complies with the contract defined by the consumer) using Maven with the following commands.

### **Command to verify the Pact (Provider-side):**
```bash
mvn pact:verify
```

- **When to use:** 
  - This command is used in the **Provider** service to verify that it meets the expectations specified by the **Consumer** in the contract.
  - It runs the **provider-side tests** to validate that the service complies with the contract published to the Pact Broker.
  
- **What it does:** 
  - The command checks the Pact files (usually fetched from the Pact Broker) and compares them against the actual provider service responses to ensure they match the consumer’s expectations.

#### **Options for `mvn pact:verify`:**
You can customize the behavior of `mvn pact:verify` with several options:

- To verify using a specific broker URL:
  ```bash
  mvn pact:verify -DpactBrokerUrl=https://your-pact-broker-url
  ```

- To verify a specific consumer:
  ```bash
  mvn pact:verify -Dconsumer=ConsumerName
  ```

- To specify a specific tag for the consumer version (useful for versioning and CI/CD pipelines):
  ```bash
  mvn pact:verify -DpactBrokerUrl=https://your-pact-broker-url -DconsumerVersionTags=prod
  ```

## **Publishing Pact Contracts**

To publish the contract created by the **Consumer** to the Pact Broker, use the following command in the consumer project:

### **Command to publish a Pact:**
```bash
mvn pact:publish -Dpact.broker.url=https://your-pact-broker-url -Dpact.provider.version=1.0.0
```

- **When to use:**
  - After the consumer has generated the contract (usually via consumer tests), you run this command to publish the contract to the Pact Broker.
  - This allows the **Provider** to retrieve the contract later for verification.
  
- **What it does:** 
  - This command publishes the Pact files to the Pact Broker at the specified URL, making them available for the **Provider** to verify.

## **Can I Deploy Command**

To control the deployment of the **Provider** or **Consumer**, Pact provides a `can-i-deploy` feature, which is integrated with the Pact Broker. This command checks whether a particular version of a **Consumer** or **Provider** can be deployed based on the state of contract verification.

### **Command to check if you can deploy:**
```bash
mvn pact:can-i-deploy -Dpacticipant=ProviderName -Dversion=1.0.0 -DpactBrokerUrl=https://your-pact-broker-url
```

- **When to use:**
  - You run this command in a **CI/CD pipeline** before deploying your services. It ensures that all the necessary contract verifications have passed before deployment.
  - Use this for both **Consumers** and **Providers** to ensure they are safe to deploy.
  
- **What it does:** 
  - It checks the Pact Broker to verify if the current version of the service (either consumer or provider) is compatible with the other party (provider or consumer) based on the contracts stored in the broker.
  - If any contract verifications have failed, the command will return a failure, preventing deployment.

#### **Options for `can-i-deploy`:**

- **To check the provider:** 
  ```bash
  mvn pact:can-i-deploy -Dpacticipant=ProviderName -Dversion=1.0.0 -DpactBrokerUrl=https://your-pact-broker-url
  ```

- **To check the consumer:**
  ```bash
  mvn pact:can-i-deploy -Dpacticipant=ConsumerName -Dversion=1.0.0 -DpactBrokerUrl=https://your-pact-broker-url
  ```

- **To specify a particular tag (useful in CI/CD):**
  ```bash
  mvn pact:can-i-deploy -Dpacticipant=ProviderName -Dversion=1.0.0 -DpactBrokerUrl=https://your-pact-broker-url -DconsumerVersionTags=dev
  ```

## **Other Useful Commands**

### **Command to create and verify Pact in a single test run:**
If you are developing and want to run the entire workflow in a single session (creating, verifying, and publishing):

1. **For the consumer:**
   ```bash
   mvn test
   ```

2. **For the provider verification:**
   ```bash
   mvn pact:verify
   ```

   After the verification succeeds, you can run the publish command for either the consumer or provider.

### **Command to publish verification results to the Pact Broker:**
After the provider successfully verifies the contract, you can also publish the results back to the Pact Broker.

```bash
mvn pact:publish-verification-results -Dpact.broker.url=https://your-pact-broker-url
```

- **When to use:**
  - This is used after running the verification tests in the **Provider** project, publishing the results back to the broker to inform the **Consumer** that the provider contract has passed the tests.

## Summary

| Command                                  | When to use                                                      |
|------------------------------------------|------------------------------------------------------------------|
| `mvn pact:verify`                        | Verifies the contract on the provider side.                      |
| `mvn pact:publish`                       | Publishes the Pact contract from the consumer to the Pact Broker. |
| `mvn pact:can-i-deploy -Dpacticipant=ItemProvider -Dlatest=true`                  | Checks if the service is safe to deploy based on verification.    |
| `mvn pact:publish-verification-results`  | Publishes the provider verification results to the Pact Broker.   |

### When to use each:

1. **Development Phase:**
   - Run `mvn test` for consumer-side tests.
   - Run `mvn pact:verify` for provider-side tests.
   
2. **CI/CD Pipeline:**
   - After consumer tests, run `mvn pact:publish` to share the contracts with the broker.
   - After provider tests, run `mvn pact:verify` and `mvn pact:publish-verification-results`.
   - Before deployment, run `mvn pact:can-i-deploy` to ensure that both services are compatible.