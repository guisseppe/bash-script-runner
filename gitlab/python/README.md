**1. Main Script (`generate_ci.py`)**

> **Note:** You need to install colorama by running `pip install colorama`

```python
import hashlib
import os
import sys
from colorama import init, Fore

# Initialize colorama for cross-platform compatibility
init(autoreset=True)

image_template = """
image: {image_name}

stages:
  - install
  - test
"""

def calculate_package_json_hash():
    """Calculates the MD5 hash of the package.json file."""
    try:
        with open("package.json", "rb") as f:
            file_content = f.read()
            return hashlib.md5(file_content).hexdigest()
    except FileNotFoundError:
        print(Fore.RED + "Error: package.json file not found." + Fore.RESET)
        sys.exit(1)

def load_job(job_file_path):
    """Loads job definition from a Python file."""
    try:
        with open(job_file_path, 'r') as f:
            return f.read() 
    except FileNotFoundError:
        print(Fore.RED + f"Error: Job file not found: {job_file_path}" + Fore.RESET)
        sys.exit(1)            

def generate_ci_config(jobs_dir):
    """Generates the complete GitLab CI configuration."""
    jobs = []
    for file in os.listdir(jobs_dir):
        if file.endswith(".py") and file != "__init__.py":
            job_file_path = os.path.join(jobs_dir, file)
            jobs.append(load_job(job_file_path))

    return image_template.format(
        image_name="node:16-alpine",
        package_json_hash=calculate_package_json_hash()
    ) + "\n".join(jobs) 

# Generate the configuration and save
try:
    ci_config = generate_ci_config("jobs")
    with open("dynamic-gitlab-ci.yml", "w") as f:
        f.write(ci_config)
except Exception as e:  # Catch any unexpected errors
    print(Fore.RED + f"Error generating CI configuration: {e}" + Fore.RESET)
    sys.exit(1)
```

**2. Job Definition Files (Within a `jobs` directory):**

* **`jobs/install_dependencies.py`**

```python
install_dependencies = """
install_dependencies:
  stage: install
  script:
    - npm install
  cache:
    key: "{package_json_hash}"
    paths:
      - node_modules/
"""
```

* **`jobs/puppeteer.py`**

```python
puppeteer = """
puppeteer:
  stage: test
  dependencies:
    - install_dependencies
  before_script:
    - node -v
    - git --version
    - export PUPPETEER_SKIP_DOWNLOAD=true
  script:
    - npm test
  after_script:
    - npm run test-report 
  artifacts:
    when: always
    paths:
      - test-results/
    reports:
      junit: test-results/cucumber-report-junit.xml 
"""
```

**Explanation:**

* **`load_job`:**  A helper function to read the job definition from a Python file.
* **`generate_ci_config`:**
   - Uses `os.listdir` to fetch files from the `jobs` directory.
   - Loads each `.py` file (except for special files like `__init__.py`) using `load_job`.
   - Combines all loaded jobs into the main configuration string.
* **Job Files:** Store individual job definitions as raw GitLab CI YAML snippets.

**Key Points:**

* **Create a `jobs` directory in the same location as your `generate_ci.py` script.**
* **Place your job definition files (e.g., `install_dependencies.py`, `puppeteer.py`) within the `jobs` directory.**
