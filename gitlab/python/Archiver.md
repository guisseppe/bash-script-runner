```python
import hashlib
import os
import argparse
import zipfile
import tarfile
from colorama import init, Fore

# Initialize colorama for colored logging
init(autoreset=True)


def create_archive(directory, archive_name):
    """Creates a tar archive of the directory contents."""
    try:
        with tarfile.open(f"{archive_name}.tar", "w:gz") as tar:
            for root, _, files in os.walk(directory):
                for file in files:
                    tar.add(os.path.join(root, file))
        print(Fore.GREEN + f"Created archive: {archive_name}.tar" + Fore.RESET)
    except Exception as e:
        print(Fore.RED + f"Error creating archive: {e}" + Fore.RESET)
        sys.exit(1)


def generate_checksums(archive_name):
    """Calculates MD5 and SHA1 checksums of the archive and saves them to separate files."""
    checksums = {}

    try:
        with open(f"{archive_name}.tar", "rb") as f:
            for checksum_type in (hashlib.md5, hashlib.sha1):
                checksum = checksum_type()
                checksum.update(f.read())
                checksums[checksum_type.__name__] = checksum.hexdigest()

        for name, checksum in checksums.items():
            with open(f"{archive_name}.{name}", "w") as f:
                f.write(checksum)

        print(Fore.GREEN + f"Generated checksum files for {archive_name}.tar" + Fore.RESET)
    except Exception as e:
        print(Fore.RED + f"Error generating checksums: {e}" + Fore.RESET)
        sys.exit(1)


def create_package(archive_name, package_name):
    """Packages the archive and checksum files into a ZIP or tar archive based on user choice."""
    try:
        if package_name.lower().endswith(".zip"):
            with zipfile.ZipFile(package_name, "w") as zip_file:
                for file in [f"{archive_name}.tar", f"{archive_name}.md5", f"{archive_name}.sha1"]:
                    zip_file.write(file)
            print(Fore.GREEN + f"Created ZIP package: {package_name}" + Fore.RESET)
        elif package_name.lower().endswith(".tar.gz"):
            with tarfile.open(package_name, "w:gz") as tar:
                for file in [f"{archive_name}.tar", f"{archive_name}.md5", f"{archive_name}.sha1"]:
                    tar.add(file)
            print(Fore.GREEN + f"Created TAR.GZ package: {package_name}" + Fore.RESET)
        else:
            print(Fore.RED + f"Invalid package format. Supported formats: .zip, .tar.gz" + Fore.RESET)
            sys.exit(1)
    except Exception as e:
        print(Fore.RED + f"Error creating package: {e}" + Fore.RESET)
        sys.exit(1)


def main():
    parser = argparse.ArgumentParser(description="Create archive with checksums and package it.")
    parser.add_argument("directory", type=str, help="Directory to archive.")
    parser.add_argument("archive_name", type=str, help="Name for the archive file.")
    parser.add_argument("package_name", type=str, help="Name for the final package file.")
    args = parser.parse_args()

    create_archive(args.directory, args.archive_name)
    generate_checksums(args.archive_name)
    create_package(args.archive_name, args.package_name)

    print(Fore.GREEN + f"Completed successfully!" + Fore.RESET)


if __name__ == "__main__":
    main()
```

**Prerequisites:**

* **colorama:** Install the colorama library (`pip install colorama`).

**How to Run the Script**

1. **Save as a Python file:** Save the provided script code as a Python file (e.g., `archive_util.py`).

2. **Open a Terminal/Command Prompt:** Navigate to the directory where you saved the script in your terminal.

3. **Execute with Arguments:** Run the script, providing the required arguments:

   ```bash
   python archive_util.py <directory_to_archive> <archive_name> <package_name>
   ```

   * **`<directory_to_archive>`:** Replace with the path to the directory you want to archive.
   * **`<archive_name>`:** Replace with the desired filename for the initial tar archive (don't include the `.tar` extension).
   * **`<package_name>`:** Replace with the desired name for the final package file (.zip or .tar.gz supported).

**Example Usage:**

```bash
python archive_util.py ./my_project my_project_archive my_package.zip 
```

**Example Log Output (Assuming Success)** 

```
Created archive: my_project_archive.tar
Generated checksum files for my_project_archive.tar
Created ZIP package: my_package.zip 
Completed successfully!
```

**Example Log (With an Error, e.g., Invalid Package Format):**

```
Created archive: my_project_archive.tar
Generated checksum files for my_project_archive.tar
Invalid package format. Supported formats: .zip, .tar.gz
```

**Notes:**

* The script will create the tar archive, checksum files, and the final package file in the same directory where your script is located.
* If any errors occur during the process (like a directory not existing), the script will print red error messages and exit with a non-zero exit code.
* The final success message is printed in green.