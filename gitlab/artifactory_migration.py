import gitlab
import re
import csv
import os
from datetime import datetime

# GitLab configuration
GITLAB_URL = os.getenv("GITLAB_URL")  # GitLab instance URL
GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")  # Personal access token
SUBGROUP_ID = os.getenv("SUBGROUP_ID")  # ID of the subgroup to search
SEARCH_URL = os.getenv("SEARCH_URL")  # URL to search for
REPLACE_URL = os.getenv("REPLACE_URL")  # Replacement URL
BRANCH_NAME = os.getenv("BRANCH_NAME", "url-update-branch")  # Branch name
MR_DESCRIPTION_FILE = os.getenv("MR_DESCRIPTION_FILE", "mr_description.md")  # MR description file
DRY_RUN = os.getenv("DRY_RUN", "true").lower() == "true"
PROJECT_PREFIXES = os.getenv("PROJECT_PREFIXES", "").split(",")  # Prefix filters
PROJECT_SUFFIXES = os.getenv("PROJECT_SUFFIXES", "").split(",")  # Suffix filters

# Labels for the MR with colors
MR_LABELS = {
    "DevOps": "#9324E0",
    "Infra": "#0075C9",
    "high priority": "#D32F2F"
}

# Initialize GitLab API client
gl = gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_TOKEN)
gl.auth()

# Ensure labels exist in GitLab
def ensure_labels_exist(project):
    existing_labels = {label.name for label in project.labels.list(all=True)}
    for label, color in MR_LABELS.items():
        if label not in existing_labels:
            project.labels.create({"name": label, "color": color})

# Get subgroup
subgroup = gl.groups.get(SUBGROUP_ID, include_subgroups=True)

# Prepare CSV report
csv_filename = "gitlab_url_update_report.csv"
fields = ["Project Name", "Project ID", "Subgroup", "Modified Files", "Branch Created", "MR Created"]

with open(csv_filename, mode="w", newline="") as file:
    writer = csv.DictWriter(file, fieldnames=fields)
    writer.writeheader()

    for project in subgroup.projects.list(all=True, archived=False):
        project_name = project.name
        project_id = project.id
        subgroup_name = project.namespace.get("name", "Unknown")

        # Check if project name matches prefix or suffix criteria
        if not any(project_name.startswith(prefix) for prefix in PROJECT_PREFIXES) and not any(project_name.endswith(suffix) for suffix in PROJECT_SUFFIXES):
            continue

        # Get default branch
        project = gl.projects.get(project_id)
        default_branch = project.default_branch

        # Ensure labels exist
        if not DRY_RUN:
            ensure_labels_exist(project)

        # Search for URL occurrences using GitLab Search API
        search_results = project.search("blobs", SEARCH_URL)
        modified_files = []
        
        for result in search_results:
            file_path = result['data']['path']
            try:
                file_obj = project.files.get(file_path=file_path, ref=default_branch)
                file_content = file_obj.decode()
            except gitlab.exceptions.GitlabGetError:
                print(f"Skipping {file_path} as it could not be retrieved.")
                continue
            
            if SEARCH_URL in file_content:
                updated_content = re.sub(re.escape(SEARCH_URL), REPLACE_URL, file_content)
                modified_files.append(file_path)
                
                if not DRY_RUN:
                    try:
                        project_file = project.files.get(file_path=file_path, ref=BRANCH_NAME)
                        project_file.content = updated_content
                        project_file.save(branch=BRANCH_NAME, commit_message=f'Updated {SEARCH_URL} to {REPLACE_URL}')
                    except gitlab.exceptions.GitlabGetError:
                        project.files.create({
                            'file_path': file_path,
                            'branch': BRANCH_NAME,
                            'content': updated_content,
                            'commit_message': f'Updated {SEARCH_URL} to {REPLACE_URL}'
                        })
        
        if modified_files:
            # Remove and recreate branch only if there are changes
            existing_branches = project.branches.list(search=BRANCH_NAME)
            if existing_branches:
                project.branches.delete(BRANCH_NAME)
                print(f"Deleted existing branch: {BRANCH_NAME}")
            project.branches.create({'branch': BRANCH_NAME, 'ref': default_branch})
            print(f"Created branch: {BRANCH_NAME}")
        
        branch_created = "Yes" if modified_files else "No"
        mr_created = "No"

        if modified_files and not DRY_RUN:
            # Check if an MR already exists
            existing_mrs = project.mergerequests.list(state="opened", source_branch=BRANCH_NAME)
            if not existing_mrs:
                # Create Merge Request
                with open(MR_DESCRIPTION_FILE, "r") as f:
                    mr_description = f.read()
                
                mr = project.mergerequests.create({
                    'source_branch': BRANCH_NAME,
                    'target_branch': default_branch,
                    'title': f'Update URL in {len(modified_files)} files',
                    'description': mr_description,
                    'labels': list(MR_LABELS.keys())
                })
                mr_created = "Yes"

        # Write to CSV
        writer.writerow({
            "Project Name": project_name,
            "Project ID": project_id,
            "Subgroup": subgroup_name,
            "Modified Files": ", ".join(modified_files),
            "Branch Created": branch_created,
            "MR Created": mr_created
        })

print(f"CSV report generated: {csv_filename}")
