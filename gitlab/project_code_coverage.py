import gitlab
import csv

# Configuration
GITLAB_URL = "https://gitlab.com"  # Replace with your GitLab instance URL
PERSONAL_ACCESS_TOKEN = "your_personal_access_token"  # Replace with your token
SUBGROUP_ID = "your_subgroup_id"  # Replace with your subgroup ID or path (e.g., 'namespace/subgroup')
IGNORELIST_FILE = "ignorelist.txt"
PREFIXES = ["prefix1", "prefix2"]  # Add your desired prefixes
SUFFIXES = ["suffix1", "suffix2"]  # Add your desired suffixes
OUTPUT_CSV = "project_coverage_report.csv"

def get_ignorelist(file_path):
    try:
        with open(file_path, "r") as f:
            ignorelist = [line.strip() for line in f.readlines()]
            print(f"Loaded ignore list: {ignorelist}")
            return ignorelist
    except FileNotFoundError:
        print(f"Ignore list file not found at {file_path}. Proceeding without it.")
        return []

def matches_prefix_or_suffix(name, prefixes, suffixes):
    result = any(name.startswith(prefix) for prefix in prefixes) or any(name.endswith(suffix) for suffix in suffixes)
    if result:
        print(f"Project '{name}' matches prefixes or suffixes.")
    else:
        print(f"Project '{name}' does not match prefixes or suffixes. Skipping.")
    return result

def fetch_coverage_score(project):
    try:
        print(f"Fetching coverage for project: {project.name}\n")

        # Fetch the full project object
        full_project = project.manager.gitlab.projects.get(project.id)

        # Get the last 10 pipelines
        pipelines = full_project.pipelines.list(order_by='id', sort='desc', per_page=10)
        if not pipelines:
            print(f"No pipelines found for project: {project.name}\n")
            return None

        for pipeline in pipelines:
            pipeline.refresh()  # Fetch detailed pipeline information
            jobs = pipeline.jobs.list()
            for job in jobs:
                if job.coverage:
                    print(f"Coverage for project '{project.name}' found in job '{job.name}': {job.coverage}%\n")
                    return job.coverage

        print(f"No coverage data found in the last 10 pipelines for project: {project.name}\n")
        return None
    except Exception as e:
        print(f"Error fetching coverage for project {project.name}: {e}\n")
        return None


def determine_project_type(project):
    files = project.repository_tree(ref=project.default_branch, per_page=100)
    if any(file['name'] == 'package.json' for file in files):
        return "SPA"
    elif any(file['name'] == 'pom.xml' for file in files):
        return "MICROSERVICE"
    else:
        return "UNKNOWN"


def get_group_name(project):
    group = project.manager.gitlab.groups.get(project.namespace['id'])
    return group.name


def generate_report():
    # Initialize GitLab connection
    print("Connecting to GitLab...")
    gl = gitlab.Gitlab(GITLAB_URL, private_token=PERSONAL_ACCESS_TOKEN)
    ignorelist = get_ignorelist(IGNORELIST_FILE)

    # Fetch subgroup and nested projects
    print(f"Fetching projects in subgroup: {SUBGROUP_ID}")
    subgroup = gl.groups.get(SUBGROUP_ID, lazy=False)
    projects = subgroup.projects.list(include_subgroups=True, all=True)
    print(f"Found {len(projects)} projects in subgroup and nested subgroups.")

    # Filter and process projects
    report_data = []
    for project in projects:
        print(f"Evaluating project: {project.name}")
        if project.archived:
            print(f"Project '{project.name}' is archived. Skipping.")
            continue
        if project.name in ignorelist:
            print(f"Project '{project.name}' is in the ignore list. Skipping.")
            continue
        if not matches_prefix_or_suffix(project.name, PREFIXES, SUFFIXES):
            continue

        # Fetch the nested subgroup name
        subgroup_name = get_group_name(project)

        # Determine project type
        project_type = determine_project_type(project)

        # Fetch coverage
        coverage = fetch_coverage_score(project)
        
        # Add data to report
        print(f"Adding project '{project.name}' to the report.")
        report_data.append({
            "Project Name": project.name,
            "Subgroup Name": subgroup_name,
            "Project Type": project_type,
            "Coverage Score": coverage or "N/A",
            "Project Link": project.web_url
        })

    # Write CSV report
    print(f"Writing report to {OUTPUT_CSV}...")
    with open(OUTPUT_CSV, mode="w", newline="") as csv_file:
        fieldnames = ["Project Name", "Subgroup Name",  "Project Type", "Coverage Score", "Project Link"]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(report_data)

    print(f"Report generation completed: {OUTPUT_CSV}")

if __name__ == "__main__":
    generate_report()