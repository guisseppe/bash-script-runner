import gitlab
from datetime import datetime, timedelta
import csv

# GitLab Configuration
GITLAB_URL = "https://gitlab.com"  # Replace with your GitLab URL
PERSONAL_ACCESS_TOKEN = "your_personal_access_token"  # Replace with your token
SUBGROUP_ID = 12345  # Replace with your subgroup ID
HOURS_THRESHOLD = 3  # Age threshold in hours for artifacts

# Matching Criteria
PREFIX_ARRAY = ["api", "service", "backend", "frontend"]  # Add your prefixes here
SUFFIX_ARRAY = ["worker", "processor", "manager", "gateway"]  # Add your suffixes here


def initialize_gitlab():
    """Initialize the GitLab connection."""
    return gitlab.Gitlab(GITLAB_URL, private_token=PERSONAL_ACCESS_TOKEN)


def delete_old_job_artifacts(project, hours):
    """
    Delete job artifacts older than a specified number of hours in a GitLab project.

    :param project: The GitLab project object
    :param hours: Age threshold in hours for deleting artifacts
    :return: A tuple with the count of artifacts deleted and total size recovered
    """
    cutoff_time = datetime.utcnow() - timedelta(hours=hours)
    deleted_count = 0
    recovered_size = 0

    # Fetch jobs
    jobs = project.jobs.list(all=True)
    for job in jobs:
        if job.artifacts_file:
            job_created_time = datetime.strptime(job.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
            if job_created_time < cutoff_time:
                try:
                    recovered_size += job.artifacts_file.get("size", 0)
                    job.delete_artifacts()
                    deleted_count += 1
                except Exception as e:
                    print(f"Failed to delete artifacts for job {job.id} in project {project.name}: {e}")
    return deleted_count, recovered_size


def process_project(project_data, hours):
    """
    Process an individual project to delete old artifacts.

    :param project_data: The project data object
    :param hours: Age threshold in hours for deleting artifacts
    :return: A dictionary with project cleanup details
    """
    project_name = project_data.name.lower()
    if project_data.archived:
        return None
    if any(project_name.startswith(prefix.lower()) for prefix in PREFIX_ARRAY) or \
       any(project_name.endswith(suffix.lower()) for suffix in SUFFIX_ARRAY):
        try:
            project = gl.projects.get(project_data.id)
            deleted_count, recovered_size = delete_old_job_artifacts(project, hours)
            return {"name": project_data.name, "artifacts_deleted": deleted_count, "storage_recovered": recovered_size}
        except Exception as e:
            print(f"Error processing project {project_data.name}: {e}")
    return None


def process_subgroup(group, hours):
    """
    Process a subgroup and its nested subgroups to delete old artifacts.

    :param group: The GitLab group object
    :param hours: Age threshold in hours for deleting artifacts
    :return: A list of dictionaries with cleanup details for each project
    """
    cleanup_report = []

    # Process projects in the group
    projects = group.projects.list(all=True, include_subgroups=True)
    for project_data in projects:
        report = process_project(project_data, hours)
        if report:
            cleanup_report.append(report)

    # Process nested subgroups
    subgroups = group.subgroups.list(all=True)
    for subgroup in subgroups:
        nested_group = gl.groups.get(subgroup.id)
        cleanup_report.extend(process_subgroup(nested_group, hours))

    return cleanup_report


def generate_csv_report(cleanup_report, output_file="cleanup_report.csv"):
    """
    Generate and save the cleanup report to a CSV file.

    :param cleanup_report: A list of dictionaries with cleanup details for each project
    :param output_file: Name of the output CSV file
    """
    # Sort by storage recovered (descending)
    cleanup_report.sort(key=lambda x: x["storage_recovered"], reverse=True)

    # Write to CSV
    with open(output_file, mode="w", newline="", encoding="utf-8") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=["Project Name", "Artifacts Deleted", "Storage Recovered (MB)"])
        writer.writeheader()
        for project in cleanup_report:
            writer.writerow({
                "Project Name": project["name"],
                "Artifacts Deleted": project["artifacts_deleted"],
                "Storage Recovered (MB)": f"{project['storage_recovered'] / (1024 ** 2):.2f}"
            })

    print(f"Cleanup report saved to {output_file}")


if __name__ == "__main__":
    try:
        # Initialize GitLab connection
        gl = initialize_gitlab()

        # Fetch the subgroup
        subgroup = gl.groups.get(SUBGROUP_ID)

        # Process the subgroup and generate the report
        report = process_subgroup(subgroup, HOURS_THRESHOLD)
        generate_csv_report(report)
    except Exception as e:
        print(f"An error occurred: {e}")