## GitLab Repo Extractor

This Bash script extracts repository names from a specified GitLab subgroup and saves them to a text file. It utilizes the GitLab API, making it a flexible solution for managing your GitLab projects.

### Features

* **Fetches repository names:**  Efficiently retrieves repository names from a GitLab subgroup using the API.
* **Pagination:** Handles retrieving repositories across multiple pages (useful for large subgroups).
* **Logging:** Provides informative logging messages with timestamps and color-coded levels (INFO, ERROR).
* **Dry Run Mode:**  Allows testing the script's actions without actually writing to the output file.
* **Environment Variable for Access Token:**  Prioritizes using an environment variable for storing the GitLab access token for enhanced security.

### Prerequisites

* **Bash:** A working Bash environment.
* **curl:**  Used for making API requests (usually pre-installed).
* **jq:** A command-line JSON processor for parsing API responses.  Install if needed ([https://stedolan.github.io/jq/](https://stedolan.github.io/jq/)).
* **GitLab Personal Access Token:** Required for authentication with the GitLab API.

### Setup

1. **Environment Variable:**

   * **Windows**
      1. Search for "Environment Variables" in the Start Menu.
      2. Click on "Edit the system environment variables".
      3. Click the "Environment Variables" button.
      4. Under "User variables", click "New".
      5. Set the variable name to `GITLAB_ACCESS_TOKEN` and the variable value to your actual GitLab Personal Access Token.
      6. Click "OK" on all the open windows.

   * **macOS/Linux**
      1. Open your terminal (e.g., Terminal on macOS, Bash on Linux).
      2.  Type the following command and press Enter:
          ```bash
          export GITLAB_ACCESS_TOKEN=your_actual_access_token
          ``` 
      3. **Optional (Persistence):**  To make the environment variable persist across terminal sessions, add the `export` command from step 2 to your shell profile file (e.g., `~/.bash_profile` or `~/.zshrc`).

2. **GitLab Access Token:**
   * If you don't have a Personal Access Token, refer to GitLab's documentation on creating one: [https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)  (Remember to give it 'api' scope).

3. **Script Settings:**
    * Open the script file (`repos.sh`).
    * Modify the following variables if needed:
        * `gitlab_url`: Your GitLab instance's URL (if not the default gitlab.com).
        * `subgroup_id`: The ID of the GitLab subgroup you want to extract repos from.
        * `output_file`: The name of the file to store the repository names.

### Usage

1. **Make the script executable:**
   ```bash
   chmod +x repos.sh 
   ```

2. **Run the script (Normal Mode):**
   ```bash
   ./repos.sh
   ```

3. **Run the script (Dry Run Mode):**
   ```bash
   ./repos.sh --dry-run
   ```
### Output

The script will generate a text file (default name: `repo_names.txt`) containing a list of all the repository names within the specified GitLab subgroup.