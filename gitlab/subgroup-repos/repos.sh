#!/bin/bash

# Settings
gitlab_url="https://gitlab.com"
output_dir="output"  
all_repos_file="all_repos.txt"
microservices_file="microservices.txt"
spa_file="spa.txt"
kafka_file="kafka.txt"
dry_run=false

# Color codes
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' 

# Logging function 
log() {
  level=$1
  message=$2
  timestamp=$(date +"%d-%m-%Y %H:%M:%S")
  echo -e "${timestamp} ${color}[${level}]${NC} $message" 
}

# Get repos
get_repos() {
  page=$1
  curl -s --header "PRIVATE-TOKEN: $access_token" \
    "$gitlab_url/api/v4/groups/$subgroup_id/projects?per_page=100&page=$page" \
    | jq -r '.[].name' 
}

# Help function
show_help() {
  echo "Usage: $0 [options]"
  echo "Options:"
  echo "  -h, --help            Show this help message"
  echo "  --access-token <token>  GitLab personal access token"
  echo "  --subgroup-id <id>      ID of the GitLab subgroup"
  echo "  --dry-run               Simulate the script's actions without saving changes"
}

# Process command-line arguments
while [[ $# -gt 0 ]]; do
 case "$1" in
    -h|--help)
      show_help
      exit 0
      ;;
  --access-token)
    access_token=$2
    shift 2 # Shift to consume both the flag and its value
   ;;
  --subgroup-id)
    subgroup_id=$2
    shift 2
    ;;
  --dry-run)
    dry_run=true
    shift
    ;;
  *)
    log "ERROR" "Unknown argument: $1"
    exit 1
    ;;
 esac
done

# Access token and Sub-group ID flag validation
if [[ -z "$access_token" ]]; then
  log "ERROR" "Missing access token. Use --access-token <token>"
  exit 1
fi

if [[ -z "$subgroup_id" ]]; then
  log "ERROR" "Missing subgroup ID. Use --subgroup-id <id>"
  exit 1
fi

# Main execution
mkdir -p $output_dir  

page=1
while true; do
  repos=$(get_repos $page)

  if [ -z "$repos" ]; then
    log "INFO" "All repositories fetched."
    break
  fi

  for repo in $repos; do
    echo "$repo" >> $output_dir/$all_repos_file  

    if [[ $repo == msvc-* || $repo == coord-* ]]; then
      echo "$repo" >> $output_dir/$microservices_file
      log "INFO" "Added $repo to $microservices_file"
    elif [[ $repo == spa-* ]]; then
      echo "$repo" >> $output_dir/$spa_file
      log "INFO" "Added $repo to $spa_file"
    elif [[ $repo == *-listener || $repo =~ kafka- ]]; then
      echo "$repo" >> $output_dir/$kafka_file
      log "INFO" "Added $repo to $kafka_file"
    fi
  done

  page=$((page + 1))
done

log "INFO" "Extraction successful"