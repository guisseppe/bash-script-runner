import gitlab
import csv
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# GitLab credentials
private_token = "YOUR_PRIVATE_TOKEN"  # Replace with your GitLab private token
gl = gitlab.Gitlab('https://gitlab.com', private_token=private_token)

# Configuration
subgroup_path = "your-subgroup-path"  # Replace with the path to your subgroup
prefixes = ["prefix1", "prefix2"]  # Replace with your project name prefixes
suffixes = ["suffix1", "suffix2"]  # Replace with your project name suffixes
search_phrase = "your-search-phrase"  # Replace with the phrase to search for

# CSV report file
csv_file = "project_report.csv"

# Function to check if a project matches the criteria
def is_matching_project(project):
    if project.archived:
        logging.debug(f"Project {project.name} is archived, skipping.")
        return False
    if not any(project.name.startswith(prefix) for prefix in prefixes) and \
       not any(project.name.endswith(suffix) for suffix in suffixes):
        logging.debug(f"Project {project.name} does not match prefix/suffix, skipping.")
        return False
    try:
        # Check for pom.xml and spring-data-mongo dependency
        project_files = project.repository_tree()
        if not any(file['name'] == 'pom.xml' for file in project_files):
            logging.debug(f"Project {project.name} does not have pom.xml, skipping.")
            return False
        pom_content = project.files.get(file_path="pom.xml", ref=project.default_branch).decode()
        if "spring-data-mongo" not in pom_content:
            logging.debug(f"Project {project.name} does not have spring-data-mongo dependency, skipping.")
            return False
    except gitlab.exceptions.GitlabGetError as e:
        logging.error(f"Error getting files for project {project.name}: {e}")
        return False
    logging.debug(f"Project {project.name} matches the criteria.")
    return True

# Function to search for the phrase in the specified file
def search_in_file(project):
    try:
        file_path = "config/spring-application-nft.json"
        file_content = project.files.get(file_path=file_path, ref=project.default_branch).decode()
        file_url = f"{project.web_url}/blob/{project.default_branch}/{file_path}"  # Construct the file URL
        if search_phrase in file_content:
            logging.debug(f"Phrase found in project {project.name}.")
            return "Present", file_url
        else:
            logging.debug(f"Phrase not found in project {project.name}.")
            return "Not Present", file_url
    except gitlab.exceptions.GitlabGetError as e:
        logging.error(f"Error getting file for project {project.name}: {e}")
        return "File Not Found", ""

# Get the subgroup
group = gl.groups.get(subgroup_path)

# Prepare the CSV writer
with open(csv_file, "w", newline="", encoding="utf-8") as f:
    writer = csv.writer(f)
    writer.writerow(["Project Name", "Subgroup", "Phrase Found", "File URL"])

    # Iterate through all projects in the subgroup and its descendants
    for project in group.projects.list(include_subgroups=True, all=True):
        logging.info(f"Checking project: {project.name}")
        if is_matching_project(project):
            phrase_found, file_url = search_in_file(project)
            # Get the project's subgroup name (only the last part)
            subgroup_name = project.namespace['name']
            writer.writerow([project.name, subgroup_name, phrase_found, file_url])

print(f"Report generated: {csv_file}")