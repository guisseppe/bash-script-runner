import gitlab
import os
import pandas as pd
import xml.etree.ElementTree as ET
import json
import re

# GitLab Configuration
GITLAB_URL = "https://gitlab.example.com"
PRIVATE_TOKEN = "your_private_token"
SUBGROUP_A_ID = 123456  # Replace with your subgroup A ID
SUBGROUP_B_ID = 789012  # Replace with your subgroup B ID

# Filtering criteria for Subgroup B projects
PROJECT_PREFIXES = ["prefix1", "prefix2"]  # Replace with your prefixes
PROJECT_SUFFIXES = ["-lib", "-service"]  # Replace with your suffixes
ADDITIONAL_LIBRARIES_FILE = "libraries.txt"  # File containing extra libraries to track

# Initialize GitLab API client
gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)


def load_additional_libraries():
    """Loads additional libraries from a text file."""
    additional_libraries = set()
    if os.path.exists(ADDITIONAL_LIBRARIES_FILE):
        with open(ADDITIONAL_LIBRARIES_FILE, "r") as f:
            additional_libraries = {line.strip() for line in f if line.strip()}
    
    return additional_libraries

def get_active_projects(subgroup_id):
    """Fetches active projects in a subgroup, including nested subgroups."""
    subgroup = gl.groups.get(subgroup_id)
    projects = subgroup.projects.list(all=True, archived=False)

    # Recursively fetch nested subgroups
    for nested in subgroup.subgroups.list(all=True):
        projects += get_active_projects(nested.id)
    
    return projects

def extract_maven_properties(content):
    """Extracts properties from the <properties> section of pom.xml."""
    properties = {}
    try:
        root = ET.fromstring(content)
        namespaces = {"maven": "http://maven.apache.org/POM/4.0.0"}
        props = root.find(".//maven:properties", namespaces)

        if props is not None:
            for prop in props:
                properties[prop.tag.split("}")[-1]] = prop.text
    except Exception as e:
        print(f"Error extracting Maven properties: {e}")

    return properties

def resolve_maven_version(version, properties):
    """Resolves Maven dependency versions that reference properties."""
    if version and version.startswith("${") and version.endswith("}"):
        prop_name = version[2:-1]  # Extract property name
        return properties.get(prop_name, version)  # Return resolved version or original if not found
    return version

def extract_maven_libraries(content):
    """Extracts published Maven libraries and resolves version properties."""
    try:
        root = ET.fromstring(content)
        namespaces = {"maven": "http://maven.apache.org/POM/4.0.0"}
        artifact_id = root.find(".//maven:artifactId", namespaces)
        version = root.find(".//maven:version", namespaces)

        properties = extract_maven_properties(content)
        resolved_version = resolve_maven_version(version.text if version is not None else "UNKNOWN", properties)

        if artifact_id is not None:
            return f"MAVEN:{artifact_id.text}", resolved_version
    except Exception as e:
        print(f"Error extracting Maven library: {e}")
    return None, None

def extract_npm_libraries(content):
    """Extracts published npm libraries from package.json."""
    try:
        data = json.loads(content)
        if "name" in data and "version" in data:
            return f"NPM:{data['name']}", data["version"]
    except Exception as e:
        print(f"Error extracting npm library: {e}")
    return None, None

def fetch_libraries_from_subgroup(subgroup_id):
    """Fetches all published libraries from Subgroup A (including nested subgroups)."""
    libraries = set()
    projects = get_active_projects(subgroup_id)

    for project in projects:
        try:
            # Checking for Maven (pom.xml)
            pom_file = project.files.get(file_path="pom.xml", ref=project.default_branch)
            library, version = extract_maven_libraries(pom_file.decode().decode("utf-8"))
            if library:
                libraries.add(library)
        
            # Checking for npm (package.json)
            package_json = project.files.get(file_path="package.json", ref=project.default_branch)
            library, version = extract_npm_libraries(package_json.decode().decode("utf-8"))
            if library:
                libraries.add(library)
        except:
            pass  # Skip projects without these files

    return libraries

def fetch_project_dependencies(project):
    """Fetches dependencies from a project's repository."""
    dependencies = set()
    
    try:
        # Search for Maven dependencies
        pom_files = project.repository_tree(path="", ref=project.default_branch, recursive=True, all=True)
        for file in pom_files:
            if file["path"].endswith("pom.xml"):
                file_content = project.files.get(file_path=file["path"], ref=project.default_branch).decode().decode("utf-8")
                dependencies.update(extract_maven_libraries(file_content))
    except:
        pass  # Skip if no pom.xml

    try:
        # Search for npm dependencies
        package_json = project.files.get(file_path="package.json", ref=project.default_branch).decode().decode("utf-8")
        dependencies.update(extract_npm_libraries(package_json))
    except:
        pass  # Skip if no package.json

    return dependencies

def generate_report():
    """Generates CSV reports of library usage."""
    print("Fetching libraries from Subgroup A...")
    subgroup_a_libraries = fetch_libraries_from_subgroup(SUBGROUP_A_ID)

    print("Loading additional libraries...")
    additional_libraries = load_additional_libraries()

    # Combine libraries from Subgroup A and additional libraries
    tracked_libraries = subgroup_a_libraries.union(additional_libraries)

    print("Fetching active projects in Subgroup B...")
    active_projects = get_active_projects(SUBGROUP_B_ID)

    dependency_data = []
    found_libraries = set()
    
    for project in active_projects:
        print(f"Analyzing {project.name}...")
        dependencies = fetch_project_dependencies(project)
        project_link = project.web_url

        for lib_name, version in dependencies:
            if lib_name in tracked_libraries:
                lib_type = "NPM" if lib_name.startswith("NPM:") else "MAVEN"
                dependency_data.append([lib_type, lib_name, project.name, project_link, version])
                found_libraries.add(lib_name)

    # Convert to DataFrame
    df = pd.DataFrame(dependency_data, columns=["Library Type", "Library Name", "Project Name", "Project Link", "Version"])
    
    # Generate summary report
    summary = df.groupby(["Library Type", "Library Name", "Version"])["Project Name"].count().reset_index()
    summary.rename(columns={"Project Name": "Total Projects Using It"}, inplace=True)
    
    # Save reports
    summary.to_csv("library_usage_report.csv", index=False)
    df.to_csv("detailed_library_usage.csv", index=False)

    # Generate missing libraries report
    missing_libraries = tracked_libraries - found_libraries
    missing_libraries_df = pd.DataFrame(
        [lib.split(":") for lib in missing_libraries], columns=["Library Type", "Library Name"]
    )
    missing_libraries_df.to_csv("missing_libraries_report.csv", index=False)

    print("Reports saved:")
    print("- library_usage_report.csv (summary)")
    print("- detailed_library_usage.csv (detailed list)")
    print("- missing_libraries_report.csv (libraries not found in any project)")

if __name__ == "__main__":
    generate_report()