# **Changelog Generator**

This script generates a changelog by categorising commits and merge requests from your GitLab project. It uses a YAML configuration file to define categories and patterns for matching commit messages. The changelog includes links to commits and merge requests, highlights breaking changes, and ensures deduplication of entries.

---

## **How to Use**

2. **Set Up the YAML Configuration**:
   - Use `categories.yml` to define categories, titles, regex patterns, and priorities.

   **Structure of `categories.yml`:**
   ```yaml
   categories:
     - name: "breaking"
       title: "⚠️ Breaking Changes"
       patterns:
         - "BREAKING CHANGE.*"
       priority: 1

     - name: "feature"
       title: "🚀 New features"
       patterns:
         - "^feature:.*"
       priority: 2

     - name: "fix"
       title: "🐞 Bug fixes"
       patterns:
         - "^fix:.*"
       priority: 3

     - name: "security"
       title: "🔒 Security updates"
       patterns:
         - "^security:.*"
         - "\\[Security\\] Bump .* from .* to .*"
       priority: 4

     - name: "dependabot"
       title: "📦 Dependency updates (Dependabot)"
       patterns:
         - "^dependabot/.*"
         - "^Bump .* from .* to .*"
       priority: 5
   ```

   **Explanation**:
   - `name`: Internal identifier for the category.
   
   - `title`: Display name in the changelog.
   
   - `patterns`: Regex patterns to match commit messages or merge request titles.
   
   - `priority`: Determines the order of categories in the changelog.

3. **Run the Script**:
   ```bash
   python changelog_generator.py
   ```

4. **Output**:
   - The changelog is appended to `CHANGELOG.md`.
   - If `CHANGELOG.md` doesn’t exist, it is created.

---

### **Examples**

#### **1. Sample Configuration in `categories.yml`**
```yaml
categories:
  - name: "breaking"
    title: "⚠️ Breaking Changes"
    patterns:
      - "BREAKING CHANGE.*"
    priority: 1

  - name: "feature"
    title: "🚀 New features"
    patterns:
      - "^feature:.*"
    priority: 2
```

---

#### **2. Sample Commits**

**Commits:**
- `BREAKING CHANGE: Update API endpoints`

- `feature: Add dashboard analytics`

- `fix: Resolve authentication bug`

- `[Security] Bump lodash from 4.17.15 to 4.17.21`

---

#### **3. Generated Changelog**

```markdown
## v1.2.0 Released on 2024-12-31 12:34:56 GMT

### ⚠️ Breaking Changes (1 change)

- [BREAKING CHANGE: Update API endpoints](https://gitlab.com/your-repo/-/commit/abc123) by John Doe
  (Merge request: [!42](https://gitlab.com/your-repo/-/merge_requests/42))

### 🚀 New features (1 change)

- [feature: Add dashboard analytics](https://gitlab.com/your-repo/-/commit/def456) by Jane Doe
  (Merge request: [!43](https://gitlab.com/your-repo/-/merge_requests/43))

### 🐞 Bug fixes (1 change)

- [fix: Resolve authentication bug](https://gitlab.com/your-repo/-/commit/ghi789) by Alice Johnson
  (Merge request: [!44](https://gitlab.com/your-repo/-/merge_requests/44))

### 🔒 Security updates (1 change)

- [Bump lodash from 4.17.15 to 4.17.21](https://gitlab.com/your-repo/-/commit/jkl101) by Dependabot
  (Merge request: [!45](https://gitlab.com/your-repo/-/merge_requests/45))
```

---

### **Notes**

- **Breaking Changes**: Highlighted at the top of the changelog.

- **Deduplication**: Prevents duplicate merge requests from appearing in the changelog.

- **Prioritisation**: Categories are processed in the order of their `priority` values in the YAML file.

- **Customisation**: Add or modify categories in `categories.yml` to suit your project.