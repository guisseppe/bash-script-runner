import gitlab
import os
import re
import yaml
from jsonschema import validate, ValidationError
from datetime import datetime
from pathlib import Path
from jinja2 import Template
import xml.etree.ElementTree as ET

# GitLab configuration
GITLAB_URL = "https://gitlab.com"
PRIVATE_TOKEN = os.getenv("GITLAB_PRIVATE_TOKEN")
PROJECT_ID = os.getenv("CI_PROJECT_ID")

# Load and validate categories from YAML
def load_categories(yaml_file="categories.yml", schema_file="categories-schema.json"):
    with open(schema_file, "r") as f:
        schema = yaml.safe_load(f)

    with open(yaml_file, "r") as f:
        data = yaml.safe_load(f)

    # Validate YAML against JSON Schema
    try:
        validate(instance=data, schema=schema)
    except ValidationError as e:
        raise ValueError(f"Invalid YAML schema: {e.message}")

    # Parse categories into regex patterns, ordered by priority
    categories = sorted(data["categories"], key=lambda c: c["priority"])
    category_dict = {}
    for category in categories:
        category_dict[category["name"]] = {
            "title": category["title"],
            "patterns": [re.compile(pattern, re.IGNORECASE) for pattern in category["patterns"]],
            "entries": [],
        }
    return category_dict

# Jinja2 changelog template
CHANGELOG_TEMPLATE = """
## {{ version_with_timestamp }}

{% for category, details in categories.items() if details.entries %}
### {{ details.title }} ({{ details.entries|length }} changes)

{% for entry in details.entries %}
- [{{ entry.title }}]({{ entry.commit_url }}) by {{ entry.author }}
  {% if entry.merge_request_url %} (Merge request: [{{ entry.merge_request_id }}]({{ entry.merge_request_url }})) {% endif %}
{% endfor %}

{% endfor %}
"""

def fetch_latest_tag(gl, project_id):
    """Fetch the latest tag of the project that does not end with '-aws'."""
    project = gl.projects.get(project_id)
    tags = project.tags.list(order_by="updated", sort="desc", all=True)
    for tag in tags:
        if not tag.name.endswith("-aws"):
            return tag
    return None

def fetch_commits_since_tag(gl, project_id, tag_name):
    """Fetch commits from the default branch since the given tag."""
    project = gl.projects.get(project_id)
    default_branch = project.default_branch

    if tag_name:
        commits = project.commits.list(ref_name=default_branch, since=tag_name.commit["created_at"])
    else:
        commits = project.commits.list(ref_name=default_branch, all=True)

    return commits

def fetch_merge_requests(gl, project_id, commit_id):
    """Fetch merge requests for a specific commit."""
    project = gl.projects.get(project_id)
    merge_requests = project.merge_requests.list(commit_sha=commit_id)
    return merge_requests

def categorise_commits(categories, gl, project_id, commits):
    """Categorise commits based on regex patterns in categories."""
    seen_merge_requests = set()

    for commit in commits:
        message = commit.message
        author = commit.author_name
        commit_url = commit.web_url

        # Match each commit against category patterns
        for category, details in categories.items():
            for pattern in details["patterns"]:
                if pattern.match(message):
                    # Fetch merge requests associated with the commit
                    merge_requests = fetch_merge_requests(gl, project_id, commit.id)
                    merge_request_url = (
                        merge_requests[0].web_url if merge_requests else None
                    )
                    merge_request_id = (
                        merge_requests[0].iid if merge_requests else None
                    )

                    # Deduplicate based on merge request ID
                    if merge_request_id and merge_request_id in seen_merge_requests:
                        continue
                    seen_merge_requests.add(merge_request_id)

                    details["entries"].append(
                        {
                            "title": message,
                            "author": author,
                            "commit_url": commit_url,
                            "merge_request_url": merge_request_url,
                            "merge_request_id": merge_request_id,
                        }
                    )
                    break

def get_version_with_timestamp():
    """Determine the version from known version files or fallback to timestamp."""
    timestamp = datetime.utcnow().strftime("Released on %Y-%m-%d %H:%M:%S GMT")
    if Path("package.json").exists():
        with open("package.json") as f:
            package_json = yaml.safe_load(f)
            version = package_json.get("version", None)
            if version:
                return f"v{version} {timestamp}"
    elif Path("pom.xml").exists():
        tree = ET.parse("pom.xml")
        root = tree.getroot()
        version = root.find("{http://maven.apache.org/POM/4.0.0}version")
        if version is not None and version.text.strip():
            return f"v{version.text.strip()} {timestamp}"
    elif Path(".VERSION").exists():
        with open(".VERSION") as f:
            version = f.read().strip()
            if version:
                return f"v{version} {timestamp}"
    # Fallback to timestamp-only if no version information is available
    return timestamp

def generate_changelog(version_with_timestamp, categories):
    """Render the changelog using Jinja2 template."""
    template = Template(CHANGELOG_TEMPLATE)
    return template.render(version_with_timestamp=version_with_timestamp, categories=categories)

def update_existing_changelog(new_content):
    """Update the existing changelog by appending new content."""
    changelog_file = Path("CHANGELOG.md")
    if changelog_file.exists():
        with open(changelog_file, "r") as file:
            existing_content = file.read()
        updated_content = existing_content.strip() + "\n\n" + new_content
    else:
        updated_content = new_content

    with open(changelog_file, "w") as file:
        file.write(updated_content)

def main():
    print("Initialising GitLab client...")
    gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)

    print("Loading categories from YAML...")
    categories = load_categories()

    print("Fetching latest tag...")
    latest_tag = fetch_latest_tag(gl, PROJECT_ID)
    if latest_tag:
        print(f"Latest tag found: {latest_tag.name}")
    else:
        print("No valid tags found. Fetching all commits.")

    print("Fetching commits since the last tag...")
    commits = fetch_commits_since_tag(gl, PROJECT_ID, latest_tag)

    print("Categorising commits...")
    categorise_commits(categories, gl, PROJECT_ID, commits)

    print("Determining version and timestamp...")
    version_with_timestamp = get_version_with_timestamp()

    print(f"Version with timestamp determined: {version_with_timestamp}")

    print("Generating changelog...")
    new_changelog_content = generate_changelog(version_with_timestamp, categories)

    print("Updating changelog file...")
    update_existing_changelog(new_changelog_content)

    print("Changelog updated successfully!")

if __name__ == "__main__":
    main()