def close_mrs_and_delete_branch(project, branch_name):
    """
    Close any open merge requests for the given branch and delete the branch if it exists.

    :param project: GitLab project object
    :param branch_name: Name of the branch to check and delete
    """
    # Find open merge requests for the branch
    merge_requests = project.mergerequests.list(state="opened", source_branch=branch_name)

    if merge_requests:
        for mr in merge_requests:
            print(f"🚨 Closing Merge Request #{mr.iid} for branch '{branch_name}'")
            mr.state_event = "close"
            mr.save()

    # Check if the branch exists
    try:
        project.branches.get(branch_name)
        print(f"🗑️ Deleting branch '{branch_name}'")
        project.branches.delete(branch_name)
    except gitlab.exceptions.GitlabGetError:
        print(f"✅ Branch '{branch_name}' does not exist, skipping deletion.")