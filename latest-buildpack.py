import json
import re
import sys

def get_latest_java_buildpack(file_path):
    """
    Reads a JSON file containing a list of PCF buildpacks and returns the latest Java buildpack.
    Exits with code 0 if no Java buildpacks are found.
    
    :param file_path: Path to the JSON file containing buildpacks.
    :return: The latest Java buildpack as a string, or exits with code 0 if none are found.
    """
    try:
        # Load the JSON file
        with open(file_path, "r", encoding="utf-8") as file:
            buildpacks = json.load(file)

        java_versions = []
        pattern = re.compile(r"java_buildpack_(\d+)_(\d+)(?:_(\d+))?")

        # Extract Java buildpack versions
        for buildpack in buildpacks:
            match = pattern.match(buildpack)
            if match:
                major, minor, patch = match.groups()
                major = int(major)
                minor = int(minor)
                patch = int(patch) if patch else 0  # Default patch to 0 if not present
                java_versions.append((major, minor, patch, buildpack))

        # If no Java buildpacks found, exit with code 0
        if not java_versions:
            print("No Java buildpack found.")
            sys.exit(0)

        # Get the latest Java buildpack based on major, minor, and patch
        latest_version = max(java_versions, key=lambda x: (x[0], x[1], x[2]))
        return latest_version[3]

    except (FileNotFoundError, json.JSONDecodeError) as e:
        print(f"Error: {e}", file=sys.stderr)
        sys.exit(1)