import json
import os

def remove_matching_keys(file_a_path, file_b_path):
    print(f"\nProcessing files: {file_a_path} and {file_b_path}\n")
    
    # Load JSON data from files
    with open(file_a_path, 'r') as file_a, open(file_b_path, 'r') as file_b:
        data_a = json.load(file_a)
        data_b = json.load(file_b)
    
    # Identify keys to remove
    keys_to_remove = set()
    for key, value in data_a.items():
        if key in data_b:
            print(f"[DEBUG] Removing key '{key}' as it exists in {file_b_path}\n")
            keys_to_remove.add(key)
        elif isinstance(value, str) and "ENC(" in value:
            print(f"[DEBUG] Removing key '{key}' due to ENC( pattern in its value\n")
            keys_to_remove.add(key)
    
    # Check for references to removed keys
    for key, value in data_a.items():
        if isinstance(value, str) and any(f"${{{k}}}" in value for k in keys_to_remove):
            print(f"[DEBUG] Removing key '{key}' as it references a removed key\n")
            keys_to_remove.add(key)
    
    # Remove identified keys
    filtered_data = {key: value for key, value in data_a.items() if key not in keys_to_remove}
    
    if len(keys_to_remove) > 0:
        # Overwrite the original file_a with the modified data
        with open(file_a_path, 'w', newline='\r\n') as output_file:
            json.dump(filtered_data, output_file, indent=4)
        print(f"Updated JSON saved to {file_a_path}\n")
        return True
    return False

def process_folders(base_directory):
    for root, dirs, files in os.walk(base_directory):
        print(f"\n[INFO] Processing folder: {root}\n")
        modified_files_count = 0
        config_files = [f for f in files if f.startswith("spring-application-") and f.endswith(".json")]
        for file in config_files:
            if "aws" not in file:
                file_suffix = file.replace("spring-application-", "").replace(".json", "")
                file_a_path = os.path.join(root, file)
                file_b_path = os.path.join(root, f"spring-application-aws{file_suffix}.json")
                if os.path.exists(file_b_path):
                    modified = remove_matching_keys(file_a_path, file_b_path)
                    if modified:
                        modified_files_count += 1
                else:
                    print(f"[WARNING] Skipping {file_a_path} as {file_b_path} does not exist\n")
        print(f"[INFO] Modified {modified_files_count} files in {root}\n")

def main():
    current_directory = os.getcwd()
    print(f"\n[INFO] Starting processing in directory: {current_directory}\n")
    process_folders(current_directory)
    print("[INFO] Processing complete.\n")

if __name__ == "__main__":
    main()