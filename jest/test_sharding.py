import os
import argparse
import sys

MAX_SHARDS = 4

def shard_test_list(file_path, num_shards):
    """Shards a test list file into multiple files.

    Args:
        file_path (str): Path to the test list file.
        num_shards (int): Number of shards to create.
    """

    with open(file_path, "r") as f:
        test_files = f.readlines()

    tests_per_shard = len(test_files) // num_shards
    remainder = len(test_files) % num_shards

    for shard_num in range(num_shards):
        start_index = shard_num * tests_per_shard
        end_index = start_index + tests_per_shard + (1 if shard_num < remainder else 0)
        shard_files = test_files[start_index:end_index]

        with open(f"test_list_shard_{shard_num + 1}.txt", "w") as shard_file:
            shard_file.writelines(shard_files)

def run_shard(shard_num, test_runner="jest"):
    """Runs a specific test shard.

    Args:
        shard_num (int): The shard number to run.
        test_runner (str, optional): The test runner command (defaults to 'jest').
    """

    shard_file = f"test_list_shard_{shard_num}.txt"
    if os.path.exists(shard_file):
        test_command = f"{test_runner} --testLocationInResults -i @\"{shard_file}\""
        print(f"Running test command: {test_command}") 
        os.system(test_command)
    else:
        print(f"Shard file not found: {shard_file}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Shard and run Jest tests.")
    parser.add_argument("test_list", help="Path to the test list file.")
    parser.add_argument("-s", "--shard", type=int, required=True, help="Shard number to run.")
    parser.add_argument("-n", "--num_shards", type=int, default=2, help="Number of shards to create.")
    parser.add_argument("-t", "--test_runner", default="jest", help="Test runner command.")
    args = parser.parse_args()

    if args.num_shards > MAX_SHARDS:
        print(f"Warning: Maximum allowed shards is {MAX_SHARDS}. Exiting.")
        sys.exit(1)

    shard_test_list(args.test_list, args.num_shards)
    run_shard(args.shard, args.test_runner)