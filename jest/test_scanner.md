# Test Scanner Configuration File

This file allows you to customize the behavior of the test file scanner script.

## Available Fields

> **Important Note:** If `test_scanner.json` is not found, the script will use its default settings.

```json
{
  "root_directory": "/your/custom/project/path",  
  "file_patterns": [".test.js", ".test.jsx", ".test.ts", ".test.tsx", ".spec.js"], 
  "exclude_dirs": ["node_modules", "build", "dist"], 
  "max_recursion_depth": 3 
}
```

### root_directory (string)
  -  The base directory to start the test file search. 
  -  **Overrides the script's default of using the current working directory.**
  -  *Example:* `"root_directory": "/home/my-username/projects/my-nodejs-app"`

### file_patterns (array of strings)
  -  File extensions to recognize as potential test files.
  -  **Defaults to:**  `[".test.js", ".test.jsx", ".test.ts", ".test.tsx"]`
  -  *Example:* `"file_patterns": [".test.js", ".spec.js"]`

### exclude_dirs (array of strings)
  -  Directory names to skip during the scan (useful for ignoring dependencies or build folders).
  -  **Defaults to:** `["node_modules", "build", "dist"]`
  -  *Example:* `"exclude_dirs": ["node_modules", ".next", "coverage"]`

### max_recursion_depth (integer)
  -  Limits how deep the script searches into subdirectories.
  -  **Optional.** Use for performance optimization in very large projects.
  -  *Example:* `"max_recursion_depth": 2`