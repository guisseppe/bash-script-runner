# Test Sharding and Execution Script

This script provides tools to shard your Jest test files and execute specific shards for parallel test running.

## Overview

* **Sharding:** Splits a "test_list.txt" file into multiple shard files for efficient distribution across parallel test runners.
* **Execution:** Runs a specified test shard using Jest.
* **Logging:** Provides logging of the executed test command for clarity.
* **Max Shards:** Logs a warning and exits if the requested number of shards exceeds the maximum limit.

## Usage

> **MAX_SHARDS (script variable):** Controls the maximum allowed number of shards (currently 4).

1. **Generate test list:** Ensure you have a "test_list.txt" file containing paths to your Jest test files. (This can be generated using a separate script.)

2. **Sharding:**
   ```bash
   python3 test_sharding.py test_list.txt -n <number_of_shards> 
   ```
   * Replaces `<number_of_shards>` with the desired number. 

3. **Run a Shard**
   ```bash
   python3 test_sharding.py test_list.txt -s <shard_number>
   ```
   *  Replaces `<shard_number>` with the shard you want to execute.

### Example

1. **Shard into 3 files:** `python3 test_sharding.py test_list.txt -n 3`

2. **Run shard 2:**  `python3 test_sharding.py test_list.txt -s 2`

**Customization**

* Modify the `MAX_SHARDS` variable within the script to change the maximum shard limit.
* The `-t` flag allows you to specify a test runner other than Jest if needed.