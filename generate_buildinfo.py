import os
import json
import argparse
from datetime import datetime, timezone

def get_prod_buckets():
    """Returns a list of bucket names that are considered production."""
    return ["prod", "devops-test-prod", "prd"]

def write_json_file(file_path: str, data: dict):
    """
    Writes the given dictionary as a JSON file to the specified file path.
    Creates any missing directories along the path.
    """
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, "w") as f:
        json.dump(data, f, indent=4)
    print(f"File written: {file_path}")

def generate_metadata_file(folder_path: str, bucket_name: str, version: str):
    """
    Generates metadata files based on bucket type:
        - `BUILDINFO.json` for non-production buckets.
        - `VERSION.json` for production buckets (only contains project name & version).
        - If the bucket contains 'nft' and is non-production, generates **both** files.
    """
    # Retrieve GitLab CI environment variables
    project_name = os.getenv("CI_PROJECT_NAME", "unknown")

    is_production = any(bucket_name.lower() == b.lower() for b in get_prod_buckets())
    is_nft = "nft" in bucket_name.lower() and not is_production

    # Define metadata for BUILDINFO.json
    buildinfo_metadata = {
        "project_name": project_name,
        "version": version,
        "commit_hash": os.getenv("CI_COMMIT_SHA", "unknown"),
        "branch_name": os.getenv("CI_COMMIT_REF_NAME", "unknown"),
        "deployed_by": os.getenv("GITLAB_USER_LOGIN", "unknown"),
        "timestamp": datetime.now(timezone.utc).isoformat()
    }

    # Define metadata for VERSION.json (only project name and version)
    version_metadata = {
        "project_name": project_name,
        "version": version
    }

    # Generate BUILDINFO.json for non-production or NFT buckets
    if not is_production or is_nft:
        buildinfo_path = os.path.join(folder_path, "BUILDINFO.json")
        write_json_file(buildinfo_path, buildinfo_metadata)

    # Generate VERSION.json for production or NFT buckets
    if is_production or is_nft:
        version_path = os.path.join(folder_path, "VERSION.json")
        write_json_file(version_path, version_metadata)

def main():
    parser = argparse.ArgumentParser(description="Generate metadata files for deployment based on bucket type.")
    parser.add_argument("--folder_path", required=True, help="Folder path where the metadata files will be saved.")
    parser.add_argument("--bucket_name", required=True, help="Name of the bucket.")
    parser.add_argument("--version", required=True, help="Version of the build.")
    args = parser.parse_args()

    generate_metadata_file(args.folder_path, args.bucket_name, args.version)

if __name__ == "__main__":
    main()