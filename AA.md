# **How to Implement a Preload Redirect in Vite & Create React App for AWS Migration**

## 📌 Overview  
This guide helps **UI developers** implement a **preload redirect** in both **Vite** and **Create React App (CRA)** to support the AWS migration.  

The redirect will:  
✅ Detect if the hostname contains `spa.`  
✅ Replace it with `static.`, **preserving paths and query parameters**  
✅ Use an **environment variable (`VITE_ENABLE_REDIRECT` or `REACT_APP_ENABLE_REDIRECT`)** as a **feature flag**  
✅ Run **before** the React app loads  

---

# **🛠️ Vite Setup**  

## **Step 1: Set Up the Environment Variable**
Create a `.env` file in the root of your Vite project:  

```env
VITE_ENABLE_REDIRECT=true
```

This allows you to enable or disable the redirect **without modifying the code**.

---

## **Step 2: Modify `src/main.jsx` (JavaScript)**
In **Vite**, modify `src/main.jsx` **before React initializes**:

```javascript
// Ensure the redirect happens before React loads
const enableRedirect = import.meta.env.VITE_ENABLE_REDIRECT === "true";

if (enableRedirect && window.location.hostname.includes("spa.")) {
  const newUrl = window.location.href.replace("spa.", "static.");
  window.location.replace(newUrl);
}

// Import React & the App only if not redirected
import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
```

---

## **Step 3: Modify `src/main.tsx` (TypeScript)**
For **TypeScript**, update `src/main.tsx`:

```typescript
// Ensure the redirect happens before React loads
const enableRedirect = import.meta.env.VITE_ENABLE_REDIRECT === "true";

if (enableRedirect && window.location.hostname.includes("spa.")) {
  const newUrl = window.location.href.replace("spa.", "static.");
  window.location.replace(newUrl);
}

// Import React & the App only if not redirected
import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
```

---

## **Step 4: Run the App**
Restart Vite for the changes to take effect:

```sh
npm run dev
```

If `VITE_ENABLE_REDIRECT` is **true**, the redirection **will happen before React loads**.

---

# **🏗️ Create React App (CRA) Setup**
If your app was created with **Create React App (CRA)** instead of Vite, follow these steps:

## **Step 1: Set Up the Environment Variable**
Create a `.env` file in the root of your CRA project:

```env
REACT_APP_ENABLE_REDIRECT=true
```

---

## **Step 2: Modify `src/index.js` (JavaScript)**
In **CRA**, modify `src/index.js`:

```javascript
// Ensure the redirect happens before React loads
const enableRedirect = process.env.REACT_APP_ENABLE_REDIRECT === "true";

if (enableRedirect && window.location.hostname.includes("spa.")) {
  const newUrl = window.location.href.replace("spa.", "static.");
  window.location.replace(newUrl);
}

// Import React & the App only if not redirected
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
```

---

## **Step 3: Modify `src/index.tsx` (TypeScript)**
For **TypeScript**, update `src/index.tsx`:

```typescript
// Ensure the redirect happens before React loads
const enableRedirect = process.env.REACT_APP_ENABLE_REDIRECT === "true";

if (enableRedirect && window.location.hostname.includes("spa.")) {
  const newUrl = window.location.href.replace("spa.", "static.");
  window.location.replace(newUrl);
}

// Import React & the App only if not redirected
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
```

---

## **Step 4: Run the CRA App**
Restart the React dev server:

```sh
npm start
```

---

# **✅ Summary**
| Feature  | Vite | Create React App |
|----------|------|----------------|
| **Env Variable** | `VITE_ENABLE_REDIRECT` | `REACT_APP_ENABLE_REDIRECT` |
| **Modify File** | `src/main.jsx` / `src/main.tsx` | `src/index.js` / `src/index.tsx` |
| **Import Condition** | Uses `import.meta.env` | Uses `process.env` |
| **Restart Command** | `npm run dev` | `npm start` |

This ensures a seamless transition from `spa.` to `static.` domains as part of our **AWS migration strategy**.

---

This guide will help UI developers handle the AWS migration **without breaking user navigation**. Let me know if you need refinements! 🚀