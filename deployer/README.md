# Deployment Script Documentation

## Table of Contents
1. [Usage](#usage)
2. [Flags](#flags)
3. [Examples](#examples)
4. [Secrets Management](#secrets-management)
5. [Warning](#warning)
6. [HTML Audit](#html-audit)
7. [Troubleshooting](#troubleshooting)
8. [Frequently Asked Questions](#frequently-asked-questions)

## Usage

```bash
./deployment_script.sh [--audit] [--dry-run] [--unmask] [--time] [--html] [--help] deployment_file secrets_file
```

## Flags

- `--audit`: Create an audit file with each executed line as a title and a code block containing the output of that execution.
- `--dry-run`: Print out the commands that would be executed, but don't actually execute them.
- `--unmask`: Print out the values of the environment variables. Note: This can be a security risk. Use with caution.
- `--time`: Log the execution time of each command.
- `--html`: Generate an HTML audit file instead of markdown.
- `--help`: Print a help manual and exit.

## Examples

### Dry Run

```bash
./deployment_script.sh --dry-run deployment_file secrets_file
```

This will print out the commands that would be executed, but it won't actually execute them.

**Output Example:**

```bash
Would execute: echo "Hello, World!"
Would execute: ls -l
```

### Audit

```bash
./deployment_script.sh --audit deployment_file secrets_file
```

This will create an audit markdown file with each executed line as a title and a code block containing the output of that execution.

**Output Example:**

```markdown
## Line 1: echo "Hello, World!"

```bash
Hello, World!
```

## Line 2: ls -l

```bash
total 0
-rw-r--r-- 1 user group 0 Mar 29 12:34 deployment_file
-rw-r--r-- 1 user group 0 Mar 29 12:34 secrets_file
```
```

### Time

```bash
./deployment_script.sh --time deployment_file secrets_file
```

This will log the execution time of each command.

**Output Example:**

```bash
Executing line 1: echo "Hello, World!"
Execution time: 0 seconds
Executing line 2: ls -l
Execution time: 0 seconds
```

## Secrets Management

The script loads secrets from a file into environment variables. This can be a security risk if not handled properly. Here are some best practices for managing secrets:

- Never commit your secrets file to version control. Add it to your `.gitignore` file.
- Restrict access to your secrets file. Set its permissions so that only the necessary users can read it.
- Regularly rotate your secrets. This reduces the risk if a secret is compromised.
- Consider using a secrets management tool or service for more robust security.

## Warning

The `--unmask` flag will print out the values of the environment variables. This can be a security risk as it may expose sensitive information in logs or on the console. Use this flag with caution.

**Output Example:**

```bash
Loading secret: SECRET_KEY=MySecretKey
```

## HTML Audit

The `--html` flag will generate an HTML audit file instead of markdown. The HTML audit file will contain a table of contents, each executed line as a title, and a preformatted block containing the output of that execution. If a command fails, it will highlight the failure in the HTML file. This flag should be used in conjunction with the `--audit` flag.

```bash
./deployment_script.sh --audit --html deployment_file secrets_file
```

This will create an HTML audit file with Bootstrap CSS for a modern look. Each executed line will be a title and a preformatted block containing the output of that execution. If a command fails, it will highlight the failure in the HTML file.

## Troubleshooting

Here are some common issues and their solutions:

- **Issue:** The script fails with "Error: Deployment file not found."
  - **Solution:** Check that the deployment file exists and that the path is correct.
- **Issue:** The script fails with "Error: Secrets file not found."
  - **Solution:** Check that the secrets file exists and that the path is correct.
- **Issue:** The script fails with "Error: Invalid key in secrets file: $key"
  - **Solution:** Check that all keys in the secrets file are valid. Keys must consist only of alphanumeric characters and underscores.

## Frequently Asked Questions

- **Question:** Can I use this script with other shell environments?
  - **Answer:** This script is written for Bash. It may work in other shell environments, but it has not been tested.
- **Question:** How can I see the values of the environment variables?
  - **Answer:** Use the `--unmask` flag to print out the values of the environment variables. Note: This can be a security risk. Use with caution.
- **Question:** How can I see how long each command takes to execute?
  - **Answer:** Use the `--time` flag to log the execution time of each command.
- **Question:** How can I generate an HTML audit file instead of markdown?
  - **Answer:** Use the `--html` flag in conjunction with the `--audit` flag to generate an HTML audit file.