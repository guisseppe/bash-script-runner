**Filename: deployment.sh**

**Purpose**

This Bash script is designed to automate and streamline deployment processes. Key features include:

* **Flexibility:** Handles various command-line flags for customization.
* **Auditing:** Creates audit logs (markdown or HTML), recording command execution and output for review.
* **Error Handling:** Provides options to continue, rollback, or exit the deployment process upon execution errors.
* **Security:** Unsets environment variables containing secrets after use.

**Command-Line Flags**

* **--audit** 
   - Generates a detailed log with each executed line as a heading (Markdown or HTML format). The output of each command is included as a code block.

* **--dry-run** 
   - Ideal for testing; displays the commands that would be executed without actual execution.  

* **--unmask**
   - Prints secret variable values loaded from the secrets file. **Caution:** This presents a potential security risk.

* **--time** 
   - Tracks and logs the execution time of each command.

* **--html**
    - Generates an HTML audit file instead of Markdown.

* **--help**  
   - Displays a detailed help manual.

**Script Breakdown**

1. **Shebang:** `#!/bin/bash` - Specifies Bash as the interpreter.

2. **Variable Declarations:**
   - `LOGFILE`    : Stores the name of the script's log file.
   - `AUDITFILE`  : Stores the name of the audit file (Markdown).
   - `HTMLFILE`   : Stores the name of the audit file (HTML).

3. **Flag Handling:**
   - Uses a `while` loop and a `case` statement to process command-line flags, setting corresponding variables.

4. **Help Manual Display:**
   - When the `--help` flag is used, prints a detailed help manual and exits.

5. **File Validation:**
   - Checks if the deployment file and secrets file exist. Exits with an error if either is missing.

6. **Loading Secrets:**
   - Reads the secrets file, exports variables using the `key=value` format.
   - Skips commented lines (`###*`).
   - Validates that keys contain only alphanumeric characters and underscores.

7. **Main Execution Loop:**
   - Reads the deployment file line by line.
   - Ignores comments (`###*`) and empty lines.
   - Replaces `@variable` placeholders with corresponding environment variable values.
   - Executes the processed command in the `eval $line` statement.
   - Provides interactive options in the case of execution errors (continue, rollback, or exit).
   - Handles various post-execution actions based on the flags used:
     * `--audit`: Logs the command and output.
     * `--dry-run`: Logs what *would* be executed.
     * `--unmask`: Logs secrets (if the flag is set).
     * `--time`: Logs execution time.  

8. **Secrets Cleanup:**
    - Iterates over imported secret variables and unsets them.

9. **Success Message**

10. **Audit File Finalization (`--audit` flag is set)**
    - Adds a Table of Contents to the audit file for easier navigation. The ToC is dynamically generated depending on whether an HTML or Markdown audit file is created.