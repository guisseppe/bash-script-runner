#!/bin/bash

# Define log file
LOGFILE="deployment.log"
AUDITFILE="audit.md"
HTMLFILE="audit.html"

# Check for flags
AUDIT=0
DRYRUN=0
UNMASK=0
TIME=0
HELP=0
HTML=0
while (( "$#" )); do
  case "$1" in
    --audit)
      AUDIT=1
      shift
      ;;
    --dry-run)
      DRYRUN=1
      shift
      ;;
    --unmask)
      UNMASK=1
      shift
      ;;
    --time)
      TIME=1
      shift
      ;;
    --html)
      HTML=1
      shift
      ;;
    --help)
      HELP=1
      shift
      ;;
    *) 
      break
      ;;
  esac
done

# Print help manual and exit
if [ $HELP -eq 1 ]; then
  echo "Usage: $0 [--audit] [--dry-run] [--unmask] [--time] [--html] [--help] deployment_file secrets_file"
  echo "--audit: Create an audit markdown file with each executed line as a title and a code block containing the output of that execution."
  echo "--dry-run: Print out the commands that would be executed, but don't actually execute them."
  echo "--unmask: Print out the values of the environment variables. Note: This can be a security risk. Use with caution."
  echo "--time: Log the execution time of each command."
  echo "--html: Generate an HTML audit file instead of markdown."
  echo "--help: Print this help manual and exit."
  exit 0
fi

# Check if the deployment file exists
if [ ! -f "$1" ]; then
    echo -e "\033[31mError: Deployment file not found.\033[0m" | tee -a $LOGFILE
    exit 1
fi

# Check if the secrets file exists
if [ ! -f "$2" ]; then
    echo -e "\033[31mError: Secrets file not found.\033[0m" | tee -a $LOGFILE
    exit 1
fi

# Load secrets into environment variables
while IFS='=' read -r key value; do
    if [[ $key != \#* ]]; then
        # Check if the key contains special characters
        if [[ $key =~ [^a-zA-Z0-9_] ]]; then
            echo -e "\033[31mError: Invalid key in secrets file: $key\033[0m" | tee -a $LOGFILE
            exit 1
        fi
        export $key=$value
    fi
done < "$2"

# Process each line in the deployment file
line_number=0
while IFS= read -r line; do
    line_number=$((line_number+1))
    # Ignore comments
    if [[ $line == \#\#\#* ]]; then
        echo "Ignoring comment at line $line_number" | tee -a $LOGFILE
        continue
    fi
    # Ignore empty lines
    if [[ -z "$line" ]]; then
        echo "Skipping empty line at line $line_number" | tee -a $LOGFILE
        continue
    fi
    # Replace @word with corresponding environment variable
    line=$(echo $line | sed -e 's/@\([a-zA-Z_][a-zA-Z_0-9]*\)/${\1}/g')
    # Execute the command
    echo "Executing line $line_number: $line" | tee -a $LOGFILE
    eval $line
    if [ $? -ne 0 ]; then
        echo -e "\033[31mError executing line $line_number: $line\033[0m" | tee -a $LOGFILE
        read -p "Do you want to continue with the next command? (y/n) " answer
        case ${answer:0:1} in
            y|Y )
                echo "Continuing with the next command..." | tee -a $LOGFILE
            ;;
            * )
                read -p "Do you want to rollback? (y/n) " answer
                case ${answer:0:1} in
                    y|Y )
                        echo "Executing rollback..." | tee -a $LOGFILE
                        while IFS= read -r rollback_line; do
                            echo "Executing rollback command: $rollback_line" | tee -a $LOGFILE
                            eval $rollback_line
                        done < "rollback.txt"
                    ;;
                    * )
                        echo "Exiting..." | tee -a $LOGFILE
                        exit 1
                    ;;
                esac
            ;;
        esac
    fi
    # After each command execution
    if [ $AUDIT -eq 1 ]; then
        if [ $HTML -eq 1 ]; then
            echo -e "<h2>Line $line_number: $line</h2><pre>$(cat $LOGFILE)</pre>" >> $HTMLFILE
            if [ $? -ne 0 ]; then
                echo -e "<p class='text-danger'>Error executing line $line_number: $line</p>" >> $HTMLFILE
            fi
        else
            echo -e "## Line $line_number: $line\n\`\`\`bash\n$(cat $LOGFILE)\n\`\`\`" >> $AUDITFILE
            if [ $? -ne 0 ]; then
                echo -e "\n> :warning: **Error executing line $line_number: $line**" >> $AUDITFILE
            fi
        fi
    fi
    # Before each command execution
    if [ $DRYRUN -eq 1 ]; then
        echo "Would execute: $line"
        continue
    fi
    # Before loading secrets into environment variables
    if [ $UNMASK -eq 1 ]; then
        echo "Loading secret: $key=$value"
    fi
    # Before and after each command execution
    if [ $TIME -eq 1 ]; then
        start_time=$(date +%s)
        # Execute the command
        end_time=$(date +%s)
        echo "Execution time: $(($end_time - $start_time)) seconds"
    fi
done < "$1"

# Unset environment variables
while IFS='=' read -r key value; do
    if [[ $key != \#* ]]; then
        unset $key
    fi
done < "$2"

echo "Deployment completed successfully." | tee -a $LOGFILE

# At the end of the script
if [ $AUDIT -eq 1 ]; then
    if [ $HTML -eq 1 ]; then
        echo -e "<!DOCTYPE html>\n<html>\n<head>\n<title>Audit Report</title>\n<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>\n</head>\n<body>\n<div class='container'>\n<h1>Table of Contents</h1>\n<ul>" > temp.html
        grep -n "^<h2> " $HTMLFILE | sed 's/^\([0-9]*\):<h2> \(.*\)<\/h2>$/<li><a href="#\1">\2<\/a><\/li>/' >> temp.html
        echo -e "</ul><hr>" >> temp.html
        cat $HTMLFILE >> temp.html
        echo -e "</div>\n</body>\n</html>" >> temp.html
        mv temp.html $HTMLFILE
    else
        echo -e "# Table of Contents\n" > temp.md
        grep -n "^## " $AUDITFILE | sed 's/^\([0-9]*\):## \(.*\)$/1. \2/' >> temp.md
        echo -e "\n---\n" >> temp.md
        cat $AUDITFILE >> temp.md
        mv temp.md $AUDITFILE
    fi
fi