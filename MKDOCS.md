# Contributing to MkDocs Documentation

This guide will help you add new content, use MkDocs features, and ensure your changes meet the required standards before merging.

## 📂 Structure

All documentation and configuration files are located in the `static-site/` directory.

- `static-site/docs/` – Markdown files for documentation.

- `static-site/mkdocs.yml` – Configuration file for MkDocs.

### 🏗️ Auto-Generated Navigation

MkDocs automatically generates the site structure based on the directory structure of the `docs/` folder. Folders without Markdown files are ignored. The `index.md` file in any directory serves as the landing page for that section.

You can control the order of folders and files by prefixing them with numbers and a dash (e.g., `01-introduction.md`, `02-setup.md`). These prefixes are stripped from the final navigation display but help organize content in a structured way.

## ✍️ Adding New Content

To add a new page:

1. Create a new Markdown (`.md`) file inside the `docs/` directory.
2. Use **Markdown features** like headings, lists, and tables to structure your content.

### 📌 MkDocs Features

#### 1️⃣ Admonitions (Callouts)

Admonitions help highlight important information. Use them like this:

````markdown
!!! note
    This is a note.

!!! warning "Caution!"
    Be careful when making changes.
````

> **Note: Follow to this [link to learn more MkDocs Admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/?h=colla#admonitions)

#### 2️⃣ Mermaid Diagrams

To add diagrams using Mermaid:

````markdown
``` mermaid
graph TD;
    A --> B;
    B --> C;
```
````

#### 3️⃣ Linking Between Pages

To link to another page:

```markdown
[Go to another page](another-page.md)
```

Use relative paths without `.md` in `mkdocs.yml` links.

#### 4️⃣ Code Blocks

Use fenced code blocks to format code:

````markdown
```python
print("Hello, MkDocs!")
```
````

> **Note:** You can highlight specific lines of code and more. Click this [link to learn more.](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/)

## 🔍 Reviewing Documentation Merge Requests

Before merging a documentation update:

1. Ensure the **build job passes**.
2. If the build fails, check the **job logs** for Markdown or YAML syntax errors.
3. Once merged the pipeline, will deploy the static to S3. You can view the site via this link

## 📚 Further Reading

- [MkDocs Official Documentation](https://www.mkdocs.org/)
- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)

Happy documenting! 🚀