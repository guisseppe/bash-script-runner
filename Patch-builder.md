# Json Patch Builder

The `JsonPatchBuilder` simplifies the creation of JSON Patches, which are used to modify JSON documents according to the [RFC 6902 specification](https://datatracker.ietf.org/doc/html/rfc6902).

## Dependencies

```xml
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.17.2</version>
</dependency>

<dependency>
    <groupId>com.github.java-json-tools</groupId>
    <artifactId>json-patch</artifactId>
    <version>1.13</version>
    <exclusions>
        <exclusion>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

## Builder

```java
package org.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.jsonpointer.JsonPointer;
import com.github.fge.jackson.jsonpointer.JsonPointerException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchOperation;
import com.github.fge.jsonpatch.ReplaceOperation;
import com.github.fge.jsonpatch.AddOperation;
import com.github.fge.jsonpatch.RemoveOperation;
import com.github.fge.jsonpatch.MoveOperation;
import com.github.fge.jsonpatch.CopyOperation;
import com.github.fge.jsonpatch.TestOperation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class JsonPatchBuilder {

    private final List<JsonPatchOperation> operations = new ArrayList<>();

    public static JsonPatchBuilder builder() {
        return new JsonPatchBuilder();
    }

    public JsonPatchBuilder add(String path, JsonNode value) throws JsonPointerException {
        operations.add(new AddOperation(new JsonPointer(path), value));
        return this;
    }

    public JsonPatchBuilder remove(String path) throws JsonPointerException {
        operations.add(new RemoveOperation(new JsonPointer(path)));
        return this;
    }

    public JsonPatchBuilder replace(String path, JsonNode value) throws JsonPointerException {
        operations.add(new ReplaceOperation(new JsonPointer(path), value));
        return this;
    }

    public JsonPatchBuilder move(String from, String path) throws JsonPointerException {
        operations.add(new MoveOperation(new JsonPointer(from), new JsonPointer(path)));
        return this;
    }

    public JsonPatchBuilder copy(String from, String path) throws JsonPointerException {
        operations.add(new CopyOperation(new JsonPointer(from), new JsonPointer(path)));
        return this;
    }

    public JsonPatchBuilder test(String path, JsonNode value) throws JsonPointerException {
        operations.add(new TestOperation(new JsonPointer(path), value));
        return this;
    }

    public JsonPatch build() {
        //test operations moved to the front of the array
        operations.sort(Comparator.comparing(op -> op instanceof TestOperation ? 0 : 1));
        return new JsonPatch(operations);
    }
}
```

## Example Operations

### Add Operation

Adds the `address` object to the JSON.

**Before:**

```json
{
    "name": "John Doe"
}
```

**Operation:**

```java
JsonPatch patch = JsonPatchBuilder.builder()
    .add("/address", objectMapper.valueToTree(new Address("123 Main St", "Anytown", "CA", "12345")))
    .build();
```

**After:**

```json
{
    "name": "John Doe",
    "address": {
    "street": "123 Main St",
    "city": "Anytown",
    "state": "CA",
    "zip": "12345"
    }
}
```

### Remove Operation

Removes `phoneNumber` field from the JSON.

**Before:**

```json
{
    "name": "John Doe",
    "phoneNumber": "555-1234"
}
```
**Operation:**

```java
JsonPatch patch = JsonPatchBuilder.builder()
    .remove("/phoneNumber")
    .build();
```

**After:**

```json
{
    "name": "John Doe"
}
```

### Replace Operation

Update the `name` to `Jane Smith`

**Before:**

```json
{
    "name": "John Doe"
}
```

**Operation:**

```java
JsonPatch patch = JsonPatchBuilder.builder()
    .replace("/name", objectMapper.valueToTree("Jane Smith"))
    .build();
```

**After:**

```json
{
    "name": "Jane Smith"
}
```

### Move Operation

Moves the value of `phoneNumber` to `contact/phone`

**Before:**

```json
{
    "name": "John Doe",
    "phoneNumber": "555-1234"
}
```

**Operation:**

```java
JsonPatch patch = JsonPatchBuilder.builder()
    .move("/phoneNumber", "/contact/phone")
    .build();
```

**After:**

```json
{
    "name": "John Doe",
    "contact": {
    "phone": "555-1234"
    }
}
```

### Copy Operation

Copies the value of `name` to `displayName`

**Before:**

```json
{
    "name": "John Doe"
}
```

**Operation**

```java
JsonPatch patch = JsonPatchBuilder.builder()
    .copy("/name", "/displayName")
    .build();
```

**After:**

```json
{
    "name": "John Doe",
    "displayName": "John Doe"
}
```

### Test Operation

Asserts that `age` is 30 (If not, the patch application fails.) then updates the value of the `age` field to `31`.

**Before:**

```json
{
    "name": "John Doe",
    "age": 30
}
```

**Operation:**

```java
JsonPatch patch = JsonPatchBuilder.builder()
    .test("/age", objectMapper.valueToTree(30))
    .replace("/age", objectMapper.valueToTree(31)) // This will only execute if the test passes
    .build();
```

**After:**

```json
{
    "name": "John Doe",
    "age": 31
}
```