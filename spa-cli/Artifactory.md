

```java
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class provides functionality to zip a directory, generate checksum files for the zip,
 * combine all files into a final zip, and upload it to an Artifactory repository.
 */
public class ZipAndUploadToArtifactory {

    /**
     * Main entry point for the program. Zips a directory, creates checksum files, combines them,
     * and uploads the result to Artifactory.
     *
     * @param args Command-line arguments (not used in this implementation)
     * @throws Exception If an error occurs during zipping, checksum creation, or uploading.
     */
    public static void main(String[] args) throws Exception {

        String directoryToZip = "your_directory";
        String zipFilename = "archive.zip";
        String artifactoryUrl = "https://your-artifactory-server/artifactory/your-repo";
        String artifactoryUsername = "your_username";
        String artifactoryPassword = "your_password";

        // Create initial ZIP
        zipDirectory(directoryToZip, zipFilename);

        // Generate checksums
        createChecksumFile(zipFilename, "SHA-1", zipFilename + ".sha1");
        createChecksumFile(zipFilename, "MD5", zipFilename + ".md5");

        // Create final ZIP with checksums
        String finalZipFilename = "final_archive.zip";
        zipFiles(new String[]{zipFilename, zipFilename + ".sha1", zipFilename + ".md5"}, finalZipFilename);

        // Upload to Artifactory
        uploadToArtifactory(finalZipFilename, artifactoryUrl, artifactoryUsername, artifactoryPassword);
    }

    /**
     * Zips a directory recursively.
     *
     * @param sourceDir The directory to zip.
     * @param zipFile   The name of the zip file to create.
     * @throws IOException If an I/O error occurs during zipping.
     */
    private static void zipDirectory(String sourceDir, String zipFile) throws IOException {
        Path sourceDirPath = Paths.get(sourceDir);
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile))) {
            Files.walk(sourceDirPath)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        ZipEntry zipEntry = new ZipEntry(sourceDirPath.relativize(path).toString());
                        try {
                            zos.putNextEntry(zipEntry);
                            Files.copy(path, zos);
                            zos.closeEntry();
                        } catch (IOException e) {
                            System.err.println("Error zipping file: " + path + " - " + e.getMessage());
                        }
                    });
        }
    }

    /**
     * Creates a checksum file for a given file using the specified algorithm.
     *
     * @param filename   The name of the file to generate the checksum for.
     * @param algorithm The checksum algorithm to use (e.g., "SHA-1", "MD5").
     * @param checksumFile The name of the file to write the checksum to.
     * @throws Exception If an error occurs during checksum calculation or file writing.
     */
    private static void createChecksumFile(String filename, String algorithm, String checksumFile) throws Exception {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        try (InputStream is = Files.newInputStream(Paths.get(filename));
             DigestInputStream dis = new DigestInputStream(is, md)) {
            byte[] buffer = new byte[8192];
            while (dis.read(buffer) != -1) ; // Read through the file
        }
        byte[] digest = md.digest();
        try (PrintWriter pw = new PrintWriter(checksumFile)) {
            pw.println(Base64.getEncoder().encodeToString(digest));
        }
    }

    /**
     * Zips multiple files into a single archive.
     *
     * @param files   The array of file names to include in the zip archive.
     * @param zipFile The name of the zip file to create.
     * @throws IOException If an I/O error occurs during zipping.
     */
    private static void zipFiles(String[] files, String zipFile) throws IOException {
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile))) {
            for (String file : files) {
                zos.putNextEntry(new ZipEntry(new File(file).getName()));
                Files.copy(Paths.get(file), zos);
                zos.closeEntry();
            }
        }
    }

    /**
     * Uploads a file to an Artifactory repository using Unirest.
     *
     * @param filename       The name of the file to upload.
     * @param url           The URL of the Artifactory repository.
     * @param username      The username for Artifactory authentication.
     * @param password      The password for Artifactory authentication.
     * @throws Exception If an error occurs during the upload process.
     */
    private static void uploadToArtifactory(String filename, String url, String username, String password) throws Exception {
        HttpResponse<String> response = Unirest.put(url + "/" + new File(filename).getName())
                .basicAuth(username, password)
                .body(new File(filename))
                .asString();

        if (!response.isSuccess()) {
            throw new IOException("Failed to upload to Artifactory. Response: " + response.getBody());
        }
    }
}

```