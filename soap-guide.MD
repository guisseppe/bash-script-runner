# **Building a Multi-Tenant Spring SOAP Service**

This guide demonstrates how to create a Spring Boot application that interacts with multiple SOAP services based on tenant identification.

> **Disclaimer:** Note this guide focuses solely on demonstrating how to connect to a SOAP service in Spring Boot. Some best practices regarding project setup have been omitted to keep this guide simple and easy to follow.

## **Table of Contents**

[[_TOC_]]

## **Add Dependencies**

```xml
<!-- Spring Web Services for SOAP client support -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web-services</artifactId>
</dependency>

<!-- JAXB for XML Binding (Marshalling and Unmarshalling) -->
<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
    <version>2.3.1</version>
</dependency>

<!-- Apache HTTP Components for custom message sending -->
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient</artifactId>
</dependency>
```

## **Generating SOAP Client Code Using jaxws-maven-plugin**

Instead of manually running `wsimport`, we use the `jaxws-maven-plugin` to automatically generate SOAP client classes from the WSDL during the Maven compile lifecycle.

### **Example pom.xml Configuration**

```xml
<build>
    <plugins>
        <plugin>
            <groupId>com.sun.xml.ws</groupId>
            <artifactId>jaxws-maven-plugin</artifactId>
            <version>4.0.3</version>
            <executions>
                <execution>
                    <id>demo-wsdl</id>
                    <goals>
                        <goal>wsimport</goal>
                    </goals>
                    <configuration>
                        <packageName>demo.spring.sandbox</packageName>
                        <wsdlDirectory>${project.basedir}/src/main/resources/wsdl/demo</wsdlDirectory>
                        <sourceDestDir>${project.build.directory}/generated-sources/</sourceDestDir>
                    </configuration>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

Run the Maven build with `mvn clean install` to generate the SOAP client classes in the specified package.

### **Example Generated Classes**

```java
package com.example.model.soap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CreateUserRequest")
public class CreateUserRequest {

    private String firstName;
    private String lastName;

    @XmlElement(required = true)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @XmlElement(required = true)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
```

> **Note:** The actual generated classes will depend on the structure defined in your WSDL file.

## Multi-Tenant Configuration

## **Configuring JAXB for Marshalling and Unmarshalling**

**Marshalling** and **Unmarshalling** are processes of converting between Java objects and XML representations, which are essential when dealing with SOAP services.

- **Marshalling:** Converting Java objects to XML format.

- **Unmarshalling:** Converting XML data back to Java objects.

We use **JAXB (Java Architecture for XML Binding)** for this purpose.

### **Configuring `Jaxb2Marshaller`**

> **JAXB** provides a convenient way to bind XML schemas and Java representations, making it easier to process XML data in Java applications.

Here’s how to configure multiple `WebServiceTemplate` beans for different SOAP services using a single configuration class.

> **Note:** It is best practice to store service URI and Context Paths in your Spring properties.

#### **SoapConfig Class**

```java
@Configuration
public class SoapConfig {

    @Bean
    @Primary
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setDefaultUri("https://www.w3schools.com/xml/tempconvert.asmx");

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("demo.spring.sandbox.model.soap.temp");
        webServiceTemplate.setMarshaller(marshaller);
        webServiceTemplate.setUnmarshaller(marshaller);

        webServiceTemplate.setMessageSender(new HttpComponentsMessageSender(HttpClients.createDefault()));
        return webServiceTemplate;
    }

    @Bean("calcWebServiceTemplate")
    public WebServiceTemplate calcWebServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setDefaultUri("http://www.dneonline.com/calculator.asmx");

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("demo.spring.sandbox.model.soap.calc");
        webServiceTemplate.setMarshaller(marshaller);
        webServiceTemplate.setUnmarshaller(marshaller);

        webServiceTemplate.setMessageSender(new HttpComponentsMessageSender(HttpClients.createDefault()));
        return webServiceTemplate;
    }
}
```

### **Marshalling and Unmarshalling Process**

**Marshalling:**

- When sending a request to the SOAP service, the `Jaxb2Marshaller` converts the Java request object (`CreateUserRequest`) into the appropriate XML format as defined by the SOAP service's schema.

**Unmarshalling:**

- Upon receiving the response from the SOAP service, the `Jaxb2Marshaller` converts the XML response back into a Java object (`CreateUserResponse`), making it easier to work with in the application code.

## **Setting up the SOAP Client**

We now set up a SOAP client that utilizes the configured `Jaxb2Marshaller` to communicate with the SOAP service.

Here’s the `CalculatorClient` class, which uses the `calcWebServiceTemplate` to perform SOAP operations with a `SoapActionCallback`.

```java
@Component
public class CalculatorClient {

    @Autowired
    @Qualifier("calcWebServiceTemplate")
    private WebServiceTemplate wsTemplate;

    public Integer add(Add add) {
        AddResponse response = (AddResponse) wsTemplate
                .marshalSendAndReceive(add, new SoapActionCallback("http://tempuri.org/Add"));
        return response.getAddResult();
    }
}
```

### **Explanation**

- **`marshalSendAndReceive`:** Sends the request to the SOAP service and processes the response.

- **`SoapActionCallback`:** Specifies the SOAP action to be used during the request.


### Tenant Context
The `TenantContext` class is a simple utility to store the current tenant for each request using `ThreadLocal`. This is useful because `ThreadLocal` variables are isolated per thread.

```java
public class TenantContext {

    private static final ThreadLocal<String> currentTenant = new ThreadLocal<>();

    // Set tenant ID for the current thread (request)
    public static void setTenant(String tenant) {
        currentTenant.set(tenant);
    }

    // Get tenant ID for the current thread (request)
    public static String getTenant() {
        String tenant = currentTenant.get();
        if (tenant == null) {
            throw new TenantNotFoundException("No tenant ID found in the context. Tenant ID is required.");
        }
        return tenant;
    }

    // Clear tenant at the end of the request to avoid leakage
    public static void clear() {
        currentTenant.remove();
    }
}
```

#### Custom Exception (`TenantNotFoundException`)
The custom exception will be thrown when the tenant ID is not found in the context.

```java
public class TenantNotFoundException extends RuntimeException {

    public TenantNotFoundException(String message) {
        super(message);
    }

    public TenantNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
```

### Tenant Properties
The `MultiTenantProperties` class is a configuration class that maps your spring properties.

`@ConfigurationProperties(prefix = "multi.tenant.mongo")`, which tells Spring Boot to bind properties that start with this prefix to the fields of the class.
  
It contains a list of `Tenant` objects, each representing a tenant's configuration, including two fields: `tenant` (the name of the tenant) and `database` (the associated database for that tenant).

```java
@Getter
@Setter
@AllArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "multi.tenant.mongo")
public class MultiTenantProperties {

    @NotEmpty(message = "The tenants list cannot be empty")
    private List<Tenant> tenants;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Tenant {
        private String tenant;
        private String database;
    }
}
```

The `MultiTenantMongoConfig` class is responsible for creating and storing a `MongoTemplate` for each tenant and returning the correct one based on the tenant ID from `TenantContext`.

```java
@Slf4j
@Configuration
@RequiredArgsConstructor
public class MultiTenantMongoConfig {

    @Value("${mongo.uri}")
    private String uri;

    @Autowired
    private final MultiTenantProperties tenants;

    private final Map<String, MongoTemplate> tenantTemplateMap = new HashMap<>();

    @PostConstruct
    public void initMongoTemplates() {
        // Load tenants from the application properties
        tenants.getTenants().forEach((t) -> {
            log.debug("Creating MongoTemplate for tenant: {} with database: {}", t.getTenant(), t.getDatabase());

            MongoClient mongoClient = MongoClients.create(uri);
            MongoDatabaseFactory mongoDatabaseFactory = new SimpleMongoClientDatabaseFactory(mongoClient, t.getDatabase());
            MongoTemplate mongoTemplate = new MongoTemplate(mongoDatabaseFactory);

            tenantTemplateMap.put(t.getTenant(), mongoTemplate);
        });
    }

    public MongoTemplate getMongoTemplate() {
        String tenant = TenantContext.getTenant();
        MongoTemplate mongoTemplate = tenantTemplateMap.get(tenant);

        if (mongoTemplate == null) {
            log.error("No MongoTemplate found for tenant ID: {}", tenant);
            throw new TenantNotFoundException("No MongoTemplate configured for tenant: " + tenant);
        }

        log.debug("MongoTemplate found for tenant ID: {}", tenant);
        return mongoTemplate;
    }
}
```

### Tenant Filter

The `TenantFilter` extracts the tenant ID from the `X-Tenant-ID` header and sets it in the `TenantContext` for each request. It also ensures the tenant context is cleared after the request is processed.

```java
@Slf4j
@Component
public class TenantFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String tenant = request.getHeader("X-Tenant-ID");

        if (tenant != null && !tenant.isEmpty()) {
            log.debug("Tenant ID from header: {}", tenant);
            TenantContext.setTenant(tenant);
        } else {
            log.debug("No Tenant ID found in header, proceeding without tenant.");
        }

        try {
            filterChain.doFilter(request, response);
        } finally {
            TenantContext.clear();
        }
    }
}
```




## Multi-Tenant SOAP Configuration

*   Create a `MultiTenantSoapConfig` class responsible for:
    *   Loading tenant-specific SOAP service endpoint URLs from your application configuration.
    *   Creating and maintaining a map of `WebServiceTemplate` instances, each associated with a particular tenant.
    *   Providing a method to fetch the correct `WebServiceTemplate` based on the tenant identifier stored in the `TenantContext`.

```java
@Configuration
public class MultiTenantSoapConfig {

    @Value("${soap.service.endpoints}") // Load from properties
    private Map<String, String> soapServiceEndpoints;

    private final Map<String, WebServiceTemplate> tenantTemplateMap = new HashMap<>();

    @PostConstruct
    public void initSoapTemplates() {
        soapServiceEndpoints.forEach((tenant, endpoint) -> {
            WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
            webServiceTemplate.setDefaultUri(endpoint);

            // Configure marshaller and unmarshaller as needed (see SOAP guide)
            Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
            // ... set context path and other properties

            webServiceTemplate.setMarshaller(marshaller);
            webServiceTemplate.setUnmarshaller(marshaller);

            tenantTemplateMap.put(tenant, webServiceTemplate);
        });
    }

    public WebServiceTemplate getWebServiceTemplate() {
        String tenant = TenantContext.getTenant();
        WebServiceTemplate webServiceTemplate = tenantTemplateMap.get(tenant);

        if (webServiceTemplate == null) {
            throw new TenantNotFoundException("No SOAP service configured for tenant: " + tenant);
        }

        return webServiceTemplate;
    }
}
```

### 4. Tenant Filter

*   Utilize the `TenantFilter` from the multi-tenant MongoDB example. This filter extracts the tenant identifier from the request header (e.g., `X-Tenant-ID`) and sets it in the `TenantContext` for the duration of the request. It also ensures that the `TenantContext` is cleared after the request is processed to prevent any leakage.

```java
@Component
public class TenantFilter extends OncePerRequestFilter {
    // ... (implementation from the MongoDB example)
}
```

### 5. SOAP Client

*   Adapt your SOAP client to use the `MultiTenantSoapConfig` to obtain the appropriate `WebServiceTemplate` for the current tenant.

```java
@Component
public class MySoapClient {

    @Autowired
    private MultiTenantSoapConfig soapConfig;

    public MySoapResponse callSoapService(MySoapRequest request) {
        WebServiceTemplate webServiceTemplate = soapConfig.getWebServiceTemplate();
        return (MySoapResponse) webServiceTemplate.marshalSendAndReceive(request); 
        // Handle potential SOAPFaultException if needed
    }
}
```

### 6. Global Exception Handler

*   Retain the global exception handler to catch any `TenantNotFoundException` and return a suitable error response (e.g., 400 Bad Request) to the client.

```java
@RestControllerAdvice
public class GlobalExceptionHandler {
    // ... (implementation from the MongoDB example)
}
```

### 7. Properties Configuration

*   In your application properties (e.g., `application.properties`), configure the tenant-specific SOAP service endpoints:

```properties
soap.service.endpoints.tenant1=http://tenant1-soap-service
soap.service.endpoints.tenant2=http://tenant2-soap-service
# Add more tenants as needed
```

### Key Points

*   **Dynamic Endpoint Resolution:** The `MultiTenantSoapConfig` dynamically determines the SOAP service endpoint based on the tenant identifier, facilitating seamless multi-tenancy.

*   **Tenant Context:** The `TenantContext` ensures that the correct tenant information is accessible throughout the request's lifecycle.

*   **Adaptability:** This approach offers flexibility in configuring and managing multiple SOAP services for various tenants within a single Spring Boot application.

## **Resources**

- **Spring Boot Documentation:** [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)

- **Spring Web Services Documentation:** [https://spring.io/projects/spring-ws](https://spring.io/projects/spring-ws)

- **MapStruct Documentation:** [https://mapstruct.org/documentation/stable/reference/html/](https://mapstruct.org/documentation/stable/reference/html/)

- **JAXB Reference:** [https://javaee.github.io/jaxb-v2/](https://javaee.github.io/jaxb-v2/)