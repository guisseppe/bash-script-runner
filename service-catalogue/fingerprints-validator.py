import sys
import yaml

# Load the fingerprints from a YAML file
def load_fingerprints(file_path):
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)
        return data.get('fingerprints', [])

# Validate the YAML structure
def validate_fingerprints(fingerprints):
    names = set()
    tags = set()
    all_fingerprints = set()
    errors = []

    for item in fingerprints:
        name = item.get('name')
        tag = item.get('tag')
        patterns = item.get('fingerprints', [])
        subgroup_id = item.get('subgroup_id')
        project_id = item.get('project_id')
        subgroup_name = item.get('subgroup_name')

        # Check for unique names
        if name in names:
            errors.append(f"Duplicate name found: {name}")
        else:
            names.add(name)

        # Check for unique tags
        if tag in tags:
            errors.append(f"Duplicate tag found: {tag}")
        else:
            tags.add(tag)

        # Validate subgroup_id
        if not (isinstance(subgroup_id, int) or subgroup_id is None):
            errors.append(f"Invalid subgroup_id for '{name}': {subgroup_id}. Must be an integer or null.")

        # Validate project_id
        if not (isinstance(project_id, int) or project_id is None):
            errors.append(f"Invalid project_id for '{name}': {project_id}. Must be an integer or null.")

        # Validate subgroup_name
        if not (isinstance(subgroup_name, str) or subgroup_name is None):
            errors.append(f"Invalid subgroup_name for '{name}': {subgroup_name}. Must be a string or null.")

        # Check for duplicate fingerprints within the same item
        duplicate_patterns = [p for p in patterns if patterns.count(p) > 1]
        if duplicate_patterns:
            errors.append(f"Duplicate fingerprints within '{name}': {set(duplicate_patterns)}")

        # Check for unique fingerprints across all items
        for pattern in patterns:
            if pattern in all_fingerprints:
                errors.append(f"Duplicate fingerprint across items: {pattern}")
            else:
                all_fingerprints.add(pattern)

    if errors:
        print("Validation Errors:")
        for error in errors:
            print(f"- {error}")
        return False
    else:
        print("Validation passed: YAML file is valid.")
        return True

# Main function to validate fingerprints
if __name__ == "__main__":
    file_path = "fingerprints.yaml"  # Adjust path if necessary
    fingerprints = load_fingerprints(file_path)
    if not validate_fingerprints(fingerprints):
        sys.exit(1)  # Exit with code 1 on validation errors
    print("No issues found!")