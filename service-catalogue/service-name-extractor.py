import os
import re
import json
import gitlab
import logging

GITLAB_URL = "https://gitlab.example.com"
PRIVATE_TOKEN = os.getenv("GITLAB_PRIVATE_TOKEN")

# Configure logging
logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s] %(message)s")

def extract_service_names(content, file_type):
    """
    Extract microservice names from api urls.

    Args:
        content (str): The file content.
        file_type (str): Type of the file ('json' or 'properties').

    Returns:
        list: Unique list of service names.
    """
    logging.debug(f"Extracting service names from content of type: {file_type}")
    url_pattern = r"https://([a-zA-Z0-9\-]+)-(v[0-9]+)\.hboapisystem\.internal\.net"
    service_names = set()

    if file_type == "json":
        try:
            data = json.loads(content)
            logging.debug(f"Parsed JSON content successfully.")
            
            for value in str(data).split():
                match = re.search(url_pattern, value)
                
                if match:
                    service, version = match.groups()
                    
                    if version != "v1":
                        service += f"-{version}"
                    service_names.add(service)
                    logging.debug(f"Found service: {service}")
        except json.JSONDecodeError as e:
            logging.error(f"Failed to parse JSON content. Error: {e}")
    elif file_type == "properties":
        logging.debug(f"Processing .properties content.")
        
        for line in content.splitlines():
            match = re.search(url_pattern, line)
            
            if match:
                service, version = match.groups()
                if version != "v1":
                    service += f"-{version}"
                service_names.add(service)
                logging.debug(f"Found service: {service}")
    else:
        logging.warning(f"Unsupported file type: {file_type}")

    logging.debug(f"Total services extracted: {len(service_names)}")
    return list(service_names)

def process_spring_config(project_id):
    gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)
    project = gl.projects.get(project_id)
    
    logging.debug(f"Evaluating project: {project.name} Default branch: {project.default_branch}")

    # Possible Spring configuration locations
    file_paths = [
        ("pcf-config/spring-application-prd.json", "json"),
        ("src/main/resources/application-prd.properties", "properties")
    ]

    for file_path, file_type in file_paths:
        try:
            logging.info(f"Attempting to fetch file: {file_path} from branch: {project.default_branch}")
            file_content = project.files.get(file_path=file_path, ref=project.default_branch).decode()
            
            logging.info(f"File found: {file_path}. Processing content.")
            return extract_service_names(file_content, file_type)
        except Exception as e:
            logging.warning(f"File not found or inaccessible: {file_path}. Error: {e}")

    logging.error("Neither configuration file was found in the project.")
    raise FileNotFoundError("Neither configuration file was found in the project.")

# Example usage
if __name__ == "__main__":
    # Get the project ID from user input
    project_id = input("Enter GitLab project ID: ").strip()

    try:
        logging.info(f"Starting processing for project ID: {project_id}")
        service_names = process_spring_config(int(project_id))
        logging.info("Extracted Service Names:")
        for service_name in service_names:
            logging.info(service_name)
    except Exception as e:
        logging.error(f"Error: {e}")