import os
import json
import gitlab

GITLAB_URL = "https://gitlab.example.com"
PRIVATE_TOKEN = os.getenv("GITLAB_PRIVATE_TOKEN")

# Fetch the coordinators from package.json and returns GitLab project IDs for each.
def get_coordinator_project_ids(project_id):
    try:
        gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)
        project = gl.projects.get(project_id)

        file_content = project.files.get(file_path='package.json', ref=project.default_branch).decode().decode('utf-8')
        package_json = json.loads(file_content)

        coordinators = package_json.get("coordinator", [])
        if not coordinators:
            print(f"Error: The 'coordinator' property is missing or empty in package.json.")

        # Get the project IDs for each coordinator
        coordinator_project_ids = []
        for coordinator in coordinators:
            try:
                coordinator_project = gl.projects.get(coordinator)
                coordinator_project_ids.append(str(coordinator_project.id))
            except gitlab.exceptions.GitlabGetError:
                print(f"Warning: Could not find the project '{coordinator}'.")
        return coordinator_project_ids
    except Exception as e:
        print(f"Error: {e}")
        return []


def process_json_and_update_dependencies(input_file, output_file):
    """
    Reads a JSON file, processes objects with type 'SPA', updates their dependencies
    using the coordinator function, and writes the updated data to a new JSON file.

    :param input_file: Path to the input JSON file.
    :param output_file: Path to save the updated JSON file.
    :param coordinator_function: Function to fetch coordinator project IDs for a given ID.
    """
    try:
        # Load the JSON file
        with open(input_file, 'r') as file:
            data = json.load(file)

        # Ensure the input is a list
        if not isinstance(data, list):
            raise ValueError("The JSON file should contain an array of objects.")

        # Process objects with type 'SPA'
        for obj in data:
            if obj.get('type') == 'SPA':
                spa_id = obj.get('_id')
                print(f"Processsing project: {obj.get('name')}")
                
                if not spa_id:
                    print(f"Warning: Object with type 'SPA' is missing '_id': {obj}")
                    continue

                # Fetch coordinator project IDs
                coordinator_ids = coordinator_function(spa_id)

                # Append coordinator IDs to the dependencies field
                if 'dependencies' not in obj:
                    obj['dependencies'] = []
                obj['dependencies'].extend(coordinator_ids)

        # Save the updated data to a new JSON file
        with open(output_file, 'w') as file:
            json.dump(data, file, indent=4)

        print(f"Updated data has been saved to {output_file}")

    except Exception as e:
        print(f"Error: {e}")


# Example usage
if __name__ == "__main__":
    # Input JSON file path
    input_file = "input.json"  # Path to the input JSON file
    # Output JSON file path
    output_file = "output.json"  # Path to save the updated JSON file

    # Process the JSON file and update dependencies
    process_json_and_update_dependencies(input_file, output_file)