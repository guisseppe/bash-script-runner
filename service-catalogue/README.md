# Document Structure to Capture Microservice and System Dependencies

### Objective
To facilitate tracking and visualisation of microservice dependencies across various teams, we will implement a javascriptDB document structure that captures each service's ownership, dependencies, type, deployment location, and optional GitLab information. This structure enables advanced querying for dependency trees and specific dependencies, such as identifying all services owned by a team that depend on a Kafka cluster.

## Document Schema

Each service will be represented as a document in a `services` collection with the following structure:

- **_id** (string): Unique identifier for each service.

- **name** (string): Name of the service.

- **team** (string): The team responsible for the service.

- **dependencies** (array of strings): Array of service IDs that this service depends on.

- **type** (string): Type of the service (e.g., "SPA," "microservice," "database," "API gateway").

- **deployment_location** (string): Deployment location of the service, with values such as "AWS," "ON-PREM," or "BOTH."

- **gitlab_project_id** (string, optional): GitLab project ID associated with the service.

- **subgroup_id** (string, optional): GitLab subgroup ID associated with the service.

### Example Document

```json
{
    "_id": "serviceA",
    "name": "Service A",
    "team": "Team Alpha",
    "dependencies": ["serviceB", "serviceC", "kafka-cluster"],
    "type": "microservice",
    "deployment_location": "AWS",
    "gitlab_project_id": "123",
    "subgroup_id": "456"
}
```

## Query 1: Retrieve Full Dependency Tree for a Specific Application

```javascript
db.services.aggregate([
    {
        $match: { name: "serviceA" }
    },
    {
        $graphLookup: {
            from: "services",
            startWith: "$dependencies",
            connectFromField: "dependencies",
            connectToField: "_id",
            as: "dependency_tree",
            depthField: "level"
        }
    }
]);
```

### Expected Output

```json
[
    {
        "_id": "serviceA",
        "name": "Service A",
        "team": "Team Alpha",
        "dependencies": ["serviceB", "serviceC", "kafka-cluster"],
        "type": "microservice",
        "deployment_location": "AWS",
        "gitlab_project_id": "123",
        "subgroup_id": "456",
        "dependency_tree": [
            {
                "_id": "serviceB",
                "name": "Service B",
                "team": "Team Beta",
                "dependencies": ["serviceD"],
                "type": "database",
                "deployment_location": "ON-PREM",
                "gitlab_project_id": "789",
                "subgroup_id": "101",
                "level": 1
            },
            {
                "_id": "serviceC",
                "name": "Service C",
                "team": "Team Gamma",
                "dependencies": [],
                "type": "API gateway",
                "deployment_location": "BOTH",
                "gitlab_project_id": "112",
                "subgroup_id": "113",
                "level": 1
            },
            {
                "_id": "kafka-cluster",
                "name": "Kafka Cluster",
                "team": "Platform Team",
                "dependencies": [],
                "type": "infrastructure",
                "deployment_location": "BOTH",
                "gitlab_project_id": "901",
                "subgroup_id": "902",
                "level": 1
            },
            {
                "_id": "serviceD",
                "name": "Service D",
                "team": "Team Delta",
                "dependencies": [],
                "type": "microservice",
                "deployment_location": "AWS",
                "gitlab_project_id": "114",
                "subgroup_id": "115",
                "level": 2
            }
        ]
    }
]
```

## Query 2: Find All Services Owned by a Specific Team with a Dependency on Kafka Cluster

```javascript
db.services.aggregate([
    {
        $match: {
            team: "Team Alpha",  // Replace with the specific team you're interested in
            dependencies: "kafka-cluster"  // Replace with the ID or name of your Kafka cluster
        }
    }
]);
```

### Expected Output

```json
[
    {
        "_id": "serviceA",
        "name": "Service A",
        "team": "Team Alpha",
        "dependencies": ["serviceB", "kafka-cluster", "serviceC"],
        "type": "microservice",
        "deployment_location": "AWS",
        "gitlab_project_id": "123",
        "subgroup_id": "456"
    },
    {
        "_id": "serviceX",
        "name": "Service X",
        "team": "Team Alpha",
        "dependencies": ["kafka-cluster"],
        "type": "SPA",
        "deployment_location": "ON-PREM",
        "gitlab_project_id": "789",
        "subgroup_id": "101"
    }
]
```