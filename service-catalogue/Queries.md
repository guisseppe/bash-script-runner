# Microservice Dependency Analysis Queries

These queries are designed to help analyse microservice dependencies, assess team responsibilities, and identify the impact of service unavailability.

## **Identify All Direct Dependencies for a Given Service**

Retrieves only the services that a specific service depends on directly (not recursive).

   ```javascript
   db.services.find(
       { "_id": "serviceA" },  // Replace with the target service ID
       { "dependencies": 1, "_id": 0 }
   );
   ```

## **Find All Services Impacted by the Unavailability of Multiple Services**

Given a list of services (e.g., core infrastructure services), find all services that directly or indirectly depend on any of them.

   ```javascript
   db.services.aggregate([
       { "$match": { "_id": { "$in": ["serviceX", "serviceY", "serviceZ"] } } },  // List of services to check
       {
           "$graphLookup": {
               "from": "services",
               "startWith": "$_id",
               "connectFromField": "_id",
               "connectToField": "dependencies",
               "as": "dependent_services",
               "depthField": "level"
           }
       },
       { "$unwind": "$dependent_services" },
       { "$group": { "_id": "$dependent_services._id", "name": { "$first": "$dependent_services.name" }, "team": { "$first": "$dependent_services.team" } } }
   ]);
   ```

## **List All Services Owned by a Specific Team**

Useful for understanding what services each team is responsible for, which can help with capacity planning and operational responsibilities.

   ```javascript
   db.services.find(
       { "team": "Team Alpha" },  // Replace with the desired team name
       { "_id": 1, "name": 1, "dependencies": 1 }
   );
   ```

## **Identify Orphan Services (No Dependencies on Them)**

Finds services that no other services depend on, which may indicate unused or isolated services.

   ```javascript
   db.services.find({
       "dependencies": { "$size": 0 }
   });
   ```

## **Identify Critical Services (Services with the Most Dependencies)**

Lists services with the highest number of dependent services, which can indicate critical points of failure.

   ```javascript
   db.services.aggregate([
       { "$unwind": "$dependencies" },
       { "$group": { "_id": "$dependencies", "count": { "$sum": 1 } } },
       { "$sort": { "count": -1 } },
       { "$limit": 10 }  // Get top 10 most critical services
   ]);
   ```

## **Find All Services Deployed on a Specific Environment**

   - Helps identify services deployed in a particular environment (e.g., AWS, ON-PREM).

   ```javascript
   db.services.find(
       { "deployment_location": "AWS" },  // Replace with the desired deployment location
       { "_id": 1, "name": 1, "team": 1 }
   );
   ```

## **List All Unique Dependencies Across All Services**

Provides an overview of all unique dependencies, which can be useful for understanding common dependencies and managing shared services.

   ```javascript
   db.services.aggregate([
       { "$unwind": "$dependencies" },
       { "$group": { "_id": "$dependencies" } }
   ]);
   ```

## **Get Dependency Path Between Two Services**

Finds if there’s a path between two services (e.g., `serviceA` to `serviceB`), showing dependencies along the way.

   ```javascript
   db.services.aggregate([
       { "$match": { "_id": "serviceA" } },
       {
           "$graphLookup": {
               "from": "services",
               "startWith": "$dependencies",
               "connectFromField": "dependencies",
               "connectToField": "_id",
               "as": "path_to_serviceB",
               "restrictSearchWithMatch": { "_id": "serviceB" }  // End condition
           }
       }
   ]);
   ```

## **Count the Number of Dependencies for Each Service**

Counts the total number of direct and indirect dependencies each service has, which is useful for identifying complex services.

   ```javascript
   db.services.aggregate([
       {
           "$graphLookup": {
               "from": "services",
               "startWith": "$dependencies",
               "connectFromField": "dependencies",
               "connectToField": "_id",
               "as": "all_dependencies"
           }
       },
       { "$addFields": { "dependency_count": { "$size": "$all_dependencies" } } },
       { "$project": { "_id": 1, "name": 1, "dependency_count": 1 } }
   ]);
   ```