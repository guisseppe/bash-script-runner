import re
import yaml
import json
import logging

# Configure logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

def search_project(fingerprint, project_id):
    """
    Checks if the given fingerprint pattern exists in the test string.

    Args:
        fingerprint (str): The pattern to search for.
        project_id (str): The GitLab project ID.

    Returns:
        bool: True if the pattern is found, False otherwise.
    """
    match = bool(re.search(fingerprint, project_id))
    logging.debug(f"Searching for pattern: {fingerprint} in project: {project_id}... Result: {match}")
    return match

def process_fingerprints(yaml_file, project_id):
    """
    Processes fingerprints defined in a YAML file and checks for matches in a given GitLab project.

    Args:
        yaml_file (str): Path to the YAML file containing fingerprint definitions.
        project_id (str): The GitLab project (ID) to search.

    Returns:
        list: A list of tags corresponding to fingerprints that were found in the project.
    """
    found_tags = []
    logging.debug(f"Loading fingerprints from YAML file: {yaml_file}")

    # Load the YAML file
    with open(yaml_file, 'r') as file:
        data = yaml.safe_load(file)

    logging.debug("Processing fingerprints...")

    # Process each fingerprint block
    for block in data.get('fingerprints', []):
        logging.debug(f"Evaluating block: {block['name']}")
        for fingerprint in block['fingerprints']:
            if search_project(fingerprint, project_id):
                logging.info(f"Match found for tag: {block['tag']}")
                found_tags.append(block['tag'])
                break  # Stop evaluating other fingerprints in this block if one is found

    logging.debug(f"Tags found: {found_tags}")
    return found_tags

def process_json_objects(json_file, yaml_file, output_file):
    """
    Processes a JSON file containing project data, applies fingerprint processing,
    and adds a 'dependencies' property to each object in the JSON.

    Args:
        json_file (str): Path to the JSON file containing project data.
        yaml_file (str): Path to the YAML file containing fingerprint definitions.
        output_file (str): Path to save the updated JSON file.

    Returns:
        None: The function saves the updated JSON data to the specified output file.
    """
    logging.debug(f"Loading JSON data from file: {json_file}")

    # Load the JSON file
    with open(json_file, 'r') as file:
        json_data = json.load(file)

    logging.debug("Processing JSON objects...")

    # Process each object in the JSON array
    for obj in json_data:
        logging.debug(f"Processing project: {obj['project_name']} (ID: {obj['project_id']})")
        dependencies = process_fingerprints(yaml_file, obj['project_id'])

        print(f"Found project dependencies {dependencies}")
        obj['dependencies'] = dependencies  # Append the dependencies to the object

    logging.debug(f"Saving updated JSON to file: {output_file}")

    # Save the updated JSON data to the output file
    with open(output_file, 'w') as file:
        json.dump(json_data, file, indent=4)

    logging.info(f"Updated JSON saved to {output_file}")

if __name__ == "__main__":
    json_file = "projects.json" 
    yaml_file = "fingerprint.yaml"  # Input YAML file with fingerprints
    output_file = "updated_projects.json"

    logging.info("Starting processing of JSON objects...")
    process_json_objects(json_file, yaml_file, output_file)
    logging.info("Processing complete.")