#!/bin/bash

set -e  # Exit on any error
set -o pipefail  # Catch errors in pipes

# ANSI color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

EXPECTED_BUILDPACK="jva_73"
CONFIG_DIR="pcf-config"
FORCE_RUN=false

# Check for --force flag to bypass date check
if [[ "$1" == "--force" ]]; then
    FORCE_RUN=true
fi

CURRENT_DAY=$(date +"%d")
if [ "$CURRENT_DAY" -lt 28 ] && [ "$FORCE_RUN" == false ]; then
    echo -e "${YELLOW}Skipping script execution. This check runs from the 28th onwards. Use --force to run it now.${NC}"
    exit 0
fi

if [ ! -d "$CONFIG_DIR" ]; then
    echo -e "${RED}Error: Directory '$CONFIG_DIR' not found.${NC}"
    exit 1
fi

INVALID_FILES=()

for file in "$CONFIG_DIR"/pcf*.config; do
    [ -e "$file" ] || continue  # Skip if no matching files
    
    BUILDPACK_VALUE=$(grep -Eo 'java_buildpack[[:space:]]*=[[:space:]]*"?[^"]+"?' "$file" | awk -F '=' '{print $2}' | tr -d ' "')
    
    if [ -z "$BUILDPACK_VALUE" ]; then
        echo -e "${YELLOW}Warning: 'java_buildpack' key is missing in file: $file${NC}"
        INVALID_FILES+=("$file")
    elif [ "$BUILDPACK_VALUE" != "$EXPECTED_BUILDPACK" ]; then
        echo -e "${RED}Error: Incorrect buildpack in file: $file (Found: $BUILDPACK_VALUE, Expected: $EXPECTED_BUILDPACK)${NC}"
        INVALID_FILES+=("$file")
    fi

done

if [ ${#INVALID_FILES[@]} -ne 0 ]; then
    echo -e "\n${RED}Validation failed. Please update the listed files.${NC}"
    exit 1
fi

echo -e "${GREEN}All PCF configuration files are correctly set.${NC}"
exit 0
