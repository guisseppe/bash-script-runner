#!/bin/bash

# Input file containing URLs (one per line)
INPUT_FILE="urls.txt"

# Check if file exists
if [[ ! -f "$INPUT_FILE" ]]; then
  echo "Error: File $INPUT_FILE not found!"
  exit 1
fi

# Counters for working and broken links
working_count=0
broken_count=0
error_count=0

# Temporary files for tracking URLs
working_urls="working_urls.txt"
broken_urls="broken_urls.txt"
error_log="errors.txt"

# Clear previous output files
> "$working_urls"
> "$broken_urls"
> "$error_log"

# Print header
echo "Checking URLs with curl (-k -v)..."

# Iterate over each URL in the file
while IFS= read -r url; do
  # Trim whitespace and skip empty lines
  url=$(echo "$url" | xargs)
  if [[ -z "$url" ]]; then
    continue
  fi

  # Check if URL starts with http:// or https://
  if [[ ! "$url" =~ ^https?:// ]]; then
    echo "ERROR: Invalid URL format - $url"
    echo "$url" >> "$error_log"
    ((error_count++))
    continue
  fi

  echo "Testing: $url"

  # filename (replace special characters)
  filename=$(echo "$url" | sed 's/[^a-zA-Z0-9]/_/g').txt

  # Run curl with verbose mode (-v), SSL bypass (-k), and pipe output to a file
  curl -k -v "$url" > "$filename" 2>&1

  # Check if the response contains angle brackets (HTML/XML-like content)
  if grep -q "<" "$filename" && grep -q ">" "$filename"; then
    echo "WORKING: $url (HTML/XML Found)"
    echo "$url" >> "$working_urls"
    ((working_count++))
  else
    echo "BROKEN: $url (No HTML/XML Detected)"
    echo "$url" >> "$broken_urls"
    ((broken_count++))
  fi
done < "$INPUT_FILE"

# Print summary
echo -e "\n==== SUMMARY ===="
echo "WORKING Links: $working_count"
echo "BROKEN Links:  $broken_count"
echo "INVALID URLs:  $error_count"

# Optional: Display lists of working and broken URLs
if [[ $working_count -gt 0 ]]; then
  echo -e "\nWORKING URLs:"
  cat "$working_urls"
fi

if [[ $broken_count -gt 0 ]]; then
  echo -e "\nBROKEN URLs:"
  cat "$broken_urls"
fi

if [[ $error_count -gt 0 ]]; then
  echo -e "\nINVALID URLs:"
  cat "$error_log"
fi