from pymongo import MongoClient
from datetime import datetime, timedelta

# MongoDB Connection
MONGO_URI = "mongodb://localhost:27017"
DB_NAME = "certificates_db"
COLLECTION_NAME = "certificates"

def cleanup_expired_certs():
    """Remove certificates where all reminders have been sent and 14 days have passed since expiry."""
    client = MongoClient(MONGO_URI)
    db = client[DB_NAME]
    collection = db[COLLECTION_NAME]

    now = datetime.utcnow()
    expiry_threshold = now - timedelta(days=14)

    # Define query to find certs that have expired over 14 days ago and all reminders have been sent
    query = {
        "valid_to": {"$lte": expiry_threshold},
        "three_month_reminder": True,
        "two_month_reminder": True,
        "one_month_reminder": True,
        "two_weeks_reminder": True
    }

    # Delete matched certificates
    result = collection.delete_many(query)

    print(f"🔄 Cleaned up {result.deleted_count} expired certificates from the database.")
    
    client.close()

if __name__ == "__main__":
    cleanup_expired_certs()