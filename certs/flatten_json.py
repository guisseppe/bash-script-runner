import os
import json
import argparse

def flatten_json(y, parent_key='', sep='.'):
    """Recursively flattens nested JSON."""
    items = []
    for k, v in y.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_json(v, new_key, sep=sep).items())
        elif isinstance(v, list):
            for i, item in enumerate(v):
                if isinstance(item, dict):
                    items.extend(flatten_json(item, f"{new_key}[{i}]", sep=sep).items())
                else:
                    items.append((f"{new_key}[{i}]", item))
        else:
            items.append((new_key, v))
    return dict(items)

def process_directory(directory):
    """Flatten and replace all JSON files in the specified directory."""
    for filename in os.listdir(directory):
        if filename.endswith(".json"):
            file_path = os.path.join(directory, filename)
            with open(file_path, 'r', encoding='utf-8') as f:
                try:
                    data = json.load(f)
                except json.JSONDecodeError as e:
                    print(f"Error decoding JSON in file {filename}: {e}")
                    continue

            flattened_data = flatten_json(data)

            with open(file_path, 'w', encoding='utf-8') as f:
                json.dump(flattened_data, f, indent=4)

            print(f"Flattened and replaced: {filename}")

def main():
    parser = argparse.ArgumentParser(description="Flatten all JSON files in a given directory and replace them.")
    parser.add_argument('directory', type=str, help='Path to the directory containing JSON files.')
    args = parser.parse_args()

    if not os.path.isdir(args.directory):
        print(f"Error: {args.directory} is not a valid directory.")
        return

    process_directory(args.directory)

if __name__ == "__main__":
    main()