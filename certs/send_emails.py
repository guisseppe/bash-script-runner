import requests
from pymongo import MongoClient
from jinja2 import Environment, FileSystemLoader
from datetime import datetime, timedelta
import argparse
from enum import Enum

# MongoDB Connection
MONGO_URI = "mongodb://localhost:27017"
DB_NAME = "certificates_db"
COLLECTION_NAME = "certificates"

# Email Microservice API Endpoint
EMAIL_API_URL = "http://email-service.local/email/test"

# Load Jinja2 template
env = Environment(loader=FileSystemLoader("."))
template = env.get_template("cert_expiry_template.html")

# Enum for Expiry Periods with CC Emails
class ExpiryPeriod(Enum):
    THREE_MONTHS = (90, ["manager1@example.com"], "three_month_reminder")
    TWO_MONTHS = (60, ["manager1@example.com", "manager2@example.com"], "two_month_reminder")
    ONE_MONTH = (30, ["manager1@example.com", "manager2@example.com", "manager3@example.com"], "one_month_reminder")
    TWO_WEEKS = (14, ["manager1@example.com", "manager2@example.com", "manager3@example.com", "department_head@example.com"], "two_weeks_reminder")

def get_expiring_certs(days, reminder_field):
    """Fetch and group expiring certificates by subgroup, skipping those already reminded."""
    now = datetime.utcnow()
    expiry_threshold = now + timedelta(days=days)

    client = MongoClient(MONGO_URI)
    db = client[DB_NAME]
    collection = db[COLLECTION_NAME]

    query = {
        "valid_to": {"$lte": expiry_threshold, "$gte": now},
        reminder_field: {"$ne": True}
    }
    certs = list(collection.find(query))

    # Group certificates by subgroup
    grouped_certs = {}
    for cert in certs:
        subgroup = cert.get("subgroup_name", "Unknown")
        if subgroup not in grouped_certs:
            grouped_certs[subgroup] = {"subgroup_email": cert.get("subgroup_email", None), "certs": []}
        grouped_certs[subgroup]["certs"].append(cert)

    client.close()
    return grouped_certs

def send_cert_emails(expiry_period):
    """Send separate emails for production and non-production certificates."""
    days, cc_emails, reminder_field = expiry_period.value
    grouped_certs = get_expiring_certs(days, reminder_field)

    client = MongoClient(MONGO_URI)
    db = client[DB_NAME]
    collection = db[COLLECTION_NAME]

    for subgroup, data in grouped_certs.items():
        subgroup_email = data["subgroup_email"]
        non_prod_certs = [cert for cert in data["certs"] if not cert["file"].endswith("prd.json")]
        prod_certs = [cert for cert in data["certs"] if cert["file"].endswith("prd.json")]

        # Send Non-Production Certificates Email
        if non_prod_certs:
            non_prod_email_body = template.render(
                subgroup_name=subgroup,
                certs=non_prod_certs,
                total_certs=len(non_prod_certs),
                urgent_certs=sum(1 for cert in non_prod_certs if "prd.json" in cert.get("file", "").lower()),
                self_signed_certs=sum(1 for cert in non_prod_certs if cert.get("file", "").endswith("dev.json")),
                renewal_link="https://cert-renewal.example.com",
                prod_label=False
            )
            if send_email_via_api(subgroup_email, f"{subgroup} - Certificate Expiry (Non-Production)", non_prod_email_body, cc_emails):
                update_cert_status(collection, non_prod_certs, reminder_field)

        # Send Production Certificates Email
        if prod_certs:
            prod_email_body = template.render(
                subgroup_name=subgroup,
                certs=prod_certs,
                total_certs=len(prod_certs),
                urgent_certs=len(prod_certs),
                self_signed_certs=0,
                renewal_link="https://cert-renewal.example.com",
                prod_label=True
            )
            if send_email_via_api(subgroup_email, f"{subgroup} - 🛘 Production Certificate Expiry", prod_email_body, cc_emails):
                update_cert_status(collection, prod_certs, reminder_field)

    client.close()

def send_email_via_api(recipient, subject, email_body, cc_emails):
    """Send an email via the email microservice API."""
    cc_list = ",".join(cc_emails + ["devops@example.com"])
    payload = {
        "recipient": recipient,
        "cc": cc_list,
        "subject": subject,
        "body": email_body
    }

    response = requests.post(EMAIL_API_URL, json=payload)

    if response.status_code == 200:
        print(f"✅ Email sent to {recipient} with subject '{subject}'")
        return True
    else:
        print(f"❌ Failed to send email to {recipient}. Error: {response.text}")
        return False

def update_cert_status(collection, certs, reminder_field):
    """Update the reminder field in MongoDB after sending an email."""
    for cert in certs:
        collection.update_one(
            {"_id": cert["_id"]},
            {"$set": {reminder_field: True}}
        )
        print(f"🔄 Updated reminder status for certificate: {cert['_id']}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Send certificate expiry emails based on the selected expiry period.")
    parser.add_argument(
        "--period",
        type=ExpiryPeriod,
        choices=list(ExpiryPeriod),
        required=True,
        help="Select expiry period: THREE_MONTHS, TWO_MONTHS, ONE_MONTH, TWO_WEEKS"
    )
    args = parser.parse_args()
    send_cert_emails(args.period)