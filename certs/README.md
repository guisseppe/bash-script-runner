# Certificate Expiry Reminder Script

This repository automates the process of sending email reminders for expiring PCF SSL certificates. The script categorises certificates based on their expiry timeline and sends separate emails for production and non-production certificates.

## Features
- Fetches certificate details from MongoDB.

- Sends separate email notifications for:
  - **Production certificates**

  - **Non-production certificates**

- Supports reminder intervals of 3 months, 2 months, 1 month, and 2 weeks.

- CCs senior managers based on the selected expiry period.

## Usage
Run the script with the desired expiry period:

```bash
python send_cert_emails.py --period <EXPIRY_PERIOD>
```

### Available Expiry Periods:
- `THREE_MONTHS`

- `TWO_MONTHS`  

- `ONE_MONTH`  

- `TWO_WEEKS`

### Example Commands:
```bash
python send_cert_emails.py --period THREE_MONTHS
python send_cert_emails.py --period TWO_MONTHS
python send_cert_emails.py --period ONE_MONTH
python send_cert_emails.py --period TWO_WEEKS
```

## Email CC Logic
Based on the selected expiry period, the following managers will be CC'd in the email:

- **3 Months**: `manager1@example.com`

- **2 Months**: `manager1@example.com`, `manager2@example.com`

- **1 Month**: `manager1@example.com`, `manager2@example.com`, `manager3@example.com`

- **2 Weeks**: `manager1@example.com`, `manager2@example.com`, `manager3@example.com`, `department_head@example.com`

All emails will also CC the **DevOps team** at `devops@example.com`.

## Template Customisation
Edit the `cert_expiry_template.html` file to customise the email format and styling. The template uses Jinja2 for dynamic content rendering.

## Example Email Subject Lines
- **Non-Production**: `Subgroup - PCF Certificate Expiry (Non-Production)`

- **Production**: `Subgroup - 🛘 Production PCF Certificate Expiry`