# MongoDB queries to retrieve expiring certificates grouped by subgroup.

### **1. Expired Certificates (Already Implemented)**

```json
db.certificates.aggregate([
    { "$match": { "is_expired": true } },
    { "$group": {
        "_id": "$subgroup_name",
        "count": { "$sum": 1 },
        "certificates": { "$push": "$$ROOT" }
    }}
])
```

📌 **Use case:** 

- Send **immediate alerts** for certificates that are **already expired**.

### **2. Certificates Expiring Within One Week**

```json
db.certificates.aggregate([
    { "$match": { "valid_to": { "$lte": new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000), "$gte": new Date() } } },
    { "$group": {
        "_id": "$subgroup_name",
        "count": { "$sum": 1 },
        "certificates": { "$push": "$$ROOT" }
    }}
])
```

📌 **Use case:**  

- Send a **"Warning: Certificate Expiring in a Week"** email to **certificate owners**.

- Escalate to **team leads** if no action is taken.

### **3. Certificates Expiring Within Three Months**

```json
db.certificates.aggregate([
    { "$match": { "valid_to": { "$lte": new Date(new Date().getTime() + 90 * 24 * 60 * 60 * 1000), "$gte": new Date() } } },
    { "$group": {
        "_id": "$subgroup_name",
        "count": { "$sum": 1 },
        "certificates": { "$push": "$$ROOT" }
    }}
])
```

📌 **Use case:** 

- **Long-term tracking** for certificates expiring in the **next 3 months**.

- Helps **prioritize renewal efforts** across multiple teams.

### **Python Script to Fetch All Three Variants**

```python
from pymongo import MongoClient
from datetime import datetime, timedelta
import json

# Connect to MongoDB
client = MongoClient("mongodb://localhost:27017")
db = client["certificates_db"]
collection = db["certificates"]

# Define expiration thresholds
now = datetime.utcnow()
one_week_from_now = now + timedelta(days=7)
three_months_from_now = now + timedelta(days=90)

# Queries
queries = {
    "Expired Certificates": {"is_expired": True},
    "Expiring in One Week": {"valid_to": {"$lte": one_week_from_now, "$gte": now}},
    "Expiring in Three Months": {"valid_to": {"$lte": three_months_from_now, "$gte": now}}
}

# Run queries and display results
for label, query in queries.items():
    aggregation = collection.aggregate([
        {"$match": query},
        {"$group": {
            "_id": "$subgroup_name",
            "count": {"$sum": 1},
            "certificates": {"$push": "$$ROOT"}  # Include all stored details
        }}
    ])
    
    print(f"\n--- {label} ---\n")
    for subgroup in aggregation:
        print(json.dumps(subgroup, indent=4, default=str))

# Close MongoDB connection
client.close()
```

### **Expected Output (Example)**

```json
--- Expired Certificates ---
{
    "_id": "security-team",
    "count": 2,
    "certificates": [
        {
            "_id": "internal.security.com",
            "subject": "CN=internal.security.com",
            "issuer": "CN=Security CA",
            "valid_from": "2021-01-01T00:00:00",
            "valid_to": "2023-01-01T00:00:00",
            "serial_number": 123456789,
            "is_expired": true,
            "project_name": "security-project",
            "project_id": "56789",
            "subgroup_name": "security-team",
            "project_link": "https://gitlab.com/security-group/security-project",
            "file": "pcf.config/spring-application-security.json"
        }
    ]
}
```