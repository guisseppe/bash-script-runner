import os
import csv
import base64
import gitlab

# CONFIGURATION
GITLAB_URL = "https://gitlab.com"  # Update if using a self-hosted instance

PRIVATE_TOKEN = os.environ.get("GITLAB_TOKEN", "your_token_here")

# The top-level GitLab group ID to start searching from
STARTING_GROUP_ID = 123456  # <-- Replace with your group ID

# List of subgroup names or full paths to exclude
EXCLUDED_GROUPS = [
    "group-to-exclude",          # by group name
    "parent-group/subgroup2"     # or by full path
]

# Target project and file
TARGET_PROJECT_NAME = "gitlab-profile"
TARGET_FILE = "team.yaml"
TARGET_BRANCH = "main"

# Output CSV file
OUTPUT_CSV = "report.csv"


gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)

def get_subgroups(group):
    """Retrieve all subgroups recursively under a given group."""
    return group.subgroups.list(all=True)


def get_projects(group):
    """Retrieve all projects in a given group."""
    return group.projects.list(all=True)


def get_file_contents(project, file_path, branch):
    """Retrieve the contents of a file from a project branch."""
    try:
        f = project.files.get(file_path=file_path, ref=branch)
        decoded_content = base64.b64decode(f.content).decode("utf-8")
        return decoded_content
    except gitlab.exceptions.GitlabGetError:
        return None


def process_group(group, csv_writer):
    """Check for the target project in a group, and recursively process subgroups."""
    group_name = group.name
    full_path = group.full_path

    # Skip excluded groups
    if group_name in EXCLUDED_GROUPS or full_path in EXCLUDED_GROUPS:
        print(f"Skipping excluded group: {full_path}")
        return

    print(f"Processing group: {full_path}")

    # Search for the target project
    projects = get_projects(group)
    for project in projects:
        if project.name == TARGET_PROJECT_NAME:
            print(f"  Found project '{TARGET_PROJECT_NAME}' in {full_path}")
            file_content = get_file_contents(project, TARGET_FILE, TARGET_BRANCH)
            if file_content:
                print(f"    Found {TARGET_FILE} in {project.web_url}")
            else:
                print(f"    {TARGET_FILE} not found in {project.web_url}")

            # Write to CSV
            csv_writer.writerow({
                "group_full_path": full_path,
                "project_name": project.name,
                "project_id": project.id,
                "project_url": project.web_url,
                "team_yaml": file_content if file_content else "Not Found"
            })

    # Process subgroups recursively
    subgroups = get_subgroups(group)
    for subgroup in subgroups:
        process_group(subgroup, csv_writer)


def main():
    with open(OUTPUT_CSV, "w", newline="", encoding="utf-8") as csvfile:
        fieldnames = ["group_full_path", "project_name", "project_id", "project_url", "team_yaml"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        try:
            starting_group = gl.groups.get(STARTING_GROUP_ID)
        except gitlab.exceptions.GitlabGetError:
            print(f"Error: Group with ID {STARTING_GROUP_ID} not found.")
            return

        process_group(starting_group, writer)

    print(f"Report generated: {OUTPUT_CSV}")


if __name__ == "__main__":
    main()