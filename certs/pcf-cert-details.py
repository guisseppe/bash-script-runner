import os
import json
import glob
from datetime import datetime
from pymongo import MongoClient
from cryptography import x509
from cryptography.hazmat.backends import default_backend


# pip install pymongo cryptography

PCF_CERT_SPRING_PROP_NAME = "initialisation-cert"


def parse_certificate(cert_flattened):
    """Convert flattened cert string to PEM format and parse details."""
    pem_header = "-----BEGIN CERTIFICATE-----"
    pem_footer = "-----END CERTIFICATE-----"
    cert_body = cert_flattened.replace(pem_header, "").replace(pem_footer, "").strip()
    formatted_cert = f"{pem_header}\n"
    for i in range(0, len(cert_body), 64):
        formatted_cert += cert_body[i:i+64] + "\n"
    formatted_cert += pem_footer

    print(f"Extracted cert:\n\n{formatted_cert}\n\n")

    # Parse the certificate
    cert = x509.load_pem_x509_certificate(formatted_cert.encode(), default_backend())
    now = datetime.utcnow()

    # Extract CN (Common Name)
    cn_name = None
    for attribute in cert.subject:
        if attribute.oid == x509.NameOID.COMMON_NAME:
            cn_name = attribute.value
            break

    if not cn_name:
        raise ValueError("CN (Common Name) not found in certificate")

    return {
        "_id": cn_name,  # Set CN as the primary key
        "subject": cert.subject.rfc4514_string(),
        "issuer": cert.issuer.rfc4514_string(),
        "valid_from": cert.not_valid_before,
        "valid_to": cert.not_valid_after,
        "serial_number": cert.serial_number,
        "is_expired": now > cert.not_valid_after,
    }


def extract_subgroup(namespace):
    """Extract only the immediate subgroup name from the full GitLab namespace."""
    parts = namespace.split("/")
    return parts[-1] if len(parts) > 1 else namespace  # Get the last part if it's a subgroup


def write_to_mongo(mongo_uri, db_name, collection_name, project_data):
    """Write project and certificate details to MongoDB."""
    try:
        client = MongoClient(mongo_uri)
        db = client[db_name]
        collection = db[collection_name]

        # Use CN as the primary key (_id)
        result = collection.update_one({"_id": project_data["_id"]}, {"$set": project_data}, upsert=True)
        
        if result.matched_count > 0:
            print(f"Updated existing certificate with CN: {project_data['_id']}")
        else:
            print(f"Inserted new certificate with CN: {project_data['_id']}")

    except Exception as e:
        print(f"Error writing to MongoDB: {e}")
    finally:
        client.close()


def process_certificates(directory, mongo_uri, db_name, collection_name):
    """Read JSON files, extract certificate details, and store in MongoDB."""
    file_pattern = os.path.join(directory, "spring-application-*.json")
    for filepath in glob.glob(file_pattern):
        try:
            with open(filepath, 'r') as file:
                data = json.load(file)

                # Get GitLab metadata from pipeline variables
                project_name = os.getenv("CI_PROJECT_NAME", "unknown")
                project_id = os.getenv("CI_PROJECT_ID", "unknown")
                namespace = os.getenv("CI_PROJECT_NAMESPACE", "unknown")
                project_link = os.getenv("CI_PROJECT_URL", "unknown")

                # Extract only the subgroup name
                subgroup_name = extract_subgroup(namespace)

                # Check if the 'initialisation-cert' property exists
                if PCF_CERT_SPRING_PROP_NAME in data:
                    cert_flattened = data[PCF_CERT_SPRING_PROP_NAME]

                    # Parse the certificate
                    cert_details = parse_certificate(cert_flattened)
                    cert_details.update({
                        "project_name": project_name,
                        "project_id": project_id,
                        "subgroup_name": subgroup_name,
                        "project_link": project_link,
                        "file": filepath,
                    })

                    # Write to MongoDB
                    write_to_mongo(mongo_uri, db_name, collection_name, cert_details)
                else:
                    print(f"Skipping {filepath}: 'initialisation-cert' not found.")
        except json.JSONDecodeError:
            print(f"Error: Failed to parse {filepath}. Not a valid JSON file.")
        except ValueError as ve:
            print(f"Skipping {filepath}: {ve}")
        except Exception as e:
            print(f"Error processing {filepath}: {e}")


# Example usage
directory = "pcf.config"  # Path to your configuration directory
mongo_uri = "mongodb://localhost:27017"  # Update with your MongoDB URI
db_name = "certificates_db"  # Database name
collection_name = "certificates"  # Collection name

process_certificates(directory, mongo_uri, db_name, collection_name)