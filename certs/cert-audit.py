import gitlab
import os
import json
import re
import pandas as pd

# Configuration
GITLAB_URL = 'https://gitlab.com'  # Change to your GitLab instance if self-hosted
GITLAB_TOKEN = os.getenv('GITLAB_TOKEN')  # Ensure you have set your GitLab token in the environment
SUBGROUP_ID = 'your-subgroup-id'  # Replace with your subgroup ID

# Prefixes and Suffixes to filter project names
PREFIXES = ['prefix1', 'prefix2']
SUFFIXES = ['suffix1', 'suffix2']

# Initialise GitLab connection
gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
gl.auth()

def flatten_json(y, parent_key='', sep='.'):
    """Recursively flattens nested JSON."""
    items = []
    for k, v in y.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_json(v, new_key, sep=sep).items())
        elif isinstance(v, list):
            for i, item in enumerate(v):
                if isinstance(item, dict):
                    items.extend(flatten_json(item, f"{new_key}[{i}]", sep=sep).items())
                else:
                    items.append((f"{new_key}[{i}]", item))
        else:
            items.append((new_key, v))
    return dict(items)

def project_matches(project_name):
    """Check if the project name matches the specified prefixes or suffixes."""
    return any(project_name.startswith(prefix) for prefix in PREFIXES) or \
           any(project_name.endswith(suffix) for suffix in SUFFIXES)

def check_json_files(project):
    """Check if the 'init.cert' value in JSON files is encrypted."""
    try:
        # List files in the 'pcf-config' directory
        items = project.repository_tree(path='pcf-config', recursive=True)
        json_files = [item['path'] for item in items if re.match(r'pcf-config/spring-application-.*\.json', item['path'])]

        if not json_files:
            return {
                'project': project.name,
                'project_link': project.web_url,
                'json_files_found': False,
                'init_cert_encrypted': None
            }

        init_cert_encrypted = True

        for json_file in json_files:
            file_content = project.files.get(file_path=json_file, ref=project.default_branch).decode().decode('utf-8')
            data = json.loads(file_content)
            flattened_data = flatten_json(data)

            init_cert_value = flattened_data.get('init.cert')
            if init_cert_value and not init_cert_value.startswith('ENC'):
                init_cert_encrypted = False
                break

        return {
            'project': project.name,
            'project_link': project.web_url,
            'json_files_found': True,
            'init_cert_encrypted': init_cert_encrypted
        }

    except Exception as e:
        print(f"Error processing project {project.name}: {e}")
        return {
            'project': project.name,
            'project_link': project.web_url,
            'json_files_found': False,
            'init_cert_encrypted': None
        }

def process_projects():
    """Process projects in the subgroup and generate a report."""
    all_results = []
    subgroup = gl.groups.get(SUBGROUP_ID)
    projects = subgroup.projects.list(include_subgroups=True, all=True)

    for project in projects:
        if project.archived:
            continue

        if project_matches(project.name):
            print(f"Processing project: {project.name}")
            project_details = gl.projects.get(project.id)
            result = check_json_files(project_details)
            all_results.append(result)

    # Generate CSV report
    df = pd.DataFrame(all_results)
    report_path = 'gitlab_init_cert_encryption_summary.csv'
    df.to_csv(report_path, index=False)
    print(f"Report saved to {report_path}")

if __name__ == "__main__":
    process_projects()