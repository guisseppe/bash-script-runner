#!/bin/bash

# Colours for output
RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
RESET="\033[0m"

# Output files
MAINTENANCE_FILE="modified_maintenance_files.txt"
RELEASE_FILE="spa-releases.txt"

# Cleanup any existing files
rm -f "$MAINTENANCE_FILE" "$RELEASE_FILE"

echo -e "${YELLOW}Checking for modified maintenance files and release files introduced by the current merge commit...${RESET}"

# Ensure we are running in the `main` branch
if [[ "$CI_COMMIT_BRANCH" != "main" ]]; then
  echo -e "${RED}This job should only run on the 'main' branch.${RESET}"
  exit 1
fi

# Ensure required variables are set
if [[ -z "$CI_MERGE_REQUEST_IID" || -z "$CI_PROJECT_ID" || -z "$CI_JOB_TOKEN" ]]; then
  echo -e "${RED}Error: Required GitLab CI variables are missing. Ensure this job is running in a merge request pipeline.${RESET}"
  exit 1
fi

if [[ -z "$SKIP_DEPLOYMENT_PREFIXES" ]]; then
  echo -e "${RED}Error: SKIP_DEPLOYMENT_PREFIXES is not set. Ensure the environment variable is defined.${RESET}"
  exit 1
fi

# Call Python script to get MR title and check exclusion
python3 fetch_mr_title.py \
  --project-id "$CI_PROJECT_ID" \
  --mr-iid "$CI_MERGE_REQUEST_IID" \
  --token "$CI_JOB_TOKEN" \
  --skip-prefixes "$SKIP_DEPLOYMENT_PREFIXES"

# Capture exit code from Python script
EXIT_CODE=$?

if [[ "$EXIT_CODE" == "0" ]]; then
  echo -e "${YELLOW}Skipping collection of changes due to MR title exclusion.${RESET}"
  exit 0
elif [[ "$EXIT_CODE" == "2" ]]; then
  echo -e "${RED}Error fetching MR details from GitLab API.${RESET}"
  exit 1
fi

# If the Python script returns 1, continue processing
echo -e "${GREEN}Proceeding with modified file collection...${RESET}"

# Get parent commits of the merge
PARENT_COMMITS=$(git log --pretty=%P -n 1 "$CI_COMMIT_SHA")
TARGET_PARENT=$(echo "$PARENT_COMMITS" | awk '{print $1}')
SOURCE_PARENT=$(echo "$PARENT_COMMITS" | awk '{print $2}')

# Debug: Show parents of the merge commit
echo -e "${YELLOW}Target parent (before merge): ${TARGET_PARENT}${RESET}"
echo -e "${YELLOW}Source parent (merged changes): ${SOURCE_PARENT}${RESET}"

# Get the list of files changed between the target parent and source parent
MODIFIED_FILES=$(git diff --name-only "$TARGET_PARENT".."$SOURCE_PARENT")

# Debug: Show all modified files detected
echo -e "${YELLOW}All files changed in the last merge:${RESET}"
echo "$MODIFIED_FILES"

# Filter modified maintenance files
MODIFIED_MAINTENANCE_FILES=$(echo "$MODIFIED_FILES" | grep -E 'release/.*/maintenance-flags(-aws)?\.(yml|yaml)$')

# Filter modified release files
MODIFIED_RELEASE_FILES=$(echo "$MODIFIED_FILES" | grep -E 'release/.*/.*-release\.(yml|yaml)$')

# Process modified maintenance files
if [[ -z "$MODIFIED_MAINTENANCE_FILES" ]]; then
  echo -e "${GREEN}No modified maintenance files found in the current merge commit.${RESET}"
else
  echo "$MODIFIED_MAINTENANCE_FILES" | sort | uniq > "$MAINTENANCE_FILE"
  echo -e "${GREEN}Modified maintenance files saved to '${MAINTENANCE_FILE}':${RESET}"
  cat "$MAINTENANCE_FILE"
fi

# Process modified release files
if [[ -z "$MODIFIED_RELEASE_FILES" ]]; then
  echo -e "${GREEN}No modified release files found in the current merge commit.${RESET}"
else
  echo "$MODIFIED_RELEASE_FILES" | sort | uniq > "$RELEASE_FILE"
  echo -e "${GREEN}Modified release files saved to '${RELEASE_FILE}':${RESET}"
  cat "$RELEASE_FILE"
fi

exit 0