import os
import sys
import gitlab
import argparse

def get_mr_title(project_id, mr_iid, token):
    """Fetches the merge request title from GitLab API."""
    gl = gitlab.Gitlab(os.getenv("CI_SERVER_URL"), private_token=token)
    project = gl.projects.get(project_id)
    mr = project.mergerequests.get(mr_iid)
    return mr.title

def should_skip_mr(mr_title, prefixes):
    """Returns True if MR title starts with any of the prefixes."""
    return any(mr_title.startswith(prefix) for prefix in prefixes)

def main():
    parser = argparse.ArgumentParser(description="Fetch GitLab Merge Request Title and Check Deployment Skip")
    parser.add_argument("--project-id", required=True, help="GitLab Project ID")
    parser.add_argument("--mr-iid", required=True, help="Merge Request IID")
    parser.add_argument("--token", required=True, help="GitLab API Token")
    parser.add_argument("--skip-prefixes", required=True, help="Comma-separated list of prefixes to skip")

    args = parser.parse_args()

    # Convert prefix list
    skip_prefixes = [p.strip() for p in args.skip_prefixes.split(",")]

    try:
        mr_title = get_mr_title(args.project_id, args.mr_iid, args.token)
        print(f"Merge Request Title: {mr_title}")

        if should_skip_mr(mr_title, skip_prefixes):
            print(f"Skipping deployment as MR title starts with an excluded prefix.")
            sys.exit(0)  # Exit with 0 to indicate skipping
        else:
            sys.exit(1)  # Exit with 1 to proceed

    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching MR: {e}")
        sys.exit(2)  # Exit with error code 2 for GitLab API errors

if __name__ == "__main__":
    main()