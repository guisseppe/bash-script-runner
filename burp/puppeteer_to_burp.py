import json
import time
import argparse
from datetime import datetime


def convert_puppeteer_to_burp(puppeteer_json):
    burp_events = []

    # Find the first navigation step to get the correct initial URL
    first_url = next((step["url"] for step in puppeteer_json.get("steps", []) if step["type"] == "navigate"), "about:blank")

    # Generate an initial event metadata block
    burp_events.append({
        "name": "Burp Suite Navigation Recorder",
        "version": "2.0.13",
        "recordingDelay": 0,
        "eventType": "start",
        "platform": "MacIntel",
        "iframes": [],
        "windows": [{"windowId": 1}],
        "tabs": [{"tabId": 1, "windowId": 1, "attributes": {"firstUrl": first_url}}],
    })

    # Track current URL
    current_url = first_url

    for step in puppeteer_json.get("steps", []):
        event = {
            "date": datetime.utcnow().isoformat() + "Z",
            "timestamp": int(time.time() * 1000),
            "frameId": 0,
            "tabId": 1,
            "windowId": 1,
        }

        if step["type"] == "navigate":
            current_url = step["url"]
            event.update({
                "eventType": "goto",
                "url": current_url,
                "triggersNavigation": True,
                "fromAddressBar": True
            })

        elif step["type"] == "click":
            xpath_selector = get_xpath_from_selectors(step.get("selectors", []))
            event.update({
                "eventType": "click",
                "tagName": get_tag_from_xpath(xpath_selector),
                "xPath": xpath_selector,
                "triggersNavigation": False,
                "triggersWithinDocumentNavigation": False,
                "url": current_url,
                "isIframe": False,
            })

        elif step["type"] == "change":
            xpath_selector = get_xpath_from_selectors(step.get("selectors", []))
            event.update({
                "eventType": "typing",
                "tagName": "INPUT",
                "xPath": xpath_selector,
                "typedValue": step["value"],
                "triggersNavigation": False,
                "triggersWithinDocumentNavigation": False,
                "url": current_url,
                "isIframe": False,
            })

        else:
            # Skip unknown or unhandled event types
            continue

        burp_events.append(event)

    return burp_events


def get_xpath_from_selectors(selectors):
    """Extracts the XPath selector from Puppeteer's selector array."""
    for selector_group in selectors:
        for selector in selector_group:
            if selector.startswith("xpath"):
                return selector.replace("xpath//", "")
    return ""


def get_tag_from_xpath(xpath):
    """Infers tag name from XPath (simple heuristic)."""
    if not xpath:
        return "INPUT"  # Default assumption
    xpath_parts = xpath.split("/")
    for part in reversed(xpath_parts):
        if part and "[" in part:
            return part.split("[")[0].upper()
    return "INPUT"  # Fallback


def main():
    parser = argparse.ArgumentParser(description="Convert Puppeteer JSON to Burp Suite JSON format.")
    parser.add_argument("input_file", help="Path to the Puppeteer JSON file")
    parser.add_argument("output_file", nargs="?", default="burp_recording.json",
    help="Output file for Burp Suite JSON (default: burp_recording.json)")

    args = parser.parse_args()

    # Load Puppeteer JSON file
    with open(args.input_file, "r") as file:
        puppeteer_data = json.load(file)

    # Convert to Burp Suite format
    burp_json = convert_puppeteer_to_burp(puppeteer_data)

    # Save to a file
    with open(args.output_file, "w") as file:
        json.dump(burp_json, file, indent=4)

    print(f"Conversion complete. Saved as {args.output_file}")


if __name__ == "__main__":
    main()