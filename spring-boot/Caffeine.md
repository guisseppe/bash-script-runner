# Spring Caffeine Cache

## Introduction
Spring Boot provides seamless integration with **Caffeine Cache**, a high-performance, in-memory caching library for Java applications. Caching is essential for improving performance, reducing database queries, and optimising response times.

This guide covers:
- **Configuring Caffeine Cache using Spring properties**

- **Understanding different eviction policies**

- **Updating the cache when writing to the database**

- **Best practices and common pitfalls**

## **Adding Caffeine Cache to Your Spring Boot Project**
First, add the required dependency:

```xml
<dependency>
    <groupId>com.github.ben-manes.caffeine</groupId>
    <artifactId>caffeine</artifactId>
    <version>3.1.8</version>
</dependency>
```

Enable caching in Spring Boot:
```java
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfig {
}
```

## **Configuring Caffeine Cache with Spring Properties**
Define cache settings in `application.properties`:
```properties
cache.enabled: true  # Ability to disable caching
cache.maximumSize: 500  # Maximum number of items in cache
cache.expireAfterWrite: 10m  # Time after which cache entries expire
cache.expireAfterAccess: 5m  # Expiry based on last access
cache.weakKeys: false  # Use weak references for keys
cache.weakValues: false  # Use weak references for values
```

## **Implementing Caffeine Cache Configuration**

```java
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CaffeineCacheConfig {

    @Value("${cache.enabled:true}")
    private boolean cacheEnabled;
    
    @Value("${cache.maximumSize:500}")
    private int maximumSize;
    
    @Value("${cache.expireAfterWrite:10m}")
    private long expireAfterWrite;
    
    @Value("${cache.expireAfterAccess:5m}")
    private long expireAfterAccess;

    @Bean
    public CacheManager cacheManager() {
        if (!cacheEnabled) {
            return new NoOpCacheManager(); // Disables caching
        }
        Caffeine<Object, Object> caffeineBuilder = Caffeine.newBuilder()
                .maximumSize(maximumSize)
                .expireAfterWrite(expireAfterWrite, TimeUnit.MINUTES)
                .expireAfterAccess(expireAfterAccess, TimeUnit.MINUTES);

        CaffeineCacheManager cacheManager = new CaffeineCacheManager("dataCache");
        cacheManager.setCaffeine(caffeineBuilder);
        return cacheManager;
    }
}
```

## **Caching and Updating Data in the Database**

### **Read Data from Cache**
```java
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class DataRepository {

    @Cacheable(value = "dataCache", key = "#dataId", unless = "#result == null")
    public Data findById(String dataId) {
        return fetchDataFromDatabase(dataId);
    }

    private Data fetchDataFromDatabase(String dataId) {
        // Simulate DB call
        return new Data(dataId, "Sample Data");
    }
}
```
#### **How It Works:**
- Retrieves data from cache if available.

- If not found, queries the database and caches the result.

---

### **Update Data & Refresh Cache**

```java
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

@Service
public class DataService {

    private final DataRepository dataRepository;

    public DataService(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    @CachePut(value = "dataCache", key = "#data.id")
    public Data updateData(Data data) {
        return saveToDatabase(data);
    }

    private Data saveToDatabase(Data data) {
        // Simulate DB save operation
        return data;
    }
}
```
#### **How It Works:**
- `@CachePut` updates cache **when data is modified**.

- Ensures cache always reflects the latest state.

## **Eviction Policies in Caffeine Cache**

### **1. Time-Based Eviction**
| Property               | Description |
|------------------------|-------------|
| `expireAfterWrite`    | Removes an entry after a set period **since creation/update** |
| `expireAfterAccess`   | Removes an entry after a set period **since last access** |

### **2. Size-Based Eviction**
| Property        | Description |
|----------------|-------------|
| `maximumSize`  | Limits cache size to a fixed number of entries |

### **3. Reference-Based Eviction**
| Property      | Description |
|--------------|-------------|
| `weakKeys`   | Uses weak references for cache keys |
| `weakValues` | Uses weak references for cache values |

## **Best Practices & Common Pitfalls**

### ✅ **Use Appropriate Expiration Policies**
- Use `expireAfterAccess` for frequently accessed data.

- Use `expireAfterWrite` for time-sensitive data.

### ✅ **Invalidate Cache After a Significant Change**
- Use `@CacheEvict` to clear outdated data:

```java
@CacheEvict(value = "dataCache", key = "#dataId")
public void removeDataFromCache(String dataId) { }
```

### ✅ **Keep Cache Size Small for Better Performance**
- Avoid storing large datasets.

- Use **maximumSize to limit cache growth**.
