# Mastering Spring Boot Filters for API Request Interception

## Introduction
Spring Boot filters are powerful tools for processing requests **before** they reach a controller or **after** they leave. Let's explore **Spring Boot filters**, how they work, and when to use them.

They are commonly used for:
- **Authentication & Authorization**

- **Logging & Auditing**

- **Rate Limiting**

- **Modifying Requests & Responses**

- **Session Validation**

## 1️⃣ **What is a Spring Boot Filter?**
A **filter** in Spring Boot is a class that processes HTTP requests and responses. It sits in the servlet chain and can:

- Inspect and modify requests before they hit a controller.

- Process responses before they leave the server.

- Halt requests if they don’t meet validation criteria.

Filters extend `OncePerRequestFilter`, ensuring they run once per request lifecycle.

## 2️⃣ **Creating a Simple Spring Boot Filter**

A basic filter that logs all incoming requests:

```java
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class LoggingFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(LoggingFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        logger.info("Incoming request: {} {}", request.getMethod(), request.getRequestURI());
        filterChain.doFilter(request, response);
        logger.info("Response status: {}", response.getStatus());
    }
}
```
### ✅ **How It Works:**
- Logs the **HTTP method** and **URI** before the request is processed.

- Proceeds with the request using `filterChain.doFilter(request, response)`.

- Logs the **HTTP response status** before sending it back.

## 3️⃣ **Applying Filters to Specific Endpoints**
To apply a filter only to certain URLs, implement `javax.servlet.Filter` instead:

```java
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import java.io.IOException;

@Component
public class CustomPathFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if (req.getRequestURI().startsWith("/secure/")) {
            res.sendError(HttpServletResponse.SC_FORBIDDEN, "Access Denied");
            return;
        }
        chain.doFilter(request, response);
    }
}
```
### ✅ **How It Works:**
- If the request starts with `/secure/`, return `403 Forbidden`.

- Otherwise, proceed as normal.

## 4️⃣ **Using Filters for Authentication**
A filter can check authentication headers **before** reaching controllers:

```java
@Component
public class AuthenticationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String authHeader = request.getHeader("Authorization");

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Missing or invalid token");
            return;
        }
        filterChain.doFilter(request, response);
    }
}
```
### ✅ **How It Works:**
- Extracts the `Authorization` header.
- If missing or invalid, returns `401 Unauthorized`.
- Otherwise, proceeds with the request.

---

## 5️⃣ **Ordering Multiple Filters**
When multiple filters exist, set execution order using `@Order`:

```java
@Component
@Order(1)
public class FirstFilter extends OncePerRequestFilter { /* Logic here */ }

@Component
@Order(2)
public class SecondFilter extends OncePerRequestFilter { /* Logic here */ }
```
### ✅ **How It Works:**
- Filters execute in **ascending order** (`@Order(1)` runs before `@Order(2)`).

- Default priority is **lowest precedence** if `@Order` is absent.