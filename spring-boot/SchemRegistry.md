# **Why Do Kafka Producers and Consumers Connect to the Schema Registry?**

When using **Avro** in Kafka, both **producers and consumers** need access to **Confluent Schema Registry** to ensure messages are serialized and deserialized correctly.


## **1️⃣ What Does Schema Registry Do?**
Schema Registry **stores Avro schemas** and assigns them **unique IDs**. It allows Kafka clients to:
- 🏷 **Producers:** Serialize messages using the correct schema version.
- 📥 **Consumers:** Fetch the schema by ID and deserialize messages correctly.

This prevents **schema mismatches** and ensures **backward/forward compatibility**.


## **2️⃣ How Producers Use Schema Registry**
When a **producer** sends an Avro message:
1. The producer **retrieves the schema ID** from Schema Registry.
2. It **serializes** the message into Avro format.
3. The first **5 bytes** of the message store the **schema ID**.
4. The **binary Avro message** is sent to the Kafka topic.

**Example Producer Code:**
```java
Properties props = new Properties();
props.put("bootstrap.servers", "localhost:9092");
props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
props.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
props.put("schema.registry.url", "http://localhost:8081"); // 🔴 Connects to Schema Registry

KafkaProducer<String, OrderEvent> producer = new KafkaProducer<>(props);
```


## **3️⃣ How Consumers Use Schema Registry**
When a **consumer** receives an Avro message:
1. It **extracts the schema ID** from the first 5 bytes of the message.
2. It **fetches the correct schema** from Schema Registry.
3. It **deserializes** the message into a Java object.

**Example Consumer Code:**
```java
Properties props = new Properties();
props.put("bootstrap.servers", "localhost:9092");
props.put("group.id", "order-group");
props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
props.put("value.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
props.put("schema.registry.url", "http://localhost:8081"); // 🔴 Connects to Schema Registry
props.put("specific.avro.reader", true); // Use Java classes instead of GenericRecord

KafkaConsumer<String, OrderEvent> consumer = new KafkaConsumer<>(props);
```


## **4️⃣ Why Some Parts of the System Don’t Need Schema Registry**
Certain components **don’t need direct access** to Schema Registry:
1. **Kafka Brokers** 🚫: Brokers **don’t care about schemas**—they just store and forward messages.
2. **Consumers Using JSON or Raw Bytes** 🚫: If consumers read raw JSON or bytes, they **don’t need Schema Registry**.
3. **Clients Using Generic Avro Deserialization** 🚫: Some clients **embed the schema** in the message itself.


## **5️⃣ What Happens if Schema Registry Is Unavailable?**
- 🛑 **Producers will fail** (they can’t fetch the schema ID).
- 🛑 **Consumers will fail** (they can’t decode messages).
- ✅ **Existing messages remain safe** in Kafka but can’t be consumed.

💡 **Solution:** Use **cached schemas** in production to reduce dependency on Schema Registry.


## **🎯 Final Thoughts**
- **Schema Registry is required** for **Avro serialization/deserialization**.
- **Producers fetch schema IDs** before sending messages.
- **Consumers need schema IDs** to decode messages.
- **Kafka brokers don’t need Schema Registry.**

Let me know if you need help debugging your setup! 🚀