# Guide to Writing Avro Schema Files (`.avsc`)

## Introduction
Avro schemas (`.avsc` files) define the structure of Avro data and ensure data consistency across platforms.

This guide provides a step-by-step approach to writing Avro schemas, including validation using enums and optional nullable fields.

## Avro Schema (`.avsc`)
An Avro schema is written in JSON format and supports various data types:

- **Records**: Objects with named fields.
- **Enums**: A fixed set of string values.
- **Arrays**: Lists of values.
- **Maps**: Key-value pairs.
- **Unions**: Multiple possible types.
- **Fixed**: Fixed-size byte arrays.

## Basic Structure of an Avro Schema
A basic Avro schema consists of:

- **Type**: The data structure (e.g., `record`, `enum`, `array`).
- **Name**: A unique identifier.
- **Namespace**: A logical grouping for the schema.
- **Fields**: Definitions of each data element.

### Example: Simple Avro Schema
```json
{
  "type": "record",
  "name": "User",
  "namespace": "com.example.avro",
  "fields": [
    { "name": "id", "type": "string" },
    { "name": "age", "type": "int" },
    { "name": "email", "type": ["null", "string"], "default": null }
  ]
}
```
✅ **Notes:**
- `namespace` groups related schemas.
- `record` defines an object.
- `type: ["null", "string"]` makes `email` optional.


## Enum Validation in Avro
Enums restrict field values to predefined options.

### Example: Enum Schema
```json
{
  "type": "enum",
  "name": "Status",
  "symbols": ["ACTIVE", "INACTIVE", "SUSPENDED"]
}
```
✅ **Usage:** Ensures only `ACTIVE`, `INACTIVE`, or `SUSPENDED` values are valid.


## Complex Records with Enums
Combining **records** with enums creates structured validation.

### Example: Employee Schema with Enum
```json
{
  "type": "record",
  "name": "Employee",
  "namespace": "com.company.hr",
  "fields": [
    { "name": "empId", "type": "string" },
    { "name": "name", "type": "string" },
    { "name": "department", "type": {
        "type": "enum",
        "name": "Department",
        "symbols": ["HR", "ENGINEERING", "SALES"]
      }
    },
    { "name": "salary", "type": "double" },
    { "name": "isActive", "type": "boolean" }
  ]
}
```
✅ **Ensures** `department` is only `HR`, `ENGINEERING`, or `SALES`.


## Making Fields Optional
Using **union types** allows nullable fields.

### Example: Nullable Field
```json
{
  "name": "phone",
  "type": ["null", "string"],
  "default": null
}
```
✅ **If not provided, defaults to `null`.**


## Arrays and Maps
### Example: Array of Strings
```json
{
  "name": "skills",
  "type": { "type": "array", "items": "string" }
}
```
✅ **Stores a list of skills.**

### Example: Key-Value Pair (Map)
```json
{
  "name": "properties",
  "type": { "type": "map", "values": "string" }
}
```
✅ **Defines a dynamic set of key-value properties.**

## Fixed Types for Binary Data
Fixed types define **fixed-length binary fields**.

### Example: MD5 Hash
```json
{
  "name": "md5",
  "type": { "type": "fixed", "name": "MD5", "size": 16 }
}
```
✅ **Ensures a 16-byte MD5 hash.**

## 8. Saving and Using Avro Schema
### **Save as `.avsc`**

- Each schema should be in its own `.avsc` file.

## Best Practices
✅ **Use meaningful names** for records and enums.

✅ **Ensure backward compatibility** with nullable new fields.

✅ **Use default values** to handle missing fields.

✅ **Validate schema changes** using Avro tools.