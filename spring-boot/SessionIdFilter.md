# Implementing a Spring Boot Filter for API Authentication using MongoDB

## Introduction
This guide focuses on implementing a **Spring Boot filter** that:

- **Intercepts API calls** matching specified patterns.

- **Extracts `sessionId`, `cin`, `account_number`, and `brand` from headers**.

- **Validates session data against MongoDB**.

- **Allows or rejects the request** based on session existence and header validation.


## 1️⃣ **Define Spring Properties**
Configure the filter behavior in `application.yml`:
```yaml
filter:
  enabled: true
  patterns:
    - "/card/.+?/([^/]+)"
    - "/secure/.+?/([^/]+)"
```

Or in `application.properties`:
```properties
filter.enabled=true
filter.patterns[0]=/card/.+?/([^/]+)
filter.patterns[1]=/secure/.+?/([^/]+)
```

## 2️⃣ **Create the API Intercepting Filter**

```java
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class SessionValidationFilter extends OncePerRequestFilter {

    @Autowired
    private SessionRepository sessionRepository;

    @Value("${filter.enabled:true}")
    private boolean filterEnabled;

    @Value("${filter.patterns}")
    private List<String> regexPatterns;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if (!filterEnabled) {
            filterChain.doFilter(request, response);
            return;
        }

        String requestURI = request.getRequestURI();
        String sessionId = extractSessionId(requestURI);
        if (sessionId == null) {
            filterChain.doFilter(request, response);
            return;
        }

        String cin = request.getHeader("cin");
        String accountNumber = request.getHeader("account_number");
        String brand = request.getHeader("brand");

        if (cin == null || accountNumber == null || brand == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing required headers: cin, account_number, or brand");
            return;
        }

        Optional<SessionData> sessionDataOpt = sessionRepository.findBySessionId(sessionId);

        if (sessionDataOpt.isPresent()) {
            SessionData sessionData = sessionDataOpt.get();
            if (sessionData.getCin().equals(cin) && sessionData.getAccountNumber().equals(accountNumber)) {
                filterChain.doFilter(request, response);
            } else {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid session details");
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Session not found");
        }
    }

    private String extractSessionId(String uri) {
        for (String patternStr : regexPatterns) {
            Pattern pattern = Pattern.compile(patternStr);
            Matcher matcher = pattern.matcher(uri);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }
}
```


## 🎯 **How It Works**
✅ **Intercepts API calls matching regex patterns from properties**  
✅ **Extracts and validates headers (`cin`, `account_number`, `brand`)**  
✅ **Queries MongoDB for session data**  
✅ **Allows the request if the `cin` and `account_number` match stored values**  
✅ **Blocks unauthorized requests (`400 Bad Request`)**  


## 🚀 **Conclusion**
This **Spring Boot filter** provides a configurable way to validate session-based requests using MongoDB. Define patterns in `application.yml` or `application.properties` using an array format and ensure requests contain valid headers before processing. 🚀
