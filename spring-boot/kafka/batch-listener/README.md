# Efficient Batch Processing with Spring Kafka: A Configurable Approach

How to build an configurable batch processing system using **Spring Kafka**. The goal is to process Kafka messages outside a defined batch window, with the flexibility to control the listener's behavior using **Spring Kafka Container Listener Registry**.

## 📊 **Architecture Overview**

Our architecture includes the following components:

1. **Kafka Listener**: Processes messages and stops when the batch window is active.

2. **Batch Window Checker**: Ensures processing happens outside defined time windows.

3. **Scheduler**: Periodically checks whether the listener should start or stop.

4. **Listener Container Manager**: Manages the Kafka listener's lifecycle programmatically.

### 🖌️ **Architecture Diagram**

```mermaid
graph TD
    A[Kafka Topic] -->|Produces Messages| B[Kafka Listener]
    B -->|Processes Messages| C[Batch Window Checker]
    C -->|Is it outside batch window?| D{Decision}
    D -->|Yes| E[Process and Acknowledge Message]
    D -->|No| F[Stop Listener]
    E --> G[Scheduler]
    F --> G
    G -->|Every 10 min| B
```

### 🖌️ **Sequence Diagram**

```mermaid
sequenceDiagram
    participant Scheduler
    participant KafkaListener
    participant BatchWindowChecker
    participant KafkaTopic

    Scheduler->>KafkaListener: Start Listener
    KafkaListener->>KafkaTopic: Poll for Messages
    KafkaTopic-->>KafkaListener: Deliver Messages
    KafkaListener->>BatchWindowChecker: Check if outside batch window
    alt Outside Batch Window
        BatchWindowChecker-->>KafkaListener: Yes
        KafkaListener->>KafkaTopic: Process and Acknowledge Messages
    else Inside Batch Window
        BatchWindowChecker-->>KafkaListener: No
        KafkaListener->>Scheduler: Stop Listener
    end
    Scheduler->>KafkaListener: Check every 10 minutes
```

## 🔧 **Key Features**

- **Configurable Batch Window**: Easily set the start, end, and buffer times via properties.

- **Controlled Message Processing**: The listener stops processing immediately when the batch window becomes active.

- **Dynamic Scheduling**: The scheduler checks every 10 minutes (or as configured) to start processing.

- **Separation of Concerns**: Each component handles a specific part of the logic for better maintainability.

## 📁 **Project Setup**

### 1. **Dependencies**
Ensure you have the following dependencies in your `pom.xml`:

```xml
<dependency>
    <groupId>org.springframework.kafka</groupId>
    <artifactId>spring-kafka</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.kafka</groupId>
    <artifactId>spring-kafka-test</artifactId>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```

## 📄 **Configuration in `application.properties`**

```properties
# Batch Window Configuration
batch.window.start=21:00
batch.window.end=06:00
batch.window.buffer.minutes=15

# Kafka Listener Configuration
kafka.listener.container-id=batchProcessorListener

# Scheduler Configuration
kafka.scheduler.cron=0 */10 * * * *  # Runs every 10 minutes
```

## 💡 **Step-by-Step Implementation**

### 1. **Batch Window Checker**
This component ensures the Kafka listener operates only outside the defined batch window.

```java
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Component
@RequiredArgsConstructor
public class BatchWindowChecker {

    @Value("${batch.window.start}")
    private String batchWindowStart;

    @Value("${batch.window.end}")
    private String batchWindowEnd;

    @Value("${batch.window.buffer.minutes}")
    private int bufferMinutes;

    public boolean isOutsideBatchWindow() {
        LocalTime now = LocalTime.now();
        LocalTime start = LocalTime.parse(batchWindowStart).minusMinutes(bufferMinutes);
        LocalTime end = LocalTime.parse(batchWindowEnd).plusMinutes(bufferMinutes);

        if (start.isBefore(end)) {
            return now.isBefore(start) || now.isAfter(end);
        } else {
            return now.isAfter(end) && now.isBefore(start);
        }
    }
}
```

### 2. **Kafka Listener for Batch Processing**

This listener checks the batch window before processing each message and stops immediately if the batch window becomes active. Messages are manually acknowledged after successful processing.

```java
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class BatchKafkaMessageProcessor {

    private final KafkaListenerContainerManager containerManager;
    private final BatchWindowChecker batchWindowChecker;

    @Value("${kafka.listener.container-id}")
    private String containerId;

    @KafkaListener(id = "${kafka.listener.container-id}", topics = "batch-topic", autoStartup = "false")
    public void processMessage(String message, Acknowledgment acknowledgment) {
        if (batchWindowChecker.isOutsideBatchWindow()) {
            log.info("Processing batch message: {}", message);

            try {
                // Your processing logic here
                acknowledgment.acknowledge();
            } catch (Exception e) {
                log.error("Error processing message: {}", e.getMessage());
                // Optionally handle retries or other error handling
            }
        } else {
            log.info("Batch window active, stopping listener.");
            containerManager.stopListener(containerId);
        }
    }
}
```

### 3. **Listener Container Manager**
This utility manages the lifecycle of the Kafka listener programmatically.

```java
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class KafkaListenerContainerManager {

    private final KafkaListenerEndpointRegistry registry;

    public void startListener(String listenerId) {
        MessageListenerContainer container = registry.getListenerContainer(listenerId);
        if (container != null && !container.isRunning()) {
            container.start();
            log.info("Started listener with ID: {}", listenerId);
        }
    }

    public void stopListener(String listenerId) {
        MessageListenerContainer container = registry.getListenerContainer(listenerId);
        if (container != null && container.isRunning()) {
            container.stop();
            log.info("Stopped listener with ID: {}", listenerId);
        }
    }
}
```

### 4. **Scheduler to Control the Listener**
The scheduler periodically checks if the batch window is closed and starts the listener accordingly.

```java
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class BatchKafkaScheduler {

    private final BatchKafkaMessageProcessor batchKafkaMessageProcessor;
    private final BatchWindowChecker batchWindowChecker;
    private final KafkaListenerContainerManager containerManager;

    @Value("${kafka.listener.container-id}")
    private String containerId;

    @Scheduled(cron = "${kafka.scheduler.cron}")
    public void checkAndProcessMessages() {
        if (batchWindowChecker.isOutsideBatchWindow()) {
            log.info("Outside batch window. Starting batch processor listener.");
            containerManager.startListener(containerId);
        } else {
            log.info("Within batch window. Ensuring batch processor listener is stopped.");
            containerManager.stopListener(containerId);
        }
    }
}
```

### 5. **Integration Tests Using Embedded Kafka**

To ensure our Kafka listener works as expected, we will write integration tests using **Embedded Kafka**. We'll dynamically adjust the batch window within the test and verify that messages are processed or skipped accordingly.

```java
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(properties = {"batch.window.start=00:00", "batch.window.end=23:59"})
@EmbeddedKafka(partitions = 1, topics = {"batch-topic"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BatchKafkaIntegrationTest {

    @Autowired
    private BatchWindowChecker batchWindowChecker;

    @Autowired
    private KafkaListenerContainerManager containerManager;

    private KafkaTemplate<String, String> createKafkaTemplate() {
        Map<String, Object> producerProps = KafkaTestUtils.producerProps(EmbeddedKafkaBroker);
        producerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(producerProps));
    }

    @Test
    public void testMessageProcessingOutsideBatchWindow() {
        System.setProperty("batch.window.start", "23:00");
        System.setProperty("batch.window.end", "23:59");

        KafkaTemplate<String, String> kafkaTemplate = createKafkaTemplate();
        kafkaTemplate.send("batch-topic", "Test Message Outside Batch Window");

        containerManager.startListener("batchProcessorListener");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        List<ConsumerRecord<String, String>> records = KafkaTestUtils.getRecords(
            KafkaTestUtils.consumerProps("testGroup", "true", EmbeddedKafkaBroker), 
            2000, 
            "batch-topic"
        );

        assertThat(records).isEmpty();
    }

    @Test
    public void testListenerStopsDuringBatchWindow() {
        System.setProperty("batch.window.start", "00:00");
        System.setProperty("batch.window.end", "23:59");

        KafkaTemplate<String, String> kafkaTemplate = createKafkaTemplate();
        kafkaTemplate.send("batch-topic", "Test Message During Batch Window");

        containerManager.startListener("batchProcessorListener");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        List<ConsumerRecord<String, String>> records = KafkaTestUtils.getRecords(
            KafkaTestUtils.consumerProps("testGroup", "true", EmbeddedKafkaBroker), 
            2000, 
            "batch-topic"
        );

        assertThat(records).isNotEmpty();
    }
}
```

## 🚀 **Running the Application**

Ensure scheduling is enabled in your main Spring Boot application class:

```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KafkaBatchApplication {
    public static void main(String[] args) {
        SpringApplication.run(KafkaBatchApplication.class, args);
    }
}
```