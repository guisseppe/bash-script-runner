# Spring Properties Checker

### Purpose:
This Bash script validates the existence and values of required properties within a Spring Boot `application.properties` file. It's designed to ensure that critical configuration parameters are present and correctly set.

### Functionality:

* **Property Validation:**  Checks if a set of required properties are defined in the `application.properties` file. Additionally, it verifies that the property values match expected values.
* **Error Reporting:** Provides clear error messages in red if properties are missing or have incorrect values.
* **Success Message:**  Displays a success message in green if all required properties are found and have the correct values.
* **Confluence Link:** Includes a link to a Confluence page for troubleshooting and configuration instructions.
* **Logging:** Uses colored output (green for success, red for errors) for improved readability.
* **Exit Codes:** Uses standard exit codes for integration with other scripts or automation tools (0 for success, 1 for errors).

### Usage:

```bash
./check_spring_properties.sh [-p <properties_file>] [-h]
```

### Options:

* `-p <properties_file>`:   Specifies the path to the `application.properties` file. If not provided, defaults to 'application.properties' in the current directory. 
* `-h`: Displays help information.

### Configuration:

1. **REQUIRED_PROPERTIES:**  Modify the `REQUIRED_PROPERTIES` associative array in the script. Add properties as key-value pairs, where the key is the property name and the value is its expected value. Example:

   ```bash
   declare -A REQUIRED_PROPERTIES=(
       ["server.port"]="8080"
       ["spring.datasource.url"]="jdbc:mysql://localhost:3306/your_database"
   )
   ```

2. **CONFLUENCE_LINK:** Update the `CONFLUENCE_LINK` variable with the correct URL to your Confluence page containing configuration instructions.

### Example Output:

**1. Missing or incorrect properties:**

```
Error: Property 'server.port' is missing.
Error: Property 'spring.datasource.url' has incorrect value. Expected: 'jdbc:mysql://localhost:3306/your_database', Found: 'jdbc:postgresql://localhost:5432/test_db'. 
Please refer to the Confluence page for configuration instructions: https://yourcompany.atlassian.net/wiki/spaces/KB/pages/123456/Spring+Boot+Configuration 
```

**2. All properties correct:**

```
All required properties found and values are correct.
```

### Integration:
This script can be easily integrated into build pipelines or deployment processes to catch configuration errors early in the development cycle.