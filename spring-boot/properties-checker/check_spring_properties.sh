#!/bin/bash

# Color codes for logging
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' 

# Default properties file
DEFAULT_PROPERTIES_FILE="application.properties"

# Confluence link
CONFLUENCE_LINK="https://yourcompany.atlassian.net/wiki/spaces/KB/pages/123456/Spring+Boot+Configuration"

# Help function
usage() {
  echo "Usage: $0 [-p <properties_file>] [-h]"
  echo "  -p <properties_file>  Path to the application.properties file (optional, defaults to '$DEFAULT_PROPERTIES_FILE')"
  echo "  -h                    Display this help message"
}

# Process command-line flags
while getopts ":p:h" opt; do
  case $opt in
    p)
      PROPERTIES_FILE=$OPTARG
      ;;
    h)
      usage
      exit 0
      ;;
    \?)
      echo -e "${RED}Invalid option: -$OPTARG${NC}" >&2
      usage
      exit 1
      ;;
  esac
done

# Check for properties file existence
if [ ! -f "$PROPERTIES_FILE" ]; then
    echo -e "${RED}Error: Properties file not found: $PROPERTIES_FILE ${NC}"
    exit 1
fi

# Function to check property existence and value
check_property() {
  local prop_name=$1
  local expected_value=$2

  local value=$(grep -oP "^$prop_name=\K.*$" "$PROPERTIES_FILE")

  if [ -z "$value" ]; then
    echo -e "${RED}Error: Property '$prop_name' is missing.${NC}"
    return 1 
  elif [ "$value" != "$expected_value" ]; then
    echo -e "${RED}Error: Property '$prop_name' has incorrect value. Expected: '$expected_value', Found: '$value'.${NC}"
    return 1
  fi
}

# Required properties and their expected values (modify as needed)
declare -A REQUIRED_PROPERTIES=(
  ["server.port"]="8080"
  ["spring.datasource.url"]="jdbc:mysql://localhost:3306/your_database"
  ["app.security.key"]="your_strong_security_key"
)

# Check properties
missing_or_incorrect=0
for prop in "${!REQUIRED_PROPERTIES[@]}"; do
  check_property "$prop" "${REQUIRED_PROPERTIES[$prop]}"
  if [ $? -ne 0 ]; then
      missing_or_incorrect=1
  fi
done

# Report and exit
if [ $missing_or_incorrect -eq 1 ]; then
  echo -e "${RED}Please refer to the Confluence page for configuration instructions:${NC} $CONFLUENCE_LINK"
  exit 1
else
  echo -e "${GREEN}All required properties found and values are correct.${NC}"
  exit 0
fi
