# Spring Dependency Injection

The `@Autowired` array functionality is an extension of Spring's core dependency injection mechanism, which focuses on injecting beans "by type". Here's how it operates:

1. **Component Scanning:**
    * Spring scans your project and finds classes annotated as candidates for dependency injection. These are typically classes annotated with `@Component`, `@Service`, `@Repository`, etc.
    * Spring creates beans (instances) of these classes and maintains them in its application context.

2. **Autowired Array:**
    * When you declare an array field with the `@Autowired` annotation, Spring analyzes the array's element type. Let's say your array looks like this:

    ```java
    @Autowired
    private List<Rule> rules; 
    ```

3. **Type Matching:**
    * Spring searches its application context for **all** beans whose type is either:
        * The exact type of the array element (`Rule` in the example above)
        * A subclass or a class that implements the array element type.

4. **Array Injection:**
    * If Spring finds matching beans, it collects them into an array.
    * Spring injects this array into the `@Autowired` array field.

## Key Points

* **All Matches:** Spring will inject _all_ compatible beans it finds, not just a single bean. Your array will likely contain multiple elements.
* **Ordering:** By default, the order of elements in the injected array may not be deterministic. If order matters, you can:
    * Use the `@Order` annotation on your rule implementations to control their priority.
    * Sort the `rules` array after injection if needed.

### Example

```java
@Component
public class AgeRuleImpl implements Rule { ... }

@Component
public class CountryRuleImpl implements Rule { ... }

@Service
public class MyService {
    @Autowired
    private List<Rule> rules; // Contains both AgeRuleImpl and CountryRuleImpl 
}
```

#### Advantages

* **Flexibility:** Easily add new rules (implementing the `Rule` interface) without modifying the service using them.
* **Loose Coupling:** The service doesn't need concrete knowledge about the specific rule implementations.