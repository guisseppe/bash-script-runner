# Journey Validator Unit Testing

## Test Example

1.  **Setup with Mocks:**  We mock both rules and inject them into the `JourneyValidator`. We configure `rule1` to return a successful response and `rule2` to return a failed response.

2.  **Execution:** The `runValidation()` method is called.

3.  **Assertions:**
    *   The returned `Optional<ValidationResponse>` should be present (not empty).
    *   The contained `ValidationResponse` should match the expected failed response.
    *   A `JourneyValidationException` should be thrown with a specific message.

```Java
@ExtendWith(MockitoExtension.class)
class JourneyValidatorTest {

    @Mock
    private Rule rule1;

    @Mock
    private Rule rule2;

    @InjectMocks
    private JourneyValidator journeyValidator;

    @Test
    void runValidation_singleRuleFailure_shouldReturnFailedResponse() {
        // Setup - Mock rule1 to pass, rule2 to fail
        when(rule1.validate(any(ValidationParams.class))).thenReturn(successfulResponse());
        when(rule2.validate(any(ValidationParams.class))).thenReturn(failedResponse());
        journeyValidator.setRules(Arrays.asList(rule1, rule2));

        // Execute & Assertion
        Exception exception = assertThrows(JourneyValidationException.class, () -> journeyValidator.runValidation(123, 456, "brand"));
        assertTrue(exception.getMessage().contains("Rule violation found:"));

        Optional<ValidationResponse> result = journeyValidator.runValidation(123, 456, "brand");
        assertTrue(result.isPresent());
        assertEquals(failedResponse(), result.get());
    }

    // ... (Helper methods as before)
    private ValidationResponse successfulResponse() {
        // Create a success response
        return new ValidationResponse(/* ... */);
    }

    private ValidationResponse failedResponse() {
        // Create a failed response
        return new ValidationResponse(/* ... */);
    }
}
```
### Key Points

*   **Helper Methods:** Using helper methods keeps your test code organized.

*   **`any()` Matcher:** In this case, we're not concerned with the exact `ValidationParams`. Mockito's `any()` matcher allows flexibility.

*   **Error Handling Test:**  The `assertThrows` method provides a convenient way to verify exception handling.
