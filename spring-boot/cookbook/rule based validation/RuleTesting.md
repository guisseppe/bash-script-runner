# Rule Unit Testing

The following is a unit test based on the example Income Rule

## Mocking Considerations

* **CustomerClient:** You'll want to use a mocking framework (e.g., Mockito) to control the behavior of `customerClient`. This isolates the `IncomeRuleImpl` logic.

## Test Scenarios

1.  **Income Above Minimum:**
    *   **Mock:** Set up `customerClient.getIncome()` to return a value greater than `minimumIncome`.
    *   **Assert:** `validate()` should return a `ValidationResponse` where `status` is `ValidationStatus.PASS` (success).

2.  **Income Below Minimum:**
    *   **Mock:**  Set up `customerClient.getIncome()` to return a value less than `minimumIncome`.
    *   **Assert:**  `validate()` should return a `ValidationResponse` where `status` is `ValidationStatus.FAILED` (failure).

3.  **Income Equal to Minimum:**
    *   **Mock:**  Set up `customerClient.getIncome()` to return a value equal to `minimumIncome`.
    *   **Assert:**  `validate()` should return a `ValidationResponse` where `status` is `ValidationStatus.PASS` (success).

4.  **CustomerClient Exception:**
    *   **Mock:**  Throw an exception from `customerClient.getIncome()`.
    *   **Assert:**  Your test should either:
        *   Expect a specific exception to be thrown by `validate()`.
        *   Verify that failure handling occurs gracefully within `IncomeRuleImpl`.

### Example Test Structure (using Mockito)

```java
@ExtendWith(MockitoExtension.class)
class IncomeRuleImplTest {

    @Mock
    private CustomerClient customerClient;

    @InjectMocks
    private IncomeRuleImpl incomeRule;

    @Test
    void validate_incomeAboveMinimum_shouldReturnSuccess() {
        // Setup
        BigDecimal customerIncome = new BigDecimal("2000");
        BigDecimal minimumIncome = new BigDecimal("1500");
        when(customerClient.getIncome()).thenReturn(customerIncome);

        // Execution
        ValidationResponse response = incomeRule.validate(new ValidationParams()); // Assuming any params are fine

        // Assertion
        assertEquals(ValidationStatus.PASSED, response.getStatus());
    }

    // ... add other test scenarios
}
```

### Example Parameterised Test


Parameterized tests for scenarios two and three.


* **Scenario Two:** Income below minimum leads to validation failure.
* **Scenario Three:** Income equal to minimum leads to validation success.

```java
@ExtendWith(MockitoExtension.class)
class IncomeRuleImplParameterizedTest {

    @Mock
    private CustomerClient customerClient;

    @InjectMocks
    private IncomeRuleImpl incomeRule;

    @ParameterizedTest
    @CsvSource({
            "1000, 1500, FAILED",  // Income below minimum
            "1500, 1500, PASS"    // Income equal to minimum
    })
    void validate_parameterized(BigDecimal customerIncome, BigDecimal minimumIncome, ValidationStatus expectedStatus) {
        // Setup
        when(customerClient.getIncome()).thenReturn(customerIncome);

        // Execution
        ValidationResponse response = incomeRule.validate(new ValidationParams()); // Assuming any params are fine

        // Assertion
        assertEquals(expectedStatus, response.getStatus());
    }
}
```

#### Explanation

1.  **@ParameterizedTest:** JUnit5 annotation to run the test multiple times with different data.
2.  **@CsvSource:** Specifies comma-separated values as the source of test parameters.
3.  **Parameter Mapping:**
    *   `customerIncome`: The customer's income for a test case.
    *   `minimumIncome`: The minimum income threshold for a test case.
    *   `expectedStatus`: The expected outcome (failure or success) for a test case.
4.  **Mocking:** As before, we mock the `CustomerClient`.
5.  **Test Logic:** Each test run uses a different set of parameters from the `CsvSource`, providing a flexible scenario-driven testing approach.

#### Key Advantages

*   **Conciseness:** Tests multiple scenarios with a single test method.
*   **Clarity:** Clearly separates test data from test logic.
*   **Maintenance:** Easy to add or change scenarios by modifying the `CsvSource`.
