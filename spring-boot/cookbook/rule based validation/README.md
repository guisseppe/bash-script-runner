# Rule Based Journey validator Template

## Overview

This code demonstrates a proof-of-concept for a flexible rule-based validation system. The core principles are:

* **Rules as Interfaces:** Validation logic is encapsulated within classes implementing the `Rule` interface.  This makes it easy to add new validation rules without modifying the core validation engine.
* **Validation Response:** The `ValidationResponse` class provides a structured format to communicate the status of a rule (pass/fail), its relative importance (rank), and various other parameters.
* **Rule Prioritization:** Rules are given a `rank` indicating their importance. This can be used to prioritize and short-circuit validation.
* **Parallel Processing:** Parallel rule execution, potentially improving performance with multiple rules.

## Rule Interface Structure

1. **Rule Interface (`Rule`)**

  Defines the contract for all validation rules.

    ```java
    public interface Rule {
        ValidationResponse validate(ValidationParams params); //Executes the rule's validation logic
    }
    ```

2. **ValidationParams (`ValidationParams`)**: Stores input parameters required by the rule classes.
    ```java
    import lombok.Builder;

    @Builder
    public class ValidationParams {
        private int cin; //Customer identification Number
        private int accountNumber;
        private String brand; // Brand name (Should be an Enum?)
    }
    ```

3. **Validation Response (`ValidationResponse`)**
    * **Purpose:** Stores the result of a rule evaluation.
    * **Fields:**
        - `status`: Enum indicating if the rule passed.
        - `rank`: Integer denoting the rule's priority.
        - `name`: String representing the rule's class name (for identification).
        [comment]: <> (Update the validation response with expected fields)

4. **Rule Implementations**
    * Journey Rules should implement the Rule Interface.

### Usage Example
This code implements an income validation rule using customer data retrieved from a CustomerClient (Java class that calls the customer API).
```java
@Slf4j
@Component
public class IncomeRuleImpl implements Rule {

    private final Integer rank;
    private final BigDecimal minimumIncome;
    private final CustomerClient customerClient;

    @Autowired
    public IncomeRuleImpl(@Value("${journey.validation.rank.income}") int rank,
     @Value("${journey.validation.params.minimumIncome}") BigDecimal minimumIncome,
     CustomerClient customerClient) {
        this.rank = rank;
        this.minimumIncome = minimumIncome;
        this.customerClient = customerClient;
    }

    @Override
    public ValidationResponse validate(ValidationParams params) {
        ValidationResponse response = new ValidationResponse();
        response.setName(IncomeRuleImpl.class.getSimpleName());

        log.debug("Starting income validation");

        response.setStatus(ValidationStatus.FAILED);

        if (customerClient.getIncome() > minimumIncome) {
          response.setStatus(ValidationStatus.PASS);
        }

        response.setRank(rank);
        return response;
    }
}
```

**@Component:** A Spring framework annotation, indicating this class is managed by Spring and can be injected into other parts of the application.

**@Slf4j:** Provides easy logging capabilities using the Lombok library.

**@Value:** Used to inject properties (rank and minimumIncome) from a properties file.

**CustomerClient:** Java class that calls the customer API.

**validate(ValidationParams params) method:** The core logic where the income is compared to the minimum, building a ValidationResponse object.

## Journey Validator Structure
```java
@Slf4j
@Service
public class JourneyValidator {

    @Autowired
    private List<Rule> rules; // Autowire all Rule implementations

    public Optional<ValidationResponse> runValidation(int cin, int accountNumber, String brand) {
        ValidationParams params = buildParams(cin, accountNumber, brand);

        Optional<ValidationResponse> violation = rules.stream()
            .parallel()
            .map(rule -> rule.validate(params)) // Pass personalDetails
            .sorted(Comparator.comparingInt(ValidationResponse::getRank))
            .filter(r -> ValidationStatus.FAILED.equals(r.getStatus()))
            .findFirst();

        // Log the output
        if (violation.isPresent()) {
            String message = StringUtils.join("Rule violation found: ", violation.get().getName());
            // exception handled in GlobalExceptionHandler class
            throw new JourneyValidationException(message, violation.get());
        }

        return violation; //Happy path is empty object - HTTP CODE 204
    }

    private ValidationParams buildParams(int cin, int accountNumber, String brand) {
      return ValidationParams params = builder.cin(cin)
                           .accountNumber(accountNumber)
                           .brand(brand)
                           .build();
    }
}
```

**@Slf4j:**  Provides logging capabilities using Lombok.

**@Service:** A Spring annotation, indicating this class is a service component managed by the Spring framework.

**List<Rule> rules:** Injected list of all classes that implement the `Rule` interface (e.g income rule, credit score rule, etc.).

**runValidation() method:** The core method responsible for:

* **Building ValidationParams:** Takes the input (`cin`, `accountNumber`, `brand`) and creates a `ValidationParams` object to package the data for validation.

* **Rule Execution:** Runs the validation rules in parallel. Each rule receives the `ValidationParams` object, checks the customer data, and returns a `ValidationResponse` indicating success or failure (and potentially additional information).

* **Violation Handling:** If any rule fails (`ValidationStatus.FAILED`), the first failed response is located and a `JourneyValidationException` is thrown, likely triggering error handling in the GlobalExceptionHandler.

* **Success Path:** If no violations are found, an empty `Optional<ValidationResponse>` is returned (a common way to indicate success without needing a dedicated success object).

## Summary
1.  **Component Scanning:** When your Spring application starts, it scans for classes annotated with `@Component`, `@Service`, or similar annotations. It will discover and register your rule implementations as beans in the Spring application context.

2.  **Autowiring:** The `@Autowired` annotation in `JourneyValidator` tells Spring to automatically inject all available beans implementing the `Rule` interface into the `rules` list.

3.  **Using the Service:** In your Spring controllers or other services, you can inject the `JourneyValidator` and use its `validate()` method to perform validation based on your configured rules.

### Advantages of this approach

* **Cleaner Separation of Concerns:** Rules are decoupled, and the `JourneyValidator` handles validation logic.
* **Improved Testability:** You can easily mock rule implementations for unit testing the validator service.
* **Flexibility:** Adding new rules just involves creating a new class implementing the `Rule` interface and annotating it with `@Component`.
