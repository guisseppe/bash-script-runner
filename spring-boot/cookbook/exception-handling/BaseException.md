An example of a `BaseException` class to serve as the foundation for your custom exception hierarchy.

**BaseException.java**

```java
package com.example.validationdemo.exceptions; // Adjust your package accordingly 

public class BaseException extends RuntimeException {

    private String errorCode; // Optional: For structured error codes

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public BaseException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    // Getter for errorCode (if you're using it)
    public String getErrorCode() {
        return errorCode;
    }
}
```

**Explanation**

*   **Inheritance:**  `BaseException` extends `RuntimeException` making it an unchecked exception (debatable choice, but often suitable for application-level errors).
*   **Constructors:**  Provides the following:
    *   Simple message-based constructor.
    *   Constructor to chain with the original exception (`Throwable cause`).
    *   Overloads to include an optional `errorCode` for more structured error reporting.
*   **errorCode:** This field allows you to associate a unique identifier with each exception type for easier tracking and handling.

**How to Use It**

```java
public class DataNotFoundException extends BaseException {
    public DataNotFoundException(String message) {
        super(message, "DATA_NOT_FOUND"); 
    }
}
```

**Ehancements**
*   **Checked vs. Unchecked:** Consider if `BaseException` should be a checked exception  (`extends Exception`) to enforce explicit handling.
*   **Additional Fields:**  You might add fields as needed to `BaseException`, like timestamps or other relevant data.