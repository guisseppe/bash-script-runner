How you can centralize the logging of response models from multiple `@ControllerAdvice` classes in a Spring Boot application.

**1. Logging Framework:**

* **Choose a logging framework:** Popular choices include Logback, Log4j2, or the standard Java Util Logging (JUL). I'll demonstrate using Logback, as it's flexible and often used with Spring Boot.

**2. Central Logging Aspect**

* **Create an aspect:** An aspect (using Spring AOP) will intercept the return values from your `@ControllerAdvice` classes.

```java
@Aspect
@Component
public class ResponseLoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(ResponseLoggingAspect.class);

    @AfterReturning(pointcut = "within(@org.springframework.web.bind.annotation.ControllerAdvice *)",  returning = "result")
    public void logResponse(JoinPoint joinPoint, Object result) {
        logger.info("Controller advice response: {}", result);
    }
}
```

**Explanation:**

* `@Aspect`:  Marks this class as an aspect.
* `@Component`: Makes it a Spring-managed bean.
* `@AfterReturning`:  The advice runs after your `@ControllerAdvice` methods return.
* `pointcut`: Targets all methods within classes annotated with `@ControllerAdvice`.
* `returning`: Captures the returned value (`result`).
* `logResponse`:  Logs the response using an SLF4J logger.

**3. Configuration**

* **Dependencies:**  Ensure you have the appropriate logging dependency (e.g., Logback) in your project.

* **Enable AspectJ:** Add `@EnableAspectJAutoProxy` to your Spring configuration class.

**4. Customization:**

* **Log formatting:**  Adjust the `logger.info` statement to customize the log format (include additional information, structure as JSON, etc.).
* **Filtering:**  Refine the `pointcut` expression to target specific `@ControllerAdvice` classes or methods if needed.
* **Error logging:**  Create a separate `@AfterThrowing` advice to log exceptions from your controller advice classes.

**Key Advantages of Centralization:**

* **Consistency:** Provides a consistent way to log responses across different controller advice classes.
* **Maintainability:** Logging logic is in one place, making changes easier.
* **Debugging:**  Simplifies debugging as you have a centralized view of responses.