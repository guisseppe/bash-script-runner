**1. Rule Interface (`Rule.java`)**

```java
package com.example.validationdemo.rules;

public interface Rule {
    ValidationResponse validate(PersonalDetails personalDetails);
}
```

**2. Validation Response (`ValidationResponse.java`)**

```java
package com.example.validationdemo.rules;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ValidationResponse {
    private Boolean status;
    private Integer rank;
    private String name;
}
```

**3. Example Rule Implementations**

```java
// AgeRuleImpl.java
package com.example.validationdemo.rules;

import org.springframework.stereotype.Component;

@Component
public class AgeRuleImpl implements Rule {
    // ... implementation
}

// (Similar for CountryRuleImpl, DependentsRuleImpl)
```

**4.  Custom Annotation (`ValidateUniqueRuleRanks.java`)**

```java
package com.example.validationdemo.rules;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = UniqueRuleRanksValidator.class)
public @interface ValidateUniqueRuleRanks {
    String message() default "Rules contain duplicate ranks";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
```

**5. Custom Annotation Validator (`UniqueRuleRanksValidator.java`)**

```java
package com.example.validationdemo.rules;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

public class UniqueRuleRanksValidator implements ConstraintValidator<ValidateUniqueRuleRanks, Set<Rule>> {

    @Override
    public boolean isValid(Set<Rule> rules, ConstraintValidatorContext context) {
        Set<Integer> seenRanks = new HashSet<>();
        for (Rule rule : rules) {
            if (!seenRanks.add(rule.getRank())) {
                throw new DuplicateRankException("Duplicate rank found: " + rule.getClass().getSimpleName());
            }
        }
        return true;
    }
}
```

**6. Custom Exception (`DuplicateRankException.java`)**

```java
package com.example.validationdemo.rules;

public class DuplicateRankException extends RuntimeException {
    public DuplicateRankException(String message) {
        super(message);
    }
}
```