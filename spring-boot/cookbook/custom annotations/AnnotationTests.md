**Key Concepts**

*   **Indirect Testing:** We don't directly test the annotation itself. Instead, we test our `UniqueRuleRanksValidator`, which is responsible for the validation logic triggered by the annotation.
*   **Test Scenarios:** Focus on covering both valid and invalid cases (duplicate ranks).
*   **Mocking:**  If the validator interacts with other components, consider mocking those dependencies to keep the unit tests focused.

**1. Test Class: `UniqueRuleRanksValidatorTest.java`**

```java
package com.example.validationdemo.rules;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class UniqueRuleRanksValidatorTest {

    private UniqueRuleRanksValidator validator = new UniqueRuleRanksValidator();

    @Test
    void testIsValid_UniqueRanks() {
        Set<Rule> rules = new HashSet<>();
        rules.add(new TestRule(1));
        rules.add(new TestRule(2));
        rules.add(new TestRule(3));

        ConstraintValidatorContext context = mock(ConstraintValidatorContext.class);

        assertTrue(validator.isValid(rules, context));
    }

    @Test
    void testIsValid_DuplicateRanks() {
        Set<Rule> rules = new HashSet<>();
        rules.add(new TestRule(1));
        rules.add(new TestRule(2));
        rules.add(new TestRule(2)); // Duplicate

        ConstraintValidatorContext context = mock(ConstraintValidatorContext.class);

        assertThrows(DuplicateRankException.class, () -> validator.isValid(rules, context));
    }

    // Simple test rule stand-in
    private static class TestRule implements Rule {
        private final int rank;

        public TestRule(int rank) {
            this.rank = rank;
        }

        @Override
        public int getRank() {
            return rank;
        }

        // ... other Rule methods (not important for this test)
    }
}
```

**Explanation**

1.  **Setup:** We create an instance of `UniqueRuleRanksValidator`.
2.  **Test Cases:**
    *   `testIsValid_UniqueRanks`: Verifies that validation passes with a set of unique rules.
    *   `testIsValid_DuplicateRanks`:  We intentionally include duplicate ranks and use `assertThrows` to assert that our `DuplicateRankException` is thrown.
3.  **Mocking:** We mock the `ConstraintValidatorContext` to isolate the validator's logic.
4.  **Test Rule:** A simple `TestRule` class to provide the rank values for our tests.